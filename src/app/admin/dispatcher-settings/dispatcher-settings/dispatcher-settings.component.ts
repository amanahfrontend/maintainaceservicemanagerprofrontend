import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EditDisAndProbModalComponent } from "./edit-dis-and-prob-modal/edit-dis-and-prob-modal.component";
import { DataTable, MessageService } from 'primeng/primeng';
import { SettingsDispatchersService } from './settings-dispatchers.service';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dispatcher-settings',
  templateUrl: './dispatcher-settings.component.html',
  styleUrls: ['./dispatcher-settings.component.scss']
})

export class DispatcherSettingsComponent implements OnInit, AfterViewInit, OnDestroy {
  disWithProbs: any = [];
  allDispatchers: any = [];
  toggleLoading: boolean;
  allAreas = [];
  dispatcherList = [];
  allProblems: any = [];
  isLoading: boolean;
  page = {
    //The number of elements in the page
    size: 50,
    //The total number of elements
    totalElements: 0,
    //The total number of pages
    totalPages: 0,
    //The current page number
    pageNumber: 0,
    offset: 1
  };

  @ViewChild('dt', { static: false }) dataTable: DataTable;
  @ViewChild('el', { static: false }) el: ElementRef;
  paginationobj = {
    pageNo: 1,
    pageSize: 50,
    count: 0
  }

  searchObj: any = {
    dispatchers: [],
    orderProblems: [],
    areas: []
  };

  numberOfServicesCalled: number = 0;
  numberOfServicesFinished: number = 0;
  closeModalSubscribtion: Subscription;

  constructor(private lookup: LookupService,
    private modalService: NgbModal,
    private settingsDispatchersService: SettingsDispatchersService,
    private messageService: MessageService,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.toggleLoading = true;
    this.getAllProblems();
    this.getAreas();
    this.getDispatcherList();
    this.getDispatchers();
  }


  ngAfterViewInit() {
    this.closeModalSubscribtion = this.settingsDispatchersService.closeModal
      .subscribe(
        () => {
          this.isLoading = true;
          this.getDispatchers();
          this.getDispatcherList();
        }
      );
  }

  onSearch() {
    this.getDispatchers();
  }

  onReset() {
    this.searchObj = {
      dispatchers: [],
      orderProblems: [],
      areas: []
    };
    this.getDispatchers();
  }

  setPage(e) {
    this.page.pageNumber = e.offset;
    this.paginationobj.count = e.count;
    this.paginationobj.pageNo = e.offset + 1;
    this.getDispatchers();
  }

  onPageChangeGetData(event: { first: number, rows: number }) {
    this.paginationobj.pageNo = (event.first / event.rows) + 1;
    this.getDispatchers();
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.toggleLoading = false;
      this.isLoading = false;
    }
  }

  AddNewProblem() {
    let modalRef = this.modalService.open(EditDisAndProbModalComponent);
    modalRef.result
      .then(
        () => {
          this.getDispatchers();
        }
      ).catch(
        () => {
          this.messageService.add({
            severity: 'info',
            summary: this.translateService.instant('Nothing Edited!'),
            detail: this.translateService.instant("You didn't submit!")
          });
        }
      );
  }

  getDispatcherList() {
    this.seriveCalled();
    this.lookup.getALLDispatcherSettingsList()
      .subscribe(
        (data) => {
          this.dispatcherList = data;
          this.dispatcherList.map((dis, i) => {
            this.dispatcherList[i] = {
              label: dis.fullName,
              value: dis.name
            }
          }
          );
          this.checkIfServicesFinished();
        },
        err => {
          console.log(err)
          this.checkIfServicesFinished();
        }
      );
  }

  getAreas() {
    this.seriveCalled();
    this.lookup.getAllAreasWithBlockedList().subscribe((data) => {
      this.allAreas = data.data;
      this.allAreas.map(
        (area, i) => {
          this.allAreas[i] = {
            label: area.name,
            value: area.name
          }
        }
      );
      this.checkIfServicesFinished();
    },
      err => {
        console.log(err);
        this.checkIfServicesFinished();
      }
    );
  }

  getAllProblems() {
    this.lookup.getAllOrderProblemWithBlockedList().subscribe((problems) => {
      this.allProblems = problems.data;
      this.allProblems.map(
        (prob, i) => {
          this.allProblems[i] = {
            label: prob.name,
            value: prob.name
          }
        }
      );
    },
      err => {
        console.log(err);
        this.checkIfServicesFinished();
      }
    )
  }

  getAllDispatchers() {
    this.seriveCalled();
    this.lookup.getAllDispatchers()
      .subscribe(
        (dispatchers) => {
          if (this.disWithProbs.length) {
            let result = this.disWithProbs.map(a => a.fK_Dispatcher_Id);
            var unique = result.filter((v, i, a) => a.indexOf(v) === i);
            this.allDispatchers = [];
            dispatchers.data.map(
              (dis) => {
                if (!~unique.indexOf(dis.id)) {
                  this.allDispatchers.push(dis);
                }
              }
            );
          } else {
            this.allDispatchers = dispatchers.data;
          }
          this.allDispatchers.map(
            (dis, i) => {
              this.allDispatchers[i] = {
                fK_Dispatcher_Id: dis.id,
                dispatcher: {
                  id: dis.id,
                  userName: dis.name
                },
                area: '-',
                orderProblem: {
                  name: '-'
                }
              };
            }
          );
          this.checkIfServicesFinished();
        },
        err => {
          console.log(err);
          this.checkIfServicesFinished();
        }
      );
  }

  openEditModal(dispatcher, event) {
    event.target.parentElement.parentElement.blur();
    let modalRef = this.modalService.open(EditDisAndProbModalComponent);
    modalRef.componentInstance.data = dispatcher;
    modalRef.result
      .then(
        () => {
          this.getDispatchers();
        }
      ).catch(
        () => {
          this.messageService.add({
            severity: 'info',
            summary: this.translateService.instant('Nothing Edited!'),
            detail: this.translateService.instant("You didn't change the old value")
          });
        }
      );
  }


  getDispatchers() {
    this.seriveCalled();
    let objToPost = {
      dispatcherNames: this.searchObj.dispatchers,
      orderProblemNames: this.searchObj.orderProblems,
      areaNames: this.searchObj.areas,
      pageInfo: this.paginationobj
    };
    this.lookup.getAllDispatcherSettingspaginated(objToPost)
      .subscribe(
        (diswithProbs) => {
          this.page.totalElements = diswithProbs.data.count;
          this.disWithProbs = diswithProbs.data.data;
          this.getAllDispatchers()
          this.checkIfServicesFinished();
        },
        err => {
          console.log(err);
          this.checkIfServicesFinished();
        }
      );
  }

  ngOnDestroy() {
    this.closeModalSubscribtion.unsubscribe();
  }
}
