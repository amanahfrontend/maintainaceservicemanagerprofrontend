import { Routes } from '@angular/router';
import { AddTeamComponent } from './add-team/add-team.component';
import { EditTeamComponent } from './edit-team/edit-team.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { TeamManagementComponent } from './team-management.component';

export const teamRoutes: Routes = [
    {
        path: '',
        component: TeamManagementComponent,
        children: [
            {
                path: '',
                redirectTo: 'add',
                pathMatch: 'full'
            },
            {
                path: 'add',
                component: AddTeamComponent,
                data: { title: 'Team Management' }
            },
            {
                path: 'update',
                component: EditTeamComponent,
                data: { title: 'Team Management' }
            },
            {
                path: 'vehicle',
                component: VehicleComponent,
                data: { title: 'Team Management' }
            }
        ]
    },
];