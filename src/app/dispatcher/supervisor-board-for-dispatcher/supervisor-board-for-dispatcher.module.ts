import { NgModule } from '@angular/core';
import { SupervisorBoardForDispatcherComponent } from './supervisor-board-for-dispatcher.component';
import { SharedBoardModule } from '../shared-board/shared-board.module';
import { RouterModule, Routes } from '@angular/router';
import { EditOrderComponent } from '../shared-board/edit-order/edit-order.component';
import { TranslateModule } from '@ngx-translate/core';

const SupervisorBoardForDispatcherRoutes: Routes =
  [
    {
      path: "",
      component: SupervisorBoardForDispatcherComponent
    },
    {
      path: "edit-order/:orderId",
      component: EditOrderComponent
    }
  ];

@NgModule({
  declarations: [
    SupervisorBoardForDispatcherComponent,
  ],
  imports: [
    SharedBoardModule,
    RouterModule.forChild(SupervisorBoardForDispatcherRoutes),
    TranslateModule
  ]
})
export class SupervisorBoardForDispatcherModule { }
