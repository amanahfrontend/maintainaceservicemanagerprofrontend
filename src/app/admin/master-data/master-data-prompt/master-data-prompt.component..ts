import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-master-data-prompt',
  templateUrl: './master-data-prompt.component.html',
  styleUrls: ['./master-data-prompt.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class MasterDataPromptComponent implements OnInit, OnDestroy {
  @Input() header;
  @Input() type;
  @Input() data: any;
  statesSubscriptions: Subscription;
  states: any[];
  Days: any = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  dateToShowOnCal: any;
  defaultDate: any;
  startDate: Date = new Date(null);
  endDate: Date = new Date(null);

  constructor(
    public activeModal: NgbActiveModal,
    private lookup: LookupService,
    private messageService: MessageService,
    private translateService: TranslateService,
  ) { }

  ngOnInit() {
    this.startDate.setHours(0);
    this.endDate.setHours(0);
    if (this.data.fromTime) {
      this.startDate = new Date(this.data.fromTime)
    }
    if (this.data.toTime) {
      this.endDate = new Date(this.data.toTime)
    }
  }

  /**
   * ? unsubscribe observables
   */
  ngOnDestroy() {
    this.statesSubscriptions && this.statesSubscriptions.unsubscribe();
  }

  /**
   * ? close the modal and send the value of the pop up.
   * 
   * @param result : value of the pop up form.
   */
  close(result) {
    //debugger
    if (this.type == 'Area' || this.type == 'Building Types' || this.type == 'Company Code' || this.type == 'Contract Type'
      || this.type == 'Division' || this.type == 'Languages') {
      result = {
        id: this.data.id,
        area_No: result.area_No,
        code: result.code,
        name: result.name,
      };
    }
    else if (this.type == 'Shift') {
      result = {
        id: this.data.id == undefined ? 0 : this.data.id,
        currentUserId: localStorage.getItem("AlghanimCurrentUserId")
      };
      let fhours = (this.startDate.getHours() < 10) ? "0" + this.startDate.getHours() : this.startDate.getHours();
      let fmins = (this.startDate.getMinutes() < 10) ? "0" + this.startDate.getMinutes() : this.startDate.getMinutes();
      result.fromTime = `0001-01-01T${fhours}:${fmins}:00`;
      let thours = (this.endDate.getHours() < 10) ? "0" + this.endDate.getHours() : this.endDate.getHours();
      let tmins = (this.endDate.getMinutes() < 10) ? "0" + this.endDate.getMinutes() : this.endDate.getMinutes();
      result.toTime = `0001-01-01T${thours}:${tmins}:00`;
      if (result.fromTime == result.toTime) {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed!'),
          detail: this.translateService.instant("From time should not be equal to to time, please select different times!")
        });
        return;
      }
    }
    else if (this.type == 'Problems') {
      result = {
        id: this.data.id,
        name: result.name,
        code: result.code,
        exceedHours: result.exceedHours
      };
    }
    else if (this.type == 'On-Hold Sub-Status Types') {
      result = {
        statusId: 6,
        code: result.code,
        name: result.name
      };
    }
    else if (this.type == 'Complete Sub-Status Types') {
      result = {
        statusId: 7,
        code: result.code,
        name: result.name
      };
    }
    else if (this.type == 'Order Cancellation Reasons') {
      result = {
        statusId: 8,
        code: result.code,
        name: result.name
      };
    }
    else if (this.type == 'Idle Types') {
      result = {
        category: 'Idle Time hours',
        categoryCode: 3,
        name: result.name
      };
    }
    else if (this.type == 'Break Types') {
      result = {
        category: 'Break hours',
        categoryCode: 4,
        name: result.name
      };
    }
    else if (this.type == 'Order Status') {
      result = {
        code: result.code,
        name: result.name
      };
    }

    else {
      result = {
        id: this.data.id,
        name: result.name,
      };
    }
    this.activeModal.close(result);
  }


  /**
   * ? enable only numbers
   * 
   * @param event html input
   */
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }


  /**
  * ? enable only numbers
  *
  * @param event html input
  */
  patternPreventSpecialCharacter(event: any) {
    const pattern = /^[0-9a-zA-Z \b]+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  /**
  * 
  * @param event html input
  */
  PreventSpaceInFirstPosition(e) {
    if (e.which === 32 && e.target.selectionStart === 0) {
      return e.preventDefault();
    }
  }

}
