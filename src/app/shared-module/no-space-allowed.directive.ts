import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[disable-spaces]'
})
export class NoSpacesAllowedDirective {
  inputElement: ElementRef;
  @Input('appNoSpacesAllowedInAllLetters') appNoSpacesAllowedInAllLetters: boolean = false;
  constructor(el: ElementRef) {
    this.inputElement = el;
  }

  @HostListener('paste', ['$event']) onPaste(event) {
    if (this.appNoSpacesAllowedInAllLetters) {
      let regex = /[a-zA-Z0-9\u0600-\u06FF]/g;;
      const e = <ClipboardEvent>event;
      const pasteData = e.clipboardData.getData('text/plain');
      let m;
      let matches = 0;
      while ((m = regex.exec(pasteData)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
          regex.lastIndex++;
        }
        // The result can be accessed through the `m`-variable.
        m.forEach((match, groupIndex) => {
          matches++;
        });
      }
      if (matches === pasteData.length) {
        return;
      } else {
        e.preventDefault();
      }
    } else {
      const e = <ClipboardEvent>event;
      const pasteData = e.clipboardData.getData('text/plain');
      if (pasteData[0] == " ") {
        e.preventDefault();
      }

    }
  }

  @HostListener('keypress', ['$event']) onKeyPress(event) {
    if (!this.appNoSpacesAllowedInAllLetters && !event.target.value.length && (event.which === 32 || event.which === 13) || !this.appNoSpacesAllowedInAllLetters && (event.which === 32 || event.which === 13) && event.target.selectionStart === 0) {
      event.preventDefault();
    } else if (this.appNoSpacesAllowedInAllLetters && (event.which === 32 || event.which === 13)) {
      event.preventDefault();
    }
  }
}