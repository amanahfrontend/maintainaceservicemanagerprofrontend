// import { AuthGuardGuard } from './api-module/guards/auth-guard.guard';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './login-register-module/login-register-module.module#LoginRegisterModuleModule' },
  { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  { path: 'dispatcher', loadChildren: './dispatcher/dispatcher.module#DispatcherModule' },
  { path: 'supervisor', loadChildren: './dispatcher/supervisor.module#SupervisorModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
export const RoutingComponents = [];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);
