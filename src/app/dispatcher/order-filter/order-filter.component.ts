import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';

@Component({
  selector: 'app-order-filter',
  templateUrl: './order-filter.component.html',
  styleUrls: ['./order-filter.component.css']
})

export class OrderFilterComponent implements OnInit, OnDestroy {
  filter: any;
  allStatuses = [];
  allAreas: any[] = [];
  allProblems: any[] = [];
  allDispatchers: any[] = [];
  allOrderTypes: any[] = [];
  allDivisions: any[] = [];
  filterObjectToPost: object;
  ListOfRejectionStatus: any[];
  ListOfFromWhere: any[];

  allRejectionStatus: any = [];
  startDateFrom: Date;
  startDateTo: Date;
  reportOrders: any[] = [];
  @Output() todayOrders = new EventEmitter;
  @Output() filteredOrders = new EventEmitter;
  @Output() resetFilter = new EventEmitter;
  @Output() orderProgressMode = new EventEmitter;
  @Output() filterTermsBoard = new EventEmitter;
  @Output() filterTermsMap = new EventEmitter;


  constructor(private lookup: LookupService) { }

  ngOnInit() {
    this.filter = {};
    this.filter.fromWhere = 3;
    this.filter.RejectionStatus = 4;
    this.getAllStatuses();
    this.getAllAreas();
    this.getAllProblems();
    this.getAllOrderTypes();
    this.getAllDevisions();
    this.getAllDispatchers();
    this.allRejectionStatus = [
      { name: 'Rejected', id: 3 },
      { name: 'accepted', id: [0, 1, 2] },
    ];
    this.ListOfFromWhere = [
      {
        label: "Unassigned only",
        value: "1"
      },
      {
        label: "Assigned only",
        value: "2"
      },
      {
        label: "Both",
        value: "3"
      },
    ];
    this.ListOfRejectionStatus = [
      {
        label: "No Action",
        value: "1"
      },
      {
        label: "Accepted",
        value: "2"
      },
      {
        label: "Rejected",
        value: "3"
      },
      {
        label: "All Actions",
        value: "4"
      },
    ];
  }

  onDateFromChanged(e) {
    this.filter.dateFrom = e;
  }

  onDateToChanged(e) {
    this.filter.dateTo = e;
  }

  ngOnDestroy() {
  }

  applyFilter() {
    let filterAreas = [];
    let filterProblems = [];
    let filterStatus = [];
    let filterOrderTypes = [];
    let filterDivisions = [];
    let filterDispatchers = [];
    // get selected areas
    this.filter.areas && this.filter.areas.map(
      (area) => {
        filterAreas.push(area.id);
      }
    );
    // get selected problems
    this.filter.problems && this.filter.problems.map(
      (problem) => {
        filterProblems.push(problem.id);
      }
    );
    // get selected orders
    this.filter.status && this.filter.status.map(
      (status) => {
        filterStatus.push(status.id);
      }
    );
    // get selected order types
    this.filter.orderTypes && this.filter.orderTypes.map(
      (ordertype) => {
        filterOrderTypes.push(ordertype.id)
      }
    );
    // get selected divisions
    this.filter.divisions && this.filter.divisions.map(
      (division) => {
        filterDivisions.push(division.id)
      }
    );
    // get selected dispatchers
    this.filter.dispatchers && this.filter.dispatchers.map(
      (dispatcher) => {
        filterDispatchers.push(dispatcher.id)
      }
    );

    let dateFrom: Date = new Date(this.filter.dateFrom);
    let dateTo: Date =new Date(this.filter.dateTo);
    dateFrom.setHours(dateFrom.getHours() + 3);
    dateTo.setHours(dateTo.getHours() + 3);


    this.filterObjectToPost = {
      orderCode: this.filter.code,
      customerName: this.filter.customerName,
      customerCode: this.filter.customerCode,
      customerPhone: this.filter.phone,
      areaIds: filterAreas,
      dateFrom: dateFrom,
      dateTo: dateTo,
      blockName: this.filter.blockName,
      problemIds: filterProblems,
      statusIds: filterStatus,
      rejection: this.filter.RejectionStatus,
      dispatcherId: filterDispatchers,
      fromWhere: this.filter.fromWhere,
    };
    // removing unused filter objects to send the used objects only
    for (let property in this.filterObjectToPost) {
      if (this.filterObjectToPost.hasOwnProperty(property)) {
        if (Array.isArray(this.filterObjectToPost[property])) {
          if (!this.filterObjectToPost[property].length) {
            delete this.filterObjectToPost[property];
          }
        } else {
          if (!this.filterObjectToPost[property]) {
            delete this.filterObjectToPost[property];
          }
        }
      }
    }
    this.filterTermsBoard.emit(this.filterObjectToPost);
  }

  emitTodayOrders() {
    this.filter = {};
    this.todayOrders.emit();
  }

  checkFilter() {
    if (!this.filter.dateFrom || !this.filter.dateTo) {
      return true;
    } else {
      return false;
    }
  }

  applyResetFilter() {
    // let today = new Date();
    // let startDate = new Date(today.setHours(0, 0, 0, 0)).toISOString();
    // let endDay = new Date(today.setDate(new Date(startDate).getDate() + 1)).toISOString();
    this.filter = {
      fromWhere: 3,
      RejectionStatus: 4,
      // dateTo: endDay,
      // dateFrom: startDate
    };
    this.reportOrders = [];
    this.resetFilter.emit();
  }

  getAllStatuses() {
    this.lookup.getAllStatusWithBlockedList()
      .subscribe(
        res => this.allStatuses = res.data,
        err => console.log(err)
      );
  }

  getAllProblems() {
    this.lookup.getAllOrderProblemWithBlockedList()
      .subscribe(
        res => this.allProblems = res.data,
        err => console.log(err)
      );
  }

  getAllAreas() {
    this.lookup.getAllAreasWithBlockedList()
      .subscribe(
        res => this.allAreas = res.data,
        err => console.log(err)
      );
  }

  getAllOrderTypes() {
    this.lookup.getAllOrderTypesWithBlockedList()
      .subscribe(
        res => this.allOrderTypes = res.data,
        err => console.log(err)
      );
  }

  getAllDevisions() {
    this.lookup.getAllDivisionsWithBlockedList()
      .subscribe(
        res => this.allDivisions = res.data,
        err => console.log(err)
      );
  }

  getAllDispatchers() {
    this.lookup.getAllDispatchersWithBlockedList()
      .subscribe(
        res => this.allDispatchers = res.data,
        err => console.log(err)
      );
  }
}
