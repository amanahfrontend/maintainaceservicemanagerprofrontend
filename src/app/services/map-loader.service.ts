import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MapLoaderService {

  private static promise;

  /**
   * 
   * @param key 
   */
  public static load(key: string) {
    const url = `http://maps.googleapis.com/maps/api/js?key=${key}&callback=__onGoogleLoaded`;

    if (!MapLoaderService.promise) {
      MapLoaderService.promise = new Promise(resolve => {
        window['__onGoogleLoaded'] = (ev) => {
          resolve('google maps api loaded');
        };

        let node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';
        document.getElementsByTagName('head')[0].appendChild(node);
      });
    }

    return MapLoaderService.promise;
  }


}
