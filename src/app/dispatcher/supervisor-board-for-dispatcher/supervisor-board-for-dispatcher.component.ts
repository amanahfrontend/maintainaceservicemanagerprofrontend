import { element } from "protractor";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,

} from "@angular/core";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NotificationsService } from "../notifications.service";
import { Subscription, interval, Subject, Observable, forkJoin } from "rxjs";
import { SharedNotificationService } from "../../shared-module/shared/shared-notification.service";
import { MessageService } from "primeng/api";
import { RefreshRateService } from "src/app/services/refresh-rate.service";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/refresh-rate.reducer";
import { switchMap, flatMap, map } from "rxjs/operators";
import { OrderCardViewModel } from "src/app/models/orderCard.model";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-supervisor-board-for-dispatcher",
  templateUrl: "./supervisor-board-for-dispatcher.component.html",
  styleUrls: ["./supervisor-board-for-dispatcher.component.scss"],
})
export class SupervisorBoardForDispatcherComponent
  implements OnInit, OnDestroy {

  // Properties

  // To Manage the Spinner
  isLoading: boolean;
  numberOfServicesCalled: number = 0;
  numberOfServicesFinished: number = 0;
  //  toggleLoading: boolean;
  fullScreenMap: any;
  showFilter: boolean;
  isFilterActive: boolean = false;
  //fetchingDataDone: boolean;
  isBulkAssign: boolean;
  cornerMessage: any[];
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  disabledStatus: string[];
  orders: OrderCardViewModel[] = [];
  foremen: any = [];
  dispatcherList: any = [];
  selectedDispatcher: any = {};
  terms: any = {
    fromWhere: 3
  };
  pageNumber: number = 1;
  CurrentUserHasDispatcherRole: boolean;
  CurrentUserHasSupervisorRole: boolean;
  subs = new Subscription();
  filterSubject = new Subject();
  @ViewChild('unassignedContainer', { static: false }) unassignedContainer: ElementRef;

  constructor(
    private lookup: LookupService,
    private notificationsService: NotificationsService,
    public sharedNotificationService: SharedNotificationService,
    private messageService: MessageService,
    private refreshRateService: RefreshRateService,
    private store: Store<AppState>,
    private translateService: TranslateService
  ) { }


  /*
   *  
   *
  */
  ngOnInit() {
    this.getDispatcherBySuper();
    this.filterSubscribtion();
    let currentUserRoles: string[] = JSON.parse(
      localStorage.getItem("AlghanimCurrentUser")
    ).data.roles;
    this.CurrentUserHasDispatcherRole = currentUserRoles.includes("Dispatcher");
    this.CurrentUserHasSupervisorRole = currentUserRoles.includes("Supervisor");
    this.subscribeToRefreshRate();
  }

  /*
   *  Get Supervisor's Dispatchers.
   *  Fill in the DispatcherList DropDownList
   * 
   */
  getDispatcherBySuper() {
    this.seriveCalled();
    let superId = localStorage.getItem("AlghanimCurrentUserId");
    this.lookup.getDispatcherBySuper(superId).subscribe(
      (res: any) => {
        this.dispatcherList = res.data;
        this.dispatcherList.map((dis: any) => {
          dis.label = dis.fullName;
          dis.value = dis.id;
        });
        this.checkIfServicesFinished();
      },
      (err) => this.onServerError(err)
    );
  }

  /*
   * subscribe to get the all orders using the Terms Object
   *
   */
  filterSubscribtion() {
    this.subs.add(
      this.filterSubject.pipe(switchMap(() => this.applyFilterBoardTemplate(this.terms)))
        .subscribe((res: any) => {
          if (res.length > 0) {
            this.fetchRequestsData(res);
            this.checkIfServicesFinished();
          } else {
            this.onServerError(res);
          }
        }, (err: any) => {
          this.onServerError(err)
        }));
  }

  /*
   * Get All the filtered orders. 
   * Using the Terms which passed in the post request
   */

  applyFilterBoardTemplate(terms: any) {
    terms.dispatcherId = this.selectedDispatcher.id;
    this.terms = Object.assign({},this.terms ,terms);
    let requests$: Observable<any>[] = [];

    let unAssignedOrders$ = this.getDispatcherUnassignedOrders();
    requests$.push(unAssignedOrders$);

    let AssignedOrders$ = this.lookup.getDispatcherFormensWithTeams(this.selectedDispatcher.id)
    requests$.push(AssignedOrders$);

    return AssignedOrders$.pipe(
      flatMap(
        (res: any) => {
          res.data.map(foreman => {
            if (foreman.teams.length > 0)
              foreman.teams.map(team => {
                let terms = JSON.parse(JSON.stringify(this.terms))
                terms.teamId = team.teamId;
                requests$.push(
                  this.lookup.getTeamOrdersByTeamId(terms)
                );
              });
          });
          return forkJoin(requests$)
        })
    );

    // if (terms.fromWhere == 3) {
    //   // terms.fromWhere = 1;
    //   this.lookup.postBoardOrderFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetAssignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    //   // terms.fromWhere = 2;
    //   this.lookup.postAllUnAssignedOrdersForDispatcherFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetUnassignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    // } else if (terms.fromWhere == 2) {
    //   // emptyFilterObj.fromWhere = 1;
    //   this.lookup.postBoardOrderFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetAssignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    //   // terms.fromWhere = 2;
    //   this.lookup
    //     .postAllUnAssignedOrdersForDispatcherFilter(emptyFilterObj)
    //     .subscribe(
    //       (res: any) => this.CheckToSetUnassignedOrders(res),
    //       (err: any) => this.onServerError(err)
    //     );
    // } else if (terms.fromWhere == 1) {
    //   // terms.fromWhere = 1;
    //   this.lookup.postBoardOrderFilter(emptyFilterObj).subscribe(
    //     (res: any) => this.CheckToSetAssignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    //   // emptyFilterObj.fromWhere = 2;
    //   this.lookup.postAllUnAssignedOrdersForDispatcherFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetUnassignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    // }
    // this.showHideFilter();
  }

  /*
   Map Responses to Unassigned and Teams Containers
  */
  fetchRequestsData(data) {
    // get Unassigned Orders Data
    let unassignedOrders = data.shift();
    // Bind unAassigned Orders
    this.CheckToSetUnassignedOrders(unassignedOrders)
    // Get Assigned Orders - Foremen
    let assignedOrders = data.shift();
    // Get each forman's teams orders
    let teamsorders = data;
    // bind orders into each team's orders
    let teamId = 0;
    for (let i = 0; i < assignedOrders.data.length; i++) // foremen
    {
      for (let j = 0; j < assignedOrders.data[i].teams.length; j++) // foreman teams
      {
        let orders = teamsorders[teamId].data.data;
        assignedOrders.data[i].teams[j].pageNumber = 1;
        assignedOrders.data[i].teams[j].orders.push(...orders);
        teamId++;
      }
    }

    this.setAssignedOrders(assignedOrders);
  }

  findOrdersByTeamId(id): any[] {

    for (let i = 0; this.foremen.length > i; i++) {
      for (let j = 0; this.foremen[i].teams.length > j; j++) {
        if (this.foremen[i].teams[j].teamId === id) {
          return this.foremen[i].teams[j].filterOrders;
        }
      }
    }

    return [];
  }

  /*
     Set the Formen's teams, and set each team orders
  */

  setAssignedOrders(foremen) {
    this.foremen = foremen.data || [];
    this.foremen.map((forman) => {
      forman.teams.map((team: any) => {
        if (team.orders) {
          team.filterOrders = team.orders.slice();
        } else {
          team.filterOrders = [];
          team.orders = [];
        }
      });
    });
  }


  /*
   * Set the Un Assigned Orders
  */
  setUnassignedOrders(orders) {
    this.orders = orders || [];
    setTimeout(() => {
      this.unassignedContainer.nativeElement.scroll(0, 0);
    }, 0);
  }


  /*
  * Event onchange Handler.
  * Fires when the supervisor select a dispatcher from the dropdown list 
  */
  onSelectDispatcher(e: any) {
    // this.seriveCalled();
    this.selectedDispatcher = {};
    this.selectedDispatcher.id = e.value;
    this.orders = [];
    this.foremen = [];
    this.resetBoardFilter();
    this.notificationsService.getPermission();
    this._receiveNotification();
    this.onRecieveChangeRank();
    // this.subscribeToRefreshRate();
  }


  /*
   *  Feed the Subject with Terms' default Values
   *  
   */
  resetBoardFilter() {
    this.pageNumber = 1;
    this.terms = {
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10,
      },
      fromWhere: 3,
    };
    this.seriveCalled();
    this.filterSubject.next();
  }

  /*
   * Event handler when the Filter Map is Emitted
  */
  onClickFilter(e) {
    this.seriveCalled();
    this.terms = Object.assign({}, this.terms, e);
    this.showHideFilter();
    this.filterSubject.next()
  }
  /*
  *  Auto Refresh that hapeens based on Refresh Rate Datay,
  *  and Emit the Filter Subject  
  */
  subscribeToRefreshRate() {
    this.subs.add(
      this.store
        .select("refreshRateData")
        .pipe(
          switchMap((refreshRateState) => {
            return interval(refreshRateState.refreshRate * 60000);
          })
        )
        .subscribe(() => {
          if (this.selectedDispatcher.id) {
            this.refreshRateService.setSettings();
            this.pageNumber = 1;
            let terms = {
              pageInfo: {
                pageNo: this.pageNumber,
                pageSize: 10
              }
            };
            // WithoutLoading
            this.applyFilterBoardTemplate(terms)
              .subscribe((res: any) => {
                if (res.length > 0) {
                  this.fetchRequestsData(res);
                } else {
                  this.alertServiceError(res);
                }
              }, (err: any) => this.alertServiceError(err))
          }
        })
    );
  }

  /*
    Event Handler for Virtual loading on Demand, On UnAssigned Orders Container
  */
  onScroll() {
    this.pageNumber++;
    let objToPost = {
      dispatcherId: this.selectedDispatcher.id,
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10
      }

    };
    let object = Object.assign({}, this.terms, objToPost,);
    this.lookup.postAllUnAssignedOrdersForDispatcher(object).subscribe(
      (res: any) => {
        if (res.isSucceeded) {
          let arr2 = res.data.data;
          this.orders.push(...arr2);
        } else {
          this.alertServiceError(res);
        }
      },
      (err) => this.alertServiceError(err)
    );
  }


  /*
    Event Hanlder for Virtual Loading on Demmand, on Teams Contaniers
  */
  onScrollTeamOrders(teamId$, teamIndex$, formanIndex$) {

    this.foremen[formanIndex$].teams[teamIndex$].pageNumber++;
    let objToPost = {
      dispatcherId: this.selectedDispatcher.id,
      teamId: teamId$,
      pageInfo: {
        pageNo: this.foremen[formanIndex$].teams[teamIndex$].pageNumber,
        pageSize: 10
      }

    }
    let object = Object.assign({}, this.terms, objToPost,);
    this.lookup.getTeamOrdersByTeamId(object).subscribe(
      (res: any) => {
        if (res.isSucceeded) {
          let arr2 = res.data.data;
          this.foremen[formanIndex$].teams[teamIndex$].filterOrders.push(...arr2);
        } else {
          this.onServerError(res);
        }
      },
      (err) => this.alertServiceError(err)
    );
  }

  /*
    Show / Hide the Filter Form
  */
  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  /*
    Handle On Error call back for Observer Subscruption
  */


  alertServiceError(error: string) {
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed'),
      detail: this.translateService.instant(`Failed due to server error.`)
    })
  }

  onServerError(error) {
    this.alertServiceError(error);
    this.checkIfServicesFinished();
  }
  /*
    Destrory all subscriptions on Subs
  */
  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onRecieveChangeRank() {
    this.sharedNotificationService.acceptChangeRankSubject.subscribe(() =>
      this.getTeamsAndOrders()
    );
  }

  CheckToSetUnassignedOrders(res) {
    if (res.isSucceeded) {
      this.setUnassignedOrders(res.data.data);
    } else {
      this.alertServiceError(res);
    }
  }

  CheckToSetAssignedOrders(res) {
    if (res.isSucceeded) {
      this.setAssignedOrders(res.data);
    } else {
      this.onServerError(res);
    }
  }

  getDispatcherUnassignedOrders() {
    let objToPost = this.terms;
    objToPost.dispatcherId = this.selectedDispatcher.id;
    objToPost.pageInfo = {
      pageNo: 1,
      pageSize: 10,
    };
    return this.lookup.postAllUnAssignedOrdersForDispatcherFilter(objToPost);

    //  subscribe(
    //       (res: any) => this.CheckToSetUnassignedOrders(res),
    //       (err: any) => this.onServerError(err));
  }

  getTeamsAndOrders() {
    this.lookup
      .getAllAssignedOrdersForDispatcher(this.selectedDispatcher.id)
      .subscribe(
        (res: any) => this.CheckToSetAssignedOrders(res),
        (err: any) => this.alertServiceError(err)
      );
  }

  /*
   angular fire Messaging, subscribe to incoming Notifications , Message Data is an order
  */
  private _receiveNotification() {
    this.notificationsService.receiveMessage().subscribe((msg: any) => {
      const order = JSON.parse(msg.data.order);
      this._searchAndUpdateOrAddOrder(order);
    });
  }

  /*
    Update / Add / Remove Order Based ON Incoming Notifications
  */
  private _searchAndUpdateOrAddOrder(order) {
    this.foremen.map((foreman: any) => {
      foreman.teams.forEach((team: any) => {
        team.filterOrders.forEach((singleOrder, i) => {
          if (singleOrder.id == order.id) {
            if (
              order.statusName == "Compelete" ||
              order.statusName == "Cancelled"
            ) {
              team.filterOrders.splice(i, 1);
            } else if (order.statusName == "On-Hold") {
              team.filterOrders.splice(i, 1);
            } else if (order.acceptanceFlag == 3) {
              team.filterOrders.splice(i, 1);
            } else {
              team.filterOrders[i] = order;
            }
          }
        });
      });
    });
    if (order.statusName == "On-Hold" || order.statusName == "Open") {
      this.orders.push(order);
    }
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

}
