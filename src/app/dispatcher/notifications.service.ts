import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';


import { BehaviorSubject } from 'rxjs';
import { LookupService } from '../api-module/services/lookup-services/lookup.service';

@Injectable({
  providedIn: 'root'
})

export class NotificationsService {

  currentMessage = new BehaviorSubject(null);

  constructor(private afMessage: AngularFireMessaging, private lookupService: LookupService) {
  }

  getPermission() {
    this.afMessage.requestPermission
      .subscribe(
        (res: any) => {
          this.afMessage.getToken
            .subscribe(
              (res: any) => {
                if (res) {
                  this.lookupService.addUserDevice(res)
                    .subscribe(
                      (res) => {
                        console.log("device registered successfully");
                      },
                      (err) => {
                        console.log(err);
                      }
                    );
                }
              }
            );
        }, (err) => {
          console.warn("notification is disabled in browser!");
        });
  }

  receiveMessage() {
    return this.afMessage.messages;
  }

}
