import { Action } from '@ngrx/store';


export const UPDATE_REFRESH_RATE = "UPDATE_REFRESH_RATE";

export class UpdateRefreshRate implements Action {
    readonly type = UPDATE_REFRESH_RATE;
    constructor(public payload: number) { }
}



export type RefreshRateActions =
    | UpdateRefreshRate;