import { NgModule } from '@angular/core';
import { userManagementModelComponent } from './userManageModel/userManageModel.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { RouterModule, Routes } from '@angular/router';
import { UserManagementContainerComponent } from './user-management-container/user-management-container.component';
import { userManagementComponent } from './user-management/user-management.component';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  { path: "", component: UserManagementContainerComponent }
];

@NgModule({
  declarations: [
    userManagementModelComponent,
    UserManagementContainerComponent,
    userManagementComponent,
  ],
  imports: [
    TranslateModule,
    SharedModuleModule,
    RouterModule.forChild(routes),
  ], entryComponents: [
    userManagementModelComponent,
  ]
})
export class UserManagementModule { }
