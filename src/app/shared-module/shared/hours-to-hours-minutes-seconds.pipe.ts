import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'HoursMinutesSeconds'
})
export class HoursToHoursMinutesSecondsPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let temp = value;
    let minutes: string;
    const hours: number = Math.floor(temp);
    let minutesNumber = Math.floor((temp - hours) * 60);
    if (minutesNumber.toString().length == 1) {
      minutes = "0" + minutesNumber
    } else {
      minutes = "" + minutesNumber;
    }
    const seconds: number = Math.floor((((temp - hours) * 60) - minutesNumber) * 60);
    return hours + ':' + minutes;
    //  + ':' + seconds;
  }

}
