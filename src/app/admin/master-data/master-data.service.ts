import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MasterDataService {

  AreasWithBlocked: any = [];
  AttendanceStatesWithBlocked: any = [];
  ContractTypesWithBlocked: any = [];
  CostCenterWithBlocked: any = [];
  DivisionWithBlocked: any = [];
  ShiftWithBlocked: any = [];
  languagesWithBlocked: any = [];
  ProblemsLocalizedWithBlocked: any = [];
  IdleTypes :any =[];
  BreakTypes:any=[]
  OrderStatusWithBlocked: any = [];
  OnHoldSubStatusWithBlocked: any = [];
  CancellationSubStatusWithBlocked: any = [];
  CompleteSubStatusWithBlocked: any = [];
  languageArray: Array<any> = [];
  CustomerTypesLocalizedWithBlocked: any = [];
  MachineTypesLocalizedWithBlocked: any = [];
  PhoneTypesLocalizedWithBlocked: any = [];



  constructor() { }
}
