import { NgModule } from '@angular/core';
import { OrderManagementReportComponent } from './order-management-report/order-management-report.component';
import { OrderManagementFilterComponent } from './order-management-report/order-management-filter/order-management-filter.component';
import { OrderManagementReportTableComponent } from './order-management-report/order-management-report-table/order-management-report-table.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    OrderManagementReportComponent,
    OrderManagementFilterComponent,
    OrderManagementReportTableComponent,
  ],
  imports: [
    TranslateModule,
    SharedModuleModule,
    RouterModule.forChild([{
      path: "",
      component: OrderManagementReportComponent
    }]),
  ]
})
export class ManagementReportModule { }
