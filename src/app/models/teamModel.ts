export class TeamModel {
  teamId: number;
  teamName: string;

  constructor(teamId: number, name: string) {
    this.teamId = teamId;
    this.teamName = name;
  }
}
