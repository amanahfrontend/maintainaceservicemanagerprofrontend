
import {map,  filter,mergeMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-title',
  templateUrl: './page-title.component.html',
  styleUrls: ['./page-title.component.css']
})
export class PageTitleComponent implements OnInit {
  title: string = null;

  constructor(private router: Router,
    public activatedRoute: ActivatedRoute,
    ) {
    this.router.events
    .pipe(
      filter(
        (event) => event instanceof NavigationEnd)
      ).pipe(map(() => this.activatedRoute),
      map(
        (route) => {
          while (route.firstChild) route = route.firstChild;
          return route;
        }
      ),).pipe(
        filter((route) => route.outlet === 'primary')
      ).pipe(mergeMap((route) => route.data))
      .subscribe((event) => {
        this.title = event['title'];
      });
   }

  ngOnInit() {
  }

}
