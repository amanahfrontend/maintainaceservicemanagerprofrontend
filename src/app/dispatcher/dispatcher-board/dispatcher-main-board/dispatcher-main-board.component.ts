import { orderDetailsUrl } from "./../../../api-module/services/globalPath";
import { teamRoutes } from "./../../../admin/team-management/team-routes";
import { DatatableComponent, id } from "@swimlane/ngx-datatable";
import { Observable, forkJoin, Subject, merge } from "rxjs";
/**
 *
 * Dispatcher Main Board Component
 *
 * Drag and drop applied via using angular material here urls
 *
 * Angular material https://material.angular.io/cdk/drag-drop/overview
 *
 * ------------------------------------------------------------------------
 * Events used
 * ===========
 *
 * (cdkDropListDropped) called when item dropped at same list or other list /
 * (cdkDragMoved) called when dragging item
 * --------------------------------------------------------------------------
 * --------------------------------------------------------------------------
 * Directives used
 * ===============
 *
 * cdkDropListGroup to target a group of lists such as assigned orders & unassigned orders /
 * cdkDropList called when target array of items such as orders /
 * cdkDrag to target which div (item) to drag
 * --------------------------------------------------------------------------
 * Inputs Used
 * ===========
 *
 *  [cdkDropListData] to pass list data /
 *  [cdkDragData] to pass item data /
 *  [cdkDragDisabled] to disable item from dragging depending on condition
 *
 */

import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ViewChildren,
  ViewContainerRef,
  QueryList,
} from "@angular/core";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { NotificationsService } from "../../notifications.service";
import { Subscription, interval } from "rxjs";
import { SharedNotificationService } from "../../../shared-module/shared/shared-notification.service";
import { MessageService } from "primeng/primeng";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import { DOCUMENT } from "@angular/common";
import { Inject } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Store, select } from "@ngrx/store";
import { AppState } from "src/app/store/refresh-rate.reducer";
import { RefreshRateService } from "src/app/services/refresh-rate.service";
import {
  switchMap,
  map,
  shareReplay,
  filter,
  mergeMap,
  tap,
  flatMap,
  single,
  find,
} from "rxjs/operators";
import { PaginatedTeamOrdersViewModel } from "src/app/models/paginatedTeamOrdersViewModel";
import { TeamModel } from "src/app/models/teamModel";
import { debug } from 'console';

@Component({
  selector: "app-dispatcher-main-board",
  templateUrl: "./dispatcher-main-board.component.html",
  styleUrls: ["./dispatcher-main-board.component.scss"],
})
export class DispatcherMainBoardComponent implements OnInit, OnDestroy {


  // Properties
  // To Manage the Spinner
  isLoading: boolean;
  numberOfServicesCalled: number = 0;
  numberOfServicesFinished: number = 0;

  @ViewChild("unassignedContainer", { static: false })
  unassignedContainer: ElementRef;

  fullScreenBoard: boolean = false;
  isFilterActive: boolean = false;
  showFilter: boolean;
  isBulkAssign: boolean;
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  defaultAssignedOrders: any[] = [];
  orders: any[] = [];
  foremen: any = [];
  subs: Subscription = new Subscription();
  filterSubject = new Subject();
  containersToBeRefreshed: any = [];


  @ViewChildren("assignedContainer", { read: ViewContainerRef })
  assignedContainer: QueryList<ViewContainerRef>;
  pageNumber: number = 1;
  terms: any = {
    fromWhere: 3,
  };
  scroll$ = new Subscription();
  dragLocation: number = 0;
  leftMoves: number = 0;
  rightMoves: number = 0;
  unAssignedOrders$: Observable<any>;
  // formensTeams$: Observable<any>;
  // teamsOrders$: Observable<any>;
  @ViewChild("assignedContainer", { static: false })
  assignedContaine: ElementRef;
  selectedDispatcher: any;

  constructor(
    private lookup: LookupService,
    private notificationsService: NotificationsService,
    public sharedNotificationService: SharedNotificationService,
    private messageService: MessageService,
    @Inject(DOCUMENT) private document: Document,
    private translateService: TranslateService,
    private refreshRateService: RefreshRateService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    //this.getFormensWithTeams();
    this.notificationsService.getPermission();
    this.receiveNotification();
    this.onRecieveChangeRank();
    this.filterSubscribtion();
    this.subscribeToRefreshRate();
    this.resetBoardFilter();
  }

  filterSubscribtion() {
    this.subs.add(
      this.filterSubject
        .pipe(switchMap(() => this.applyFilterBoardTemplate(this.terms)))
        .subscribe(
          (res: any) => {
            if (res.length > 0) {
              this.fetchRequestsData(res);
              this.checkIfServicesFinished();
            } else {
              this.onServerError(res);
            }
          },
          (err: any) => this.onServerError(err)
        )
    );
  }

  subscribeToRefreshRate() {
    this.subs.add(
      this.store
        .select("refreshRateData")
        .pipe(
          switchMap((refreshRateState) => {
            return interval(refreshRateState.refreshRate * 60000);
          })
        )
        .subscribe(() => {
          this.refreshRateService.setSettings();
          this.showHideFilter();
          this.pageNumber = 1;
          this.terms.pageInfo = {
            pageNo: this.pageNumber,
            pageSize: 10,
          };
          // WithoutLoading
          this.applyFilterBoardTemplate(this.terms)
            .subscribe((res: any) => {
              if (res.length > 0) {
                this.fetchRequestsData(res);
              } else {
                this.alertServiceError(res);
              }
            }, (err: any) => this.alertServiceError(err))
        })
    );
  }

  // used to update the board data once a notification is received
  private receiveNotification() {
    this.subs.add(
      this.notificationsService.receiveMessage().subscribe(
        (msg: any) => {
          if (msg.data.NotificationType == "assignDispatcher") {
            let order: any = JSON.parse(msg.data.Order);
            this.orders.push(order);
          } else if (msg.data.NotificationType == "unAssignDispatcher") {
            const orderId = msg.data.Order;
            let unassignedOrders = this.orders;
            this.orders = unassignedOrders.filter(
              (order) => order.id !== +orderId
            );
          } else if (msg.data.NotificationType == "New Bulk Status") {
            let order = JSON.parse(msg.data.order);
            if (order.teamId) {
              this.foremen.map((foreman) => {
                foreman.teams.forEach((team) => {
                  if (team.teamId == order.teamId) {
                    team.filterOrders.push(order);
                  }
                });
              });
              this.orders = this.orders.filter((o) => o.id != order.id);
            } else {
              this.orders.push(order);
            }
          } else {
            const order = JSON.parse(msg.data.order);
            this.searchAndUpdateOrAddOrder(order);
          }
        },
        (err: any) => this.alertServiceError(err)
      )
    );
  }

  // used to decide whether to put order in a team, or to put it in the unassigned orders
  private searchAndUpdateOrAddOrder(order) {
    this.foremen.map((foreman) => {
      foreman.teams.forEach((team) => {
        team.filterOrders.forEach((singleOrder, i) => {
          if (singleOrder.id == order.id) {
            if (
              order.statusName == "Compelete" ||
              order.statusName == "Cancelled"
            ) {
              team.filterOrders.splice(i, 1);
            } else if (order.statusName == "On-Hold") {
              team.filterOrders.splice(i, 1);
            } else if (order.acceptanceFlag == 3) {
              team.filterOrders.splice(i, 1);
            } else {
              team.filterOrders[i] = order;
            }
          }
        });
      });
    });

    if (
      order.dispatcherId &&
      (order.statusName == "On-Hold" || order.statusName == "Open")
    ) {
      this.orders.push(order);
    }
  }

  /* 
   ReLoad assigned orders of a team again when Technician changes the position 
    of an order 
  */
  onRecieveChangeRank() {
    this.subs.add(
      this.sharedNotificationService.acceptChangeRankSubject.subscribe((res) => {
        this.GetOrdersOfAteam(res.teamId);
        //this.getTeamsAndOrders()
      }
      )
    );
  }

  // start enhancments works

  // getFormensTeamsAsObservable(terms: any): Observable<any> {
  //   this.pageNumber = 1;
  //   this.terms.pageInfo = {
  //     pageNo: this.pageNumber,
  //     pageSize: 10,
  //   };
  //   const dispatcherId = +localStorage.getItem("AlghanimCurrentUserId");
  //   this.terms.dispatcherId = dispatcherId;
  //   terms = this.terms;
  //   return this.lookup.getDispatcherFormensWithTeams(dispatcherId).pipe(
  //     map((res) => res),
  //     shareReplay()
  //   );
  // }

  getTeamsOrdersAsObservable(objectToPost: any): Observable<any> {
    return this.lookup.getTeamOrdersByTeamId(objectToPost).pipe(
      map((res) => res),
      shareReplay()
    );
  }

  // getFormensWithTeams() {
  //   this.seriveCalled();
  //   const dispatcherId = localStorage.getItem("AlghanimCurrentUserId");
  //   this.terms.dispatcherId = dispatcherId;
  //   this.subs.add(
  //     this.getFormensTeamsAsObservable(dispatcherId).subscribe(
  //       (data) => {
  //         if (data.isSucceeded) {
  //           this.foremen = data.data;
  //           this.checkIfServicesFinished();
  //         }
  //       },
  //       (err: any) => this.onAPIFailure(err)
  //     )
  //   );
  // }


  // Un Used
  // getOrdersByTeamId(teamId: number): Observable<any> {
  //   this.pageNumber = 1;
  //   this.terms.pageInfo = {
  //     pageNo: this.pageNumber,
  //     pageSize: 10,
  //   };
  //   this.terms.teamId = teamId;
  //   this.terms.dispatcherId = +localStorage.getItem("AlghanimCurrentUserId");
  //   return this.getTeamsOrdersAsObservable(this.terms).pipe(map((res) => res));
  // }

  onScrollTeamOrders(teamId$, teamIndex$, formanIndex$) {
    this.foremen[formanIndex$].teams[teamIndex$].pageNumber++;
    // let objToPost = this.terms;
    let objToPost = {
      dispatcherId: +localStorage.getItem("AlghanimCurrentUserId"),
      teamId: teamId$,
      pageInfo: {
        pageNo: this.foremen[formanIndex$].teams[teamIndex$].pageNumber,
        pageSize: 10
      }
    };
    let object = Object.assign({}, this.terms, objToPost,);
    // objToPost.dispatcherId = +localStorage.getItem("AlghanimCurrentUserId");
    // objToPost.teamId = teamId$;
    // objToPost.pageInfo = {
    //   pageNo: this.foremen[formanIndex$].teams[teamIndex$].pageNumber,
    //   pageSize: 10
    // };

    this.getTeamsOrdersAsObservable(object).subscribe(
      (res: any) => {
        if (res.isSucceeded) {
          let arr2 = res.data.data;
          this.foremen[formanIndex$].teams[teamIndex$].filterOrders.push(
            ...arr2
          );
        } else {
          this.alertServiceError(res);
        }
      },
      (err) => this.alertServiceError(err)
    )

  }

  // onScrollAssignedOrders(teamId, formenId) {
  //   this.terms.dispatcherId = +localStorage.getItem("AlghanimCurrentUserId");
  //   let formen = this.foremen.filter((forman) => forman.foremanId === formenId);
  //   let teams = formen[0].teams;
  //   this.pageNumber++;
  //   this.terms.pageInfo = {
  //     pageNo: this.pageNumber,
  //     pageSize: 10,
  //   };
  //   let terms = JSON.parse(JSON.stringify(this.terms));
  //   terms.teamId = teamId;
  //   this.subs.add(
  //     this.getTeamsOrdersAsObservable(terms).subscribe((orders) => {
  //       // console.log('shareReplay',)
  //       let array = orders.data.data;
  //       // let team = teams.filter((team) => team.teamId === teamId);
  //       teams.forEach((team) => {
  //         if (team.teamId === teamId) {
  //           // team.filterOrders.push(array);
  //           team.filterOrders.push(...array);
  //         }
  //       });
  //     })
  //   );
  //   this.gettingAssignedOdersInProgress = false;
  // }


  // end  enhancment
  onScroll() {

    this.pageNumber++;
    //let objToPost = this.terms;
    // objToPost.pageInfo = {
    //   pageNo: this.pageNumber,
    //   pageSize: 10
    // };
    let objToPost = {
      dispatcherId: +localStorage.getItem("AlghanimCurrentUserId"),
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10
      }
    };
    let object = Object.assign({}, this.terms, objToPost);
    this.subs.add(
      this.lookup.postAllUnAssignedOrdersForDispatcher(object).subscribe(
        (dispatcher) => {
          let array = dispatcher.data.data;
          this.orders.push(...array);
        },
        (err: any) => this.alertServiceError(err)
      )
    );
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  getDispatcherUnassignedOrders(objToPost: any) {
    const dispatcherId = localStorage.getItem("AlghanimCurrentUserId");
    this.terms.dispatcherId = dispatcherId;
    this.pageNumber = 1;
    this.terms.pageInfo = {
      pageNo: this.pageNumber,
      pageSize: 10,
    };
    objToPost = Object.assign({}, this.terms);
    return this.lookup.postAllUnAssignedOrdersForDispatcher(objToPost);
  }

  /*
    Get all teams Orders
  */
  getTeamsAndOrders() {
    const dispatcherId = localStorage.getItem("AlghanimCurrentUserId");
    this.subs.add(
      this.lookup.getAllAssignedOrdersForDispatcher(dispatcherId).subscribe(
        (res: any) => this.CheckToSetAssignedOrders(res),
        (err: any) => this.alertServiceError(err)
      )
    );
  }

  applyFilterBoardTemplate(terms: any) {
    this.showHideFilter();
    const dispatcherId = +localStorage.getItem("AlghanimCurrentUserId");
    terms.dispatcherId = dispatcherId;
    this.terms = Object.assign({}, terms);
    let requests$: Observable<any>[] = [];

    let unAssignedOrders$ = this.getDispatcherUnassignedOrders(this.terms);
    requests$.push(unAssignedOrders$);

    let AssignedOrders$ = this.lookup.getDispatcherFormensWithTeams(
      dispatcherId
    );
    requests$.push(AssignedOrders$);

    return AssignedOrders$.pipe(
      flatMap((res: any) => {
        res.data.map((foreman) => {
          if (foreman.teams.length > 0)
            foreman.teams.map((team) => {
              let terms = JSON.parse(JSON.stringify(this.terms));
              terms.teamId = team.teamId;
              requests$.push(this.getTeamsOrdersAsObservable(terms));
            });
        });
        return forkJoin(requests$);
      })
    );
  }

  setUnassignedOrders(orders) {
    this.orders = orders || [];
    setTimeout(() => {
      this.unassignedContainer.nativeElement.scroll(0, 0);
    }, 0);
  }

  CheckToSetUnassignedOrders(res) {
    if (res.isSucceeded) {
      this.setUnassignedOrders(res.data.data);
    } else {
      this.alertServiceError(res);
    }
  }

  fetchRequestsData(data) {
    // get Unassigned Orders Data
    let unassignedOrders = data.shift();
    // Bind unAassigned Orders
    this.CheckToSetUnassignedOrders(unassignedOrders);
    // Get Assigned Orders - Foremen
    let assignedOrders = data.shift();
    // Get each forman's teams orders
    let teamsorders = data;
    // bind orders into each team's orders
    let teamId = 0;
    for (
      let i = 0;
      i < assignedOrders.data.length;
      i++ // foremen
    ) {
      for (
        let j = 0;
        j < assignedOrders.data[i].teams.length;
        j++ // foreman teams
      ) {
        let orders = teamsorders[teamId].data.data;
        assignedOrders.data[i].teams[j].pageNumber = 1;
        assignedOrders.data[i].teams[j].orders.push(...orders);
        teamId++;
      }
    }

    this.setAssignedOrders(assignedOrders);
  }

  CheckToSetAssignedOrders(res) {
    if (res.isSucceeded) {
      this.setAssignedOrders(res.data);
    } else {
      this.alertServiceError(res);
    }
  }

  setAssignedOrders(foremen) {
    this.foremen = foremen.data || [];
    this.foremen.map((forman) => {
      forman.teams.map((team: any) => {
        if (team.orders) {
          team.filterOrders = team.orders.slice();
        } else {
          team.filterOrders = [];
          team.orders = [];
        }
      });
    });
  }

  resetBoardFilter() {
    this.showHideFilter();
    this.isFilterActive = false;
    this.terms = {};
    this.pageNumber = 1;
    this.terms.pageInfo = {
      pageNo: this.pageNumber,
      pageSize: 10,
    };
    this.terms.dispatcherId = localStorage.getItem("AlghanimCurrentUserId");
    this.terms.fromWhere = 3;

    this.seriveCalled();
    this.filterSubject.next();
  }

  // onChangeRankWithoutNotification(obj) {
  //   this.toggleLoading = true;
  //   this.lookup.rankTeamOrdersWithoutNotification(obj).subscribe(res => {
  //     this.toggleLoading = false;
  //     this.getTeamsAndOrders();
  //   }, err => {
  //     this.toggleLoading = false;
  //     this.alert('error', this.translateService.instant('Failed'), this.translateService.instant(`Failed due to server error.`));
  //   });
  // }

  // /**
  //  *
  //  * @param body
  //  */
  // onChangeRankOld(body: Object) {
  //   //debugger
  //   this.gettingAssignedOdersInProgress = true;
  //   this.lookup.rankTeamOrders(body).subscribe(
  //     res => {
  //       this.gettingAssignedOdersInProgress = false;
  //       this.getTeamsAndOrders();
  //     }, (err: any) => this.onAPIFailure(err, `Can't Assign Order To Unavailable Team!`));
  // }


  // onChangeRank(body: Object) {
  //   this.gettingAssignedOdersInProgress = true;
  //   this.lookup.rankTeamOrders(body).subscribe(
  //     res => {
  //       this.gettingAssignedOdersInProgress = false;
  //       this.getTeamsAndOrders();
  //       //debugger
  //       if (!res.isSucceeded) {
  //         this.alert('error', this.translateService.instant('Failed'), this.translateService.instant(res.status.message));

  //       }
  //     }, (err: any) => this.onAPIFailure(err, `Can't Assign Order To Unavailable Team!`));
  // }


  onClickFilter(e) {
    this.terms = e;
    this.showHideFilter();
    this.seriveCalled();
    this.filterSubject.next();
    this.isFilterActive = false;
    this.showFilter = false;
  }
  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  /// The Following Functions to manage Dragging and Dropping the Orders
  /**
   * Assign order from
   * @param event
   * @param teamId
   */

  onDropOrder(
    event: CdkDragDrop<string[]>, type: string, teamId?: number, formanId?: number) {
    this.scroll$.unsubscribe();
    this.leftMoves = 0;
    this.rightMoves = 0;
    this.containersToBeRefreshed = [];

    // Order
    const item = event.item.data;
    const sameContainer = event.previousContainer === event.container ? true : false;

    if (sameContainer) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex
      );
    }

    let ordersIds = {};
    const array = event.container.data;
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      ordersIds = { ...ordersIds, ...{ [element["id"]]: index } };
    }

    if (type === "assign" && !sameContainer) {
      const body = {
        teamId: teamId,
        orderId: item.id
        // ordersIds: ordersIds,
        // formanId: formanId,
        // order: item
      };
      this.containersToBeRefreshed.push(
        // Push From Container
        {
          teamId: item.teamId
        }
        // Push To Container
        , {
          teamId: teamId
        });
      this.assignOrder(body);

    } else if (type === "assign" && sameContainer) {
      const body = {
        teamId: teamId,
        orders: ordersIds,
        orderId: item.id
      };
      if (event.currentIndex != event.previousIndex) {
        this.onChangeRank(body);
      } else {
        this.alert(
          "error",
          this.translateService.instant("Failed"),
          this.translateService.instant(
            `Please Change Order Position to store Team Rank!`
          )
        );
      }
    } else if (type === "unassign" && !sameContainer) {
      const body = {
        orderId: item.id,
      };
      this.unAssignOrder(body);
      this.containersToBeRefreshed.push(
        // Push From Container
        {
          teamId: item.teamId,
        }
        // Push To Container
        , {
          teamId: teamId
        });
    }
  }

  assignOrder(body: object) {

    this.seriveCalled();
    //this.subs.add(
    this.lookup.assginOrder(body).subscribe(
      (res) => {
        if (res.isSucceeded) {
          this.checkIfServicesFinished();
          this.onDragSucceeded();
          delete body["orderId"];

          // this.resetBoardFilter();
          // this.refreshAssignedOrders();

          // this.lookup.rankTeamOrdersWithoutNotification(body).subscribe(res => {
          //   this.toggleLoading = false;
          //   this.getTeamsAndOrders();
          // }, err => {
          //   this.toggleLoading = false;
          //   this.alert('error', 'Failed', `Failed due to server error.`);
          // });
        } else if (res.status.code == 6) {
          this.checkIfServicesFinished();
          this.filterSubject.next();
          this.alert(
            "error",
            this.translateService.instant("Failed"),
            this.translateService.instant(
              `Can't Assign Order To Unavailable Team!`
            )
          );
        } else {
          this.onServerError(res);
        }
      },
      (err: any) => this.onServerError(err)
    )
    //);
  }

  /**
   *
   * @param body
   */
  unAssignOrder(body: Object) {
    this.seriveCalled();
    this.subs.add(
      this.lookup.unAssginOrder(body).subscribe(
        (res: any) => {
          if (res.isSucceeded) {
            this.checkIfServicesFinished();
            this.onDragSucceeded();
          } else {
            this.onServerError(res);
          }
        },
        (err: any) => this.onServerError(err)
      )
    );
  }

  onChangeRank(body: Object) {
    this.seriveCalled();
    this.subs.add(
      this.lookup.rankTeamOrders(body).subscribe(
        (res) => {
          this.checkIfServicesFinished();
          let teamid = body['teamId'];
          this.pageNumber = 1;
          this.terms.pageInfo.pageNo = this.pageNumber;
          let ObjToPost = Object.assign({}, this.terms);
          ObjToPost.teamId = teamid;
          this.lookup.getTeamOrdersByTeamId(ObjToPost).subscribe(res => {
            if (res.isSucceeded) {
              let teamOrders = res.data.data;
              this.foremen.forEach(forman => {
                forman.teams.map((team: any) => {
                  if (team.teamId == teamid) {
                    this.assignedContainer.forEach((ele) => {
                      console.log("this.assignedContainer", ele);
                      if (ele.element.nativeElement.id == teamid) {
                        let selectedContainer = ele.element.nativeElement;
                        selectedContainer.scroll(0, 0);
                        team.filterOrders = [...teamOrders];
                        team.pageNumber = 1;
                      }
                    });
                  }
                });
              })
            }

          });
          //this.getTeamsAndOrders();
        },
        (err: any) =>
          this.alert(
            "error",
            this.translateService.instant("Failed"),
            this.translateService.instant(
              `Cant Assign Order to unavaliable team!`
            )
          )
      )
    );
  }
  onDragSucceeded() {
    this.pageNumber = 1;
    this.terms.pageInfo.pageNo = this.pageNumber;
    this.containersToBeRefreshed.map((obj) => {
      console.log("this.containersToBeRefreshed ", obj);
      console.log("teeee ", obj.teamId);
      let ObjToPost = Object.assign({}, this.terms);
      if (obj.teamId) {
        ObjToPost.teamId = obj.teamId;
        this.seriveCalled();
        this.lookup.getTeamOrdersByTeamId(ObjToPost).subscribe((res) => {
          this.checkIfServicesFinished();
          if (res.isSucceeded) {
            let teamOrders = res.data.data;
            console.log("teamOrders", teamOrders);
            // let forman = this.foremen.filter(
            //   (x) => x.foremanId === obj.formanId
            // );
            this.foremen.forEach(forman => {
              // if (forman.length > 0) {
              forman.teams.map((team: any) => {
                if (team.teamId == obj.teamId) {
                  this.assignedContainer.forEach((ele) => {
                    console.log("this.assignedContainer", ele);
                    if (ele.element.nativeElement.id == obj.teamId) {
                      let selectedContainer = ele.element.nativeElement;
                      selectedContainer.scroll(0, 0);
                      team.filterOrders = [...teamOrders];
                      team.pageNumber = 1;
                    }
                  });
                }
              });
              //}
            });
          }
        });
      }
      else {
        this.seriveCalled();
        this.getDispatcherUnassignedOrders({}).subscribe(
          (res: any) => {
            if (res.isSucceeded) {
              this.checkIfServicesFinished();
              this.setUnassignedOrders(res.data.data);
            } else {
              this.onServerError(res);
            }
          }, (err: any) => this.onServerError(err)
        );
      }
    });
  }

  refreshTeamOrders(teamId, formanId, order: any) {
    let forman = this.foremen.filter((x) => x.foremanId === formanId);
    forman[0].teams.map((team: any) => {
      if (team.orders && team.teamId === teamId) {
        team.filterOrders = team.orders.slice();
        team.filterOrders.push(order);

        console.log("from refresh", team.filterOrders);
      } else {
        team.filterOrders = [];
        team.orders = [];
      }
    });
    //this.getTeamsAndOrders();
    // this.refreshAssignedOrders();
  }

  refreshAssignedOrders() {
    this.showHideFilter();
    this.isFilterActive = false;
    this.terms = {};
    this.terms.pageInfo = {
      pageNo: 1,
      pageSize: 10,
    };
    this.terms.dispatcherId = localStorage.getItem("AlghanimCurrentUserId");
    this.terms.fromWhere = 3;
    // Without Loading Spinner
    this.filterSubject.next();
  }


  /**
   *
   * @param tpye
   * @param title
   * @param details
   */


  scrollSubscription() {
    this.scroll$ = interval(25).subscribe(() => {
      // console.log("inside sub");
      if (this.dragLocation > 0.9 && this.dragLocation < 0.971) {
        this.document.getElementById('technican-overflow-container').scrollBy({
          left: 50,
        });
        // console.log("in drag position", this.dragLocation);
      } else if (this.dragLocation < 0.3 && this.dragLocation > 0.186) {
        this.document.getElementById('technican-overflow-container').scrollBy({
          left: -50,
        });
        // console.log("in drag position", this.dragLocation);
      } else {
        console.log("%cOut Of Drag Area", 'padding: 5px; margin: 5px; background: #111; color: #bada55; font-size: 20px; font-weight: bold;');
      }
    });
  }

  /**
   * calculate left moves and right moves to start Horizontal scroll moving
   *
   *
   * @param event
   */
  onDragMoved(event) {
    let xPosition = event.event.pageX;
    let totalWidth = this.document.body.offsetWidth;
    this.dragLocation = xPosition / totalWidth;
  }

  /**
   * calculate left moves and right moves to start Horizontal scroll moving 
   * 
   * 
   * @param event 
   */
  onDragStart(event) {
    console.log("onDragStart", event);
    this.scrollSubscription();
  }

  alert(tpye: string, title: string, details?: string) {
    this.messageService.add({
      severity: tpye,
      summary: title,
      detail: details,
    });
  }
  /*
    Handle On Error call back for Observer Subscruption
  */


  alertServiceError(error: string) {
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed'),
      detail: this.translateService.instant(error || `Failed due to server error.`)
    })
  }

  onServerError(error) {

    if (error.status && error.status.message) {
      error = error.status.message;
    }

    this.alertServiceError(error);
    this.checkIfServicesFinished();
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished === this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  /*
   Get Orders of A team
  */
  GetOrdersOfAteam(teamid: number) {
    this.seriveCalled();
    this.terms.pageInfo.pageNo = 1;
    let ObjToPost = Object.assign({}, this.terms);
    ObjToPost.teamId = teamid;
    this.lookup.getTeamOrdersByTeamId(ObjToPost).subscribe(res => {
      if (res.isSucceeded) {
        let teamOrders = res.data.data;
        this.foremen.forEach(forman => {
          forman.teams.map((team: any) => {
            if (team.teamId == teamid) {
              this.assignedContainer.forEach((ele) => {
                console.log("this.assignedContainer", ele);
                if (ele.element.nativeElement.id == teamid) {
                  let selectedContainer = ele.element.nativeElement;
                  selectedContainer.scroll(0, 0);
                  team.filterOrders = [...teamOrders];
                  team.pageNumber = 1;
                }
              });
            }
          });
        })
      }
      this.checkIfServicesFinished();
    },
      (err: any) =>
        this.onServerError(err)
    );
  }
}
