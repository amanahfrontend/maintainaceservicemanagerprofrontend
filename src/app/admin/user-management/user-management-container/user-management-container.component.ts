import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';

@Component({
  selector: 'app-user-management-container',
  templateUrl: './user-management-container.component.html',
  styleUrls: ['./user-management-container.component.css']
})

export class UserManagementContainerComponent implements OnInit {

  // Properties

  /*
   All Items Of Roles
   Rendered using Ngfor to build the Roles Menu
  */
  itemsRoles: any[];
  getDataForRole: string;
  items: any[];

  /*
   Manage Hide/show progressSpinner
   Bound to Hidden Property 
  */
  isLoading: boolean = true;

  constructor(private lookupS: LookupService) { }

  // Methods
  /*
     called after   all data-bound properties initialized.
     Start a spinner loading.
     Call API Get all Roles.
  */
  ngOnInit() {
    this.isLoading = true;
    this.getRoles();
  }

  /*
    Get all the Roles and subscribe to the Observable
  */
  getRoles() {
    this.lookupS.getAllRoles().subscribe(result => {
      this.itemsRoles = result.data;
      this.isLoading = false;
      // Intialize the First Role
      this.getUserData(this.itemsRoles[0].name);
    },
      error => {
        console.log(error)
      }
    );
  }

  /*
    Fired When the A tab is clicked
    assign Role to GetDataForRole
  */
  getUserData(role) {
    this.getDataForRole = role;
  }


  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open'
    }
  }

  applyActiveTab(role) {
    if (this.itemsRoles.indexOf(role) == 0) {
      return 'uk-active'
    }
  }

}
