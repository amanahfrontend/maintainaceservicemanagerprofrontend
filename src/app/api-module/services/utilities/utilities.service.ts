import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import { SharedNotificationService } from '../../../shared-module/shared/shared-notification.service';

// import {Subject} from "rxjs/Subject";

@Injectable()
export class UtilitiesService {
  savedNotificationText = new BehaviorSubject(<any>'');
  previousUrl: any;
  statusColors: any =
    [
      {
        name: 'open',
        color: '#2400ff'
      },
      {
        name: 'dispatched',
        color: '#ff0000'
      },
      {
        name: 'in progress',
        color: '#ffea00'
      },
      {
        name: 'completed',
        color: '#0c1676'
      },
      {
        name: 'cancelled',
        color: '#ff1700'
      }
    ];
  growlMessage = new Subject();

  constructor(private router: Router, public sharedNotificationService: SharedNotificationService) {
    // this.router.events
    //   .filter(event => event instanceof NavigationEnd)
    //   .subscribe(e => {
    //     this.previousUrl = e['url'];
    //   });
  }

  setGrowlMessage(msg) {
    this.growlMessage.next(msg);
    this.sharedNotificationService.notificationCountSubject.next(this.sharedNotificationService.notificationCount + 1);
  }

  setSavedNotificationText(value) {
    this.savedNotificationText.next(value);
  }

  unauthrizedAction() {
    this.router.navigate(['/login']);
  }

  hideGrowl(time) {
    return setTimeout(() => {
      return [];
    }, time);
  }

  convertDatetoNormal(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${day}/${month}/${year}`;
    return formatedDate;
  }

  convertDateForSearchBinding(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${year}-${month}-${day}`;
    return formatedDate;
  }

  printComponent(elementId) {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=1000px,width=1000px');

    var divToPrint = document.getElementById(elementId);
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  setOrdersColorByState(orders) {
    return orders.map((order) => {
      let orderStatus = (order.status && order.status.name.toLowerCase()) || (order.orderStatus && order.orderStatus.name.toLowerCase());
      return this.statusColors.map((color) => {
        if (color.name == orderStatus) {
          return order.color = color.color;
        }
      });
    });
  }
}
