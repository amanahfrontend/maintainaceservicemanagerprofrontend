import { NgModule } from '@angular/core';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { ShiftingOrdersComponent } from './shifting-orders/shifting-orders.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ShiftingOrdersComponent
  ],
  imports: [
    SharedModuleModule,
    RouterModule.forChild([{
      path: "",
      component: ShiftingOrdersComponent
    }]),
    TranslateModule
  ]
})
export class OrderReAssignModule { }
