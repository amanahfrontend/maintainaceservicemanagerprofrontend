
import { Injectable, Inject } from '@angular/core';
import * as myGlobals from '../globalPath';
import { Subject, BehaviorSubject } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from '@angular/common';
import { LanguageService } from './../../../services/language.service';

@Injectable()

export class AuthenticationServicesService {
  isLoggedin = new BehaviorSubject(<boolean>false);
  userRoles = new Subject<string[]>();
  quotaionSignalR = new Subject<any>();
  callSignalR = new Subject<any>();
  orderSignalR = new Subject<any>();
  loginSubject = new BehaviorSubject<boolean>(false);
  loadNotification = new Subject<any>();

  constructor(
    private http: HttpClient,
    private translateService: TranslateService,
    @Inject(DOCUMENT) private document: Document,
    private languageService: LanguageService,
  ) { }


  setLoggedIn(value) {
    this.isLoggedin.next(value);
    this.loginSubject.next(value)
  }

  setRoles(value) {
    this.userRoles.next(value);
  }


  login(username: string, password: string, deviceId) {
    return this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.loginManagementUrl, JSON.stringify(//loginUrlUserManagement if one project 
      {
        userName: username,
        password: password,
        deviceId: deviceId
      }
    ))
  }

  storeDataOnLoginComplete(user) {
    if (user && user.data && user.data.token) {

      localStorage.setItem('AlghanimCurrentUserId', JSON.stringify(user.data.id));
      localStorage.setItem('AlghanimCurrentUser', JSON.stringify(user));

      const lngCode = user.data.languageCode.toLowerCase();
      if (lngCode == 'ur' || lngCode == 'ar' || lngCode == 'Ur' || lngCode == 'Ar') {
        this.document.body.style.direction = "rtl";
      } else {
        this.document.body.style.direction = "ltr";
      }


      this.translateService.use(user.data.languageCode.toLowerCase());
      this.setLoggedIn(true);
      this.loadNotification.next();
    }
  }

  logout() {
    localStorage.removeItem('AlghanimCurrentUser');
    this.setLoggedIn(false);
  }

  isLoggedIn() {
    if (localStorage.getItem("AlghanimCurrentUser") == null) {
      this.setLoggedIn(false);
      return false;
    }
    else {
      return true;
    }
  }

  CurrentUser() {
    if (localStorage.getItem("AlghanimCurrentUser") == null) {
      this.setLoggedIn(false);
      return null;
    }
    else {
      this.setLoggedIn(true);
      this.setRoles(JSON.parse(localStorage.getItem("AlghanimCurrentUser")).data.roles);
      return JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
    }
  }

}
