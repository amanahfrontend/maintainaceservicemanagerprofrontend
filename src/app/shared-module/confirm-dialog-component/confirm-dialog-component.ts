import { Component, Input, Inject } from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'confirm-dialog-component',
  templateUrl: './confirm-dialog-component.html',
})
export class ConfirmationDialog {
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    public dialogRef: MatDialogRef<ConfirmationDialog>) {

    if(data){
      this.message = data.message || "Are You Sure ?";
      this.cancelButtonText = data.cancelButtonText || "Cancel";
      this.confirmButtonText = data.confirmButtonText || "Confirm";
    }

  }


    public message:string;
    public cancelButtonText:string;
    public confirmButtonText:string;



}