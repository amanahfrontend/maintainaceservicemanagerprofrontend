import { AuthGuardGuard } from '../api-module/guards/auth-guard.guard';
import { EditOrderComponent } from './shared-board/edit-order/edit-order.component';
import { NotificationListComponent } from '../shared-module/shared/notification-list/notification-list.component';
import { Routes } from '@angular/router';
import { SupervisorBoardComponent } from './supervisor-board/supervisor-board.component';

export const supervisorRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardGuard],
    data: { roles: ['Supervisor'] },
    children: [
      {
        path: '',
        redirectTo: 'board',
        pathMatch: 'full',
      },
      {
        path: 'board',
        loadChildren: "./supervisor-board/supervisor-board.module#SupervisorBoardModule",
        data: { title: 'Orders Board' }
      },
      {
        path: "notifications",
        component: NotificationListComponent,
        data: { title: 'Notifications' }
      },
      {
        path: 'dispatcher',
        loadChildren: "./supervisor-board-for-dispatcher/supervisor-board-for-dispatcher.module#SupervisorBoardForDispatcherModule",
        data: { title: 'Dispatcher Board' }
      },
      {
        path: 'order-management-report',
        loadChildren: "./management-report/management-report.module#ManagementReportModule",
        data: { title: 'Order Management' }
      },
      {
        path: 'orders-reassignment',
        loadChildren: "./order-re-assign/order-re-assign.module#OrderReAssignModule",
        canActivate: [AuthGuardGuard],
        data: {
          title: 'Order Re-Assignment',
          roles: ['Supervisor']
        }
      }
    ]
  }
];
