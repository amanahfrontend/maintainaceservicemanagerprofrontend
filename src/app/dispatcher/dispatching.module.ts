import { NgModule } from '@angular/core';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { SidebarModule } from 'primeng/components/sidebar/sidebar';
import { ScheduleModule } from 'primeng/components/schedule/schedule';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { DragDropModule } from '@angular/cdk/drag-drop';


@NgModule({
  imports: [
    SharedModuleModule,
    SidebarModule,
    ScheduleModule,
    RadioButtonModule,
    CheckboxModule,
    DragDropModule,
  ],
  declarations: [
  ],
  exports: [
    SharedModuleModule,
  ]
})

export class DispatchingModule {
}
