import { AuthenticationServicesService } from './../../api-module/services/authentication/authentication-services.service';
import { AlertServiceService } from './../../api-module/services/alertservice/alert-service.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { HttpClient } from '@angular/common/http';
import * as myGlobals from '../../api-module/services/globalPath'
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { Observable, Subscription } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DOCUMENT } from '@angular/common';
// declare var particlesJS: any;
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})


export class LoginComponentComponent implements OnInit {
  responseUponLogin: any = {};
  selectedLanguage: any;
  notLoggedIn: boolean = true;
  newUser: boolean = false;
  model: any = {};
  loading = true;
  returnUrl: string;
  arr: any = [];
  languageList: any = [];
  config: any;
  currentHostType: string;
  isDataLoaded : boolean = false;
  constructor(
    private _snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private authenticationService: AuthenticationServicesService,
    private alertService: AlertServiceService,
    private afMessage: AngularFireMessaging,
    private lookUp: LookupService,
    private modalScervice: NgbModal,
    public dialog: MatDialog,
    @Inject(DOCUMENT) private document: any
  ) {
  }

  ngOnInit() {
    this.checkMyCurrentHost();

    let id = localStorage.getItem('AlghanimCurrentUserId')
    if (id) {
      this.router.navigate(['./']);
    } else {
      this.isDataLoaded = true;
      // reset login status
      this.authenticationService.logout();
      // get return url from route parameters or default to '/'
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.languagesUrl).subscribe((res: any) => {
      // console.log(res)
      if (res.isSucceeded) {
        this.languageList = res.data;
        this.loading = false;
      } else {
        console.error(res);
        this.loading = false;
      }
    }, err => {
      console.error(err);
      this.loading = false;
      this.isDataLoaded = true;
    });
    // this.languageList = [
    //   {
    //     label: "English",
    //     value: 1
    //   },
    //   {
    //     label: "Arabic",
    //     value: 2
    //   },
    //   {
    //     label: "Urdi",
    //     value: 3
    //   },
    //   {
    //     label: "Tamil",
    //     value: 4
    //   },
    // ];
    window.addEventListener('storage', (event) => {
      if (event.storageArea == localStorage) {
        let token = localStorage.getItem('AlghanimCurrentUser');
        if (token) {
          this.navigate();
        }
      }
    });
  
    
  }

  uponLoginStoreAndNavigate(data) {
    this.authenticationService.storeDataOnLoginComplete(this.responseUponLogin);
    setTimeout(() => {
      this.navigate();
    }, 0);
  
  
  
  
  }

  navigate() {
    let data: any = JSON.parse(localStorage.getItem("AlghanimCurrentUser"));
    if (data.data.roles) {
      if (data.data.roles[0] == 'Dispatcher') {
        this.router.navigate(['/dispatcher']);
      } else if (data.data.roles[0] == 'Supervisor') {
        this.router.navigate(['/supervisor']);
      } else if (data.data.roles[0] == 'Admin') {
        this.router.navigate(['/admin']);
      } else if (data.data.roles[0] == "Technician") {
        this.alertService.error('This is a technician!');
        localStorage.removeItem('AlghanimCurrentUser');
        localStorage.removeItem('AlghanimCurrentUserId');
        this.authenticationService.setLoggedIn(false);
        this.notLoggedIn = true;
      }
      else {
        this.router.navigate(['./' + this.returnUrl]);
      }
      this.authenticationService.userRoles.next(data.data.roles[0]);
    }
  }

  cancelLogin() {
    this.notLoggedIn = true;
  }

  onSubmitLanguage() {
    let objToPost = {
      UserId: this.responseUponLogin.data.userId,
      LanguageId: this.selectedLanguage.id
    };
    this.loading = true;
    this.http.post(myGlobals.BaseUrlUserManagementNew + myGlobals.submitLanguageUrlManagement, objToPost).subscribe((res: any) => {
      if (res.isSucceeded) {
        this.loading = false;
        this.responseUponLogin.data.languageId = this.selectedLanguage.id;
        this.uponLoginStoreAndNavigate(this.responseUponLogin);
      } else {
        console.error(res);
      }
    }, err => {
      console.error(err);
    })
  }

  onSubmitCredentialsSuccess(data) {
    if (data.isSucceeded) {
      this.responseUponLogin = data;
      this.loading = false;
      this.newUser = data.data.isFirstLogin;
      this.notLoggedIn = false;
      if (!this.newUser) {
        this.uponLoginStoreAndNavigate(data);
      }
    } else {
      this.onSubmitCredentialsFailed(data);
    }
  }

  onSubmitCredentialsFailed(error) {
    //debugger
    console.warn(error);
    let errorMessager = "Incorrect Username or Password"
    //userNotFound 
    if (error.status.code == 3) {
      errorMessager = errorMessager;
    }
    // this.alertService.error('Incorrect Username or Password');
    this._snackBar.open(errorMessager, "", {
      duration: 5000,
      verticalPosition: 'top',
      panelClass: ['red-snackbar']
    });


    this.loading = false;
  }

  onClickLogin() {
    this.loading = true;
    this.afMessage.getToken
      .subscribe(
        (res: any) => {
          localStorage.setItem("deviceId", res);
          this.authenticationService.login(this.model.username, this.model.password, res)
            .subscribe(
              (data: any) => this.onSubmitCredentialsSuccess(data),
              error => this.onSubmitCredentialsFailed(error));
        },
        err => {
          console.warn("notification is disabled in browser!");
          localStorage.setItem("deviceId", "");
          this.authenticationService.login(this.model.username, this.model.password, "")
            .subscribe(
              (data: any) => this.onSubmitCredentialsSuccess(data),
              error => this.onSubmitCredentialsFailed(error));
        },
      );
  }

  /**
   * UAT
   * dispatcher-engineering-dev.chevrolet-autline-dev.p.azurewebsites.net
   * 
   * Staging
   * dispatcher-engineering-dev-staging.chevrolet-autline-dev.p.azurewebsites.net
   */
  checkMyCurrentHost() {
    const host = this.document.location.host;
    if (host == "dispatcher-engineering-dev.chevrolet-autline-dev.p.azurewebsites.net") {
      this.currentHostType = "UAT";
      // particlesJS.load('particles-js', 'assets/particles/particles-three.json');
    } else if (host == "dispatcher-engineering-dev-staging.chevrolet-autline-dev.p.azurewebsites.net") {
      this.currentHostType = "Staging";
      // particlesJS.load('particles-js', 'assets/particles/particles-two.json');
    } else if (this.document.location.hostname == 'localhost') {
      this.currentHostType = "Local";
      // particlesJS.load('particles-js', 'assets/particles/particles-one.json');
    } else {

    }
  }

}
