import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { dispatcherRoutes } from './dispatcher-routes';
import { NotificationsService } from './notifications.service';
import { DispatchingModule } from './dispatching.module';
import { PrintoutComponent } from './printout/printout.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    DispatchingModule,
    RouterModule.forChild(dispatcherRoutes),
    TranslateModule
  ],
  declarations: [
    PrintoutComponent
  ],
  providers: [
    NotificationsService
  ]
})

export class DispatcherModule {
}
