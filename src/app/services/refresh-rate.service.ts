import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRefreshRate from '../store/refresh-rate.reducer';
import * as RefreshRateActions from '../store/refresh-rate.actions';
import { LookupService } from '../api-module/services/lookup-services/lookup.service';

@Injectable({
  providedIn: 'root'
})
export class RefreshRateService {

  constructor(
    private store: Store<fromRefreshRate.AppState>,
    private lookupService: LookupService,
  ) { }


  setSettings() {
    // this.store.select("refreshRateData").subscribe((refreshRateState) => {
    this.lookupService.getMapSettings(1)
      .subscribe(
        res => {
          if (res && res.isSucceeded) {
            const currentRefreshRate = +localStorage.getItem('refreshRate');
            const newRefreshRate = +res.data.refresh_Rate;
            if (newRefreshRate != currentRefreshRate) {
              localStorage.setItem('refreshRate', ("" + newRefreshRate));
              this.store.dispatch(new RefreshRateActions.UpdateRefreshRate(newRefreshRate))
            }
            localStorage.setItem('webApiKey', res.data.web_Api_Key);
          }
        }, err => {
          console.error(err);
        }
      );
    // })
  }
}
