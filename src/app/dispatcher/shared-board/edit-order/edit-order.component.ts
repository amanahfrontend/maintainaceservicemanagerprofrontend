
/// <reference path="../../../../../node_modules/@types/googlemaps/index.d.ts"/>

import { Component, OnDestroy, OnInit, NgZone, ViewChild, ElementRef, ChangeDetectorRef, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, forkJoin } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from 'primeng/primeng';
import { MapsAPILoader } from '@agm/core';
import { FormControl, NgForm } from '@angular/forms';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { Output, EventEmitter } from '@angular/core';
import { HttpEventType, } from '@angular/common/http';

declare const google: any;

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit, OnDestroy {
  loadingPopUp: any;
  toggleLoading: boolean;
  orderDetails: any = {};
  originalOrderDetails: any = {};
  activatedRouteSubscription: Subscription;
  allPriorities: any[];
  allStatus: any[];
  allCompanyCode: any[];
  allProblem: any[];
  allDivisions: any[];
  allTypes: any[];
  togglePaciLoading: boolean;
  showChangeSubStatusPopUp: boolean;
  subStatuses: any = [];
  previousStatusId: string;
  GStatusName: string;
  GSubStatusName: string;
  GStatusID: number;
  GSubStatusID: number;
  mapView: string = 'roadmap';
  showError: boolean = false;
  lat: number = 29.378586;
  long: number = 47.990341;
  zoom: number = 8;
  searchBox: any;
  // used to show a popup window containing the google map to search for a location
  show_map: boolean = false;

  // search for location on google maps
  public searchControl: FormControl;

  // we use this property to control the supervisor access on edit this order
  currentUserRoles: string[] = [];

  @ViewChild('search', { static: false }) searchElementRef: ElementRef;
  @ViewChild('editOrderForm', { static: false }) ngForm: NgForm;
  disabledUntilEdit: boolean = true;
  public CurrentUserHasDispatcherRole: boolean;
  public CurrentUserHasSupervisorRole: boolean;

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private lookupService: LookupService,
    private modelService: NgbModal,
    private cd: ChangeDetectorRef,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private translateService: TranslateService
  ) {
  }

  ngOnInit() {
    this._getViewData();
    this.searchMap();
    this.currentUserRoles = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.roles;
    this.CurrentUserHasDispatcherRole = this.currentUserRoles.includes('Dispatcher');
    this.CurrentUserHasSupervisorRole = this.currentUserRoles.includes('Supervisor');
  }

  onServerError(err) {
    this.showChangeSubStatusPopUp = false;
    this.show_map = false;
    this.toggleLoading = false;
    console.error(err);
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed'),
      detail: this.translateService.instant(`Failed due to server error.`)
    });
  }


  onKeyDown(e) {
    if (e.key == "Enter") {
      // Create the search box and link it to the UI element.
      var input = document.getElementById('txtsearchinput');
      if (!this.searchBox) {
        this.searchBox = new google.maps.places.SearchBox(input);

        this.searchBox.addListener('places_changed', () => {
          var places = this.searchBox.getPlaces();
          if (places.length > 0) {
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            let firstPlace = places[0];
            if (!firstPlace.geometry) {
              console.log("Returned place contains no geometry");
              return;
            } else {
              this.disableButtonsInMap = false;
              this.lat = firstPlace.geometry.location.lat();
              this.long = firstPlace.geometry.location.lng();
              if (!this.cd['destroyed'])
                this.cd.detectChanges();
            }
          }
        });
      }
    }
  }


  searchMap() {
    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        keywords: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.orderDetails.lat = this.lat = place.geometry.location.lat();
          this.orderDetails.long = this.long = place.geometry.location.lng();

        });
      });
    });

  }


  private _reloadOrderObject() {
    this.toggleLoading = true;
    this._getOrderDetails(this.orderDetails.id).subscribe(res => {
      if (this.orderDetails.lat) {
        this.lat = this.orderDetails.lat;
      }
      if (this.orderDetails.long) {
        this.long = this.orderDetails.long;
      }
      if (this.orderDetails.long && this.orderDetails.lat) {
        this.zoom = 15;
      }
      this.toggleLoading = false;
    })
  }



  private _getViewData() {
    this.toggleLoading = true;
    this.activatedRouteSubscription = this.activatedRoute.params
      .subscribe(
        (params) => {
          forkJoin([
            this._getOrderDetails(+params.orderId),
            this._getPriority(),
            this._getStatus(),
            this._getCompanyCode(),
            this._getProblem(),
            this._getDivision(),
            this._getAllOrderTypes(),
          ]).subscribe(([res1, res2, res3, res4, res5, res6, res7]) => {
            this.previousStatusId = res1.data.statusId;
            this.orderDetails = res1.data;
            this.originalOrderDetails = JSON.parse(JSON.stringify(res1.data));
            if (this.orderDetails.lat) {
              this.lat = this.orderDetails.lat;
            }
            if (this.orderDetails.long) {
              this.long = this.orderDetails.long;
            }
            if (this.orderDetails.long && this.orderDetails.lat) {
              this.zoom = 15;
            }
            this.allPriorities = res2.data;
            this.allStatus = res3.data;
            this.allCompanyCode = res4.data;
            this.allProblem = res5.data;
            this.allDivisions = res6.data;
            this.allTypes = res7.data;
            this.toggleLoading = false;
          },
            ([err1, err2, err3, err4, err5, err6, err7]) => {
              this.onServerError([err1, err2, err3, err4, err5, err6, err7]);
            }
          );
        }
      );
  }

  onStatusChange(item) {
    this.GStatusName = item.statusName;
    let statusId = item.statusId;
    if (statusId == 6 || statusId == 7 || statusId == 8) {
      this.toggleLoading = true;
      return this.lookupService.getAllSubOrderStatus(statusId)
        .subscribe(
          (res: any) => {
            this.showChangeSubStatusPopUp = true;
            this.showError = false;
            this.orderDetails.subStatusId = null;
            this.orderDetails.serveyReport = "";
            this.toggleLoading = false;
            this.subStatuses = res.data;
          }, err => this.onServerError(err));
    }
  }

  private _getOrderDetails(orderId) {
    return this.lookupService.getOrderDetails(orderId);
  }

  private _getPriority() {
    return this.lookupService.getAllPriority();
  }

  private _getStatus() {
    return this.lookupService.getAllStatus();
  }

  private _getCompanyCode() {
    return this.lookupService.getCompanyCode();
  }

  private _getProblem() {
    return this.lookupService.getAllProblems();
  }

  private _getDivision() {
    return this.lookupService.getAllDivisions();
  }

  private _getAllOrderTypes() {
    return this.lookupService.getAllOrderTypes();
  }

  getLoctionByPaci() {
    const paciCode = "" + this.orderDetails.paci;
    if (paciCode.length == 8) {
      this.togglePaciLoading = true;
      this.lookupService.getLocationByPaci(this.orderDetails.paci)
        .subscribe(
          (res: any) => {
            this.orderDetails.govName = res.governorate.name;
            // this.orderDetails.govId = res.governorate.id;
            this.orderDetails.areaName = res.area.name;
            // this.orderDetails.areaId = res.area.id;
            this.orderDetails.blockName = res.block.name;
            // this.orderDetails.blockId = res.block.id;
            this.orderDetails.streetName = res.street.name;
            // this.orderDetails.streetId = res.street.id;
            this.orderDetails.lat = JSON.parse(JSON.stringify(res.lat));
            this.lat = JSON.parse(JSON.stringify(res.lat));
            this.originalOrderDetails.lat = JSON.parse(JSON.stringify(res.lat));
            this.orderDetails.long = JSON.parse(JSON.stringify(res.lng));
            this.long = JSON.parse(JSON.stringify(res.lng));
            this.originalOrderDetails.long = JSON.parse(JSON.stringify(res.lng));
            this.orderDetails.floor = res.floorNumber;
            this.orderDetails.houseKasima = res.buildingNumber;
            this.orderDetails.appartmentNo = res.appartementNumber;
            this.togglePaciLoading = false;
          },
          err => {
            this.onServerError(err)
            this.togglePaciLoading = false;
          }
        );
    }
  }

  onOpenMap() {
    this.show_map = true;
    document.documentElement.style.overflow = 'hidden';
    this.disableButtonsInMap = this.orderDetails.paci == this.originalOrderDetails.paci;
  }

  closeModal() {
    this.show_map = false;
    this.toggleLoading = false;
    document.documentElement.style.overflow = 'unset';
  }

  closeModalAndResetMap() {
    this.show_map = false;
    this.toggleLoading = false;
    document.documentElement.style.overflow = 'unset';
    const originalData = JSON.parse(JSON.stringify(this.originalOrderDetails));
    this.lat = this.orderDetails.lat = originalData.lat;
    this.long = this.orderDetails.long = originalData.long;
  }

  onSubmit() {
    return false;
  }

  private _prepareOrderToPost() {
    const orderDataToPost = Object.assign({}, this.orderDetails);
    orderDataToPost.statusName = this.allStatus
      .find((element) => element.id == this.orderDetails.statusId)['name'];
    orderDataToPost.problemName = this.orderDetails.problemName;
    orderDataToPost.typeName = this.allTypes
      .find((element) => element.id == this.orderDetails.typeId)['name'];
    orderDataToPost.OrderTypeCode = this.allTypes
      .find((element) => element.id == this.orderDetails.typeId)['code'];

    orderDataToPost.priorityName = this.allPriorities
      .find((element) => element.id == this.orderDetails.priorityId)['name'];

    // orderDataToPost.companyCodeName = this.allCompanyCode.
    //   find((element:any) => {
    //     return element.name == this.orderDetails.companyCodeName
    //   }).name;
    orderDataToPost.divisionName = this.allDivisions.
      find((element) => element.id == this.orderDetails.divisionId)['name'];
    return orderDataToPost;
  }

  checkTosaveOrderEdits() {
    this.closeModal();
    if (this.orderDetails.teamId) {
      this.submitData();
    } else if (this.orderDetails.statusId == 1 || this.orderDetails.statusId == 6 || this.orderDetails.statusId == 8) {
      this.submitData();
    } else {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('error'),
        detail: `${this.translateService.instant('Order has no team to update to') + this.allStatus.filter((status) => status.id == this.orderDetails.statusId)[0].name + this.translateService.instant(' status')}`
      })
    }
  }

  submitData() {
    this.toggleLoading = true;
    const preparedOrderToPost = this._prepareOrderToPost();
    this.lookupService.updateOrder(preparedOrderToPost)
      .subscribe(
        (res: any) => {
          if (res.isSucceeded) {
            this.originalOrderDetails = JSON.parse(JSON.stringify(this.orderDetails));
            this.onUpdateSucceeded();
            this._reloadOrderObject();
          } else {
            this.onUpdateFailed(res);
          }
        }, err => this.onServerError(err));
  }

  onUpdateFailed(res) {
    if (res.status && res.status.subCode == 1) {
      this.unableToUpdateOrder(`${this.translateService.instant('Order has no team to update to') + this.allStatus.filter((status) => status.id == this.orderDetails.statusId)[0].name + this.translateService.instant(' status')}`);
    } else if (res.status && res.status.subCode == 2) {
      this.unableToUpdateOrder(this.translateService.instant('Can\'t edit closed order'));
    } else {
      this.onServerError(res);
    }
  }

  onUpdateSucceeded() {
    if (this.orderDetails.statusId == 1 || this.orderDetails.statusId == 6) {
      this.orderDetails.teamId = null;
    }
    this.show_map = false;
    this.toggleLoading = false;
    this.showChangeSubStatusPopUp = false;
    this.messageService.add({
      severity: 'success',
      summary: this.translateService.instant('Successful!'),
      detail: this.translateService.instant(`Order number `) + this.orderDetails.code + this.translateService.instant(` Edited & Saved Successfully!`)
    });
    this.ngForm.form.markAsPristine();
  }

  unableToUpdateOrder(msgDetails) {
    this.show_map = false;
    this.toggleLoading = false;
    this.showChangeSubStatusPopUp = false;
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('error'),
      detail: msgDetails
    })
  }


  onToggleMap() {
    if (this.mapView == 'roadmap') {
      this.mapView = 'hybrid';
    } else {
      this.mapView = 'roadmap';
    }
  }


  ngOnDestroy() {
    this.activatedRouteSubscription.unsubscribe();
  }

  checkTosaveOrderEditsAndGoBack() {
    this.closeModal();
    if (this.orderDetails.teamId) {
      this.saveOrderEditsAndGoBack();
    } else if (this.orderDetails.statusId == 1 || this.orderDetails.statusId == 6 || this.orderDetails.statusId == 8) {
      this.saveOrderEditsAndGoBack();
    } else {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('error'),
        detail: `${this.translateService.instant('Order has no team to update to') + this.allStatus.filter((status) => status.id == this.orderDetails.statusId)[0].name + this.translateService.instant(' status')}`
      })
    }
  }

  saveOrderEditsAndGoBack() {
    this.toggleLoading = true;
    const preparedOrderToPost = this._prepareOrderToPost();
    this.lookupService.updateOrder(preparedOrderToPost)
      .subscribe(
        (res: any) => {
          if (res.isSucceeded) {
            this.onUpdateSucceeded();
            this.router.navigate(["../../"]);
          } else {
            this.onUpdateFailed(res);
          }
        },
        err => this.onServerError(err));
  }

  onCancelChangeStatus() {
    this.orderDetails.statusId = this.previousStatusId;
    this.showChangeSubStatusPopUp = false;
  }

  checkToChangeStatus() {
    this.showError = true;
    if (this.orderDetails.subStatusId && this.orderDetails.serveyReport) {
      this.closeModal();
      if (this.orderDetails.teamId) {
        this.onChangeStatus();
      } else if (this.orderDetails.statusId == 1 || this.orderDetails.statusId == 6 || this.orderDetails.statusId == 8) {
        this.onChangeStatus();
      } else {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('error'),
          detail: `${this.translateService.instant('Order has no team to update to') + this.allStatus.filter((status) => status.id == this.orderDetails.statusId)[0].name + this.translateService.instant(' status')}`
        })
      }
    }
  }

  onChangeStatus() {
    this.toggleLoading = true;
    let objToPost = {
      orderId: this.orderDetails.id,
      statusId: this.orderDetails.statusId,
      subStatusId: this.orderDetails.subStatusId,
      serveyReport: this.orderDetails.serveyReport,
      statusName: this.GStatusName
    };
    this.lookupService.changeOrderStatus(objToPost)
      .subscribe(
        (res: any) => {
          if (res.isSucceeded) {
            this.onUpdateSucceeded();
          } else {
            this.onUpdateFailed(res);
          }
        },
        err => this.onServerError(err));
  }

  onSaveChangeStatusAndGoBack() {
    this.toggleLoading = true;
    let objToPost = {
      orderId: this.orderDetails.id,
      statusId: this.orderDetails.statusId,
      subStatusId: this.orderDetails.subStatusId,
      serveyReport: this.orderDetails.serveyReport
    };
    this.lookupService.changeOrderStatus(objToPost)
      .subscribe(
        (res: any) => {
          if (res.isSucceeded) {
            this.onUpdateSucceeded();
            this.router.navigate(["../../"]);
          } else {
            this.onUpdateFailed(res);
          }
        },
        err => this.onServerError(err));
  }


  checkToSaveStatusAndGoBack() {
    this.showError = true;
    if (this.orderDetails.teamId) {
      this.onSaveChangeStatusAndGoBack();
    } else if (this.orderDetails.statusId == 1 || this.orderDetails.statusId == 6 || this.orderDetails.statusId == 8) {
      this.onSaveChangeStatusAndGoBack();
    } else {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('error'),
        detail: `${this.translateService.instant('Order has no team to update to') + this.allStatus.filter((status) => status.id == this.orderDetails.statusId)[0].name + this.translateService.instant(' status')}`
      })
    }
  }



  disableButtonsInMap: boolean = true;

  placeMarker(event) {
    if (this.CurrentUserHasSupervisorRole == false) {
      this.lat = event.coords.lat;
      this.long = event.coords.lng;
      this.orderDetails.lat = this.lat;
      this.orderDetails.long = this.long;
      this.disableButtonsInMap = false;
    }
  }

  onChanges(): void {
    this.ngForm.valueChanges.subscribe(val => {
      console.log(val);
    });
  }

  /**
   * 
   * @param value 
   */
  onInputChange(value: string): void {
    console.log(value.length);

    if (value.length != 0) {
      this.disabledUntilEdit = false;
    }
  }



  public progress: number;
  public message: string;
  @Output() public onUploadFinished = new EventEmitter();

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('Name', fileToUpload.name);
    formData.append('TileImage', fileToUpload);

    debugger;
    this.http.post('http://localhost:9999/api/Upload', formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Accept': 'application/json',

        },
        reportProgress: true,
        observe: 'events'
      },
    )
      .subscribe(event => {
        // if (event.type === HttpEventType.UploadProgress)
        //   this.progress = Math.round(100 * event.loaded / event.total);
        // else if (event.type === HttpEventType.Response) {
        //   this.message = 'Upload success.';
        //   this.onUploadFinished.emit(event.body);
        // }
      });
  }














}
