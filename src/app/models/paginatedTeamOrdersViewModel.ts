export class PaginatedTeamOrdersViewModel {
  teamId: number;
  dispatcherId: number;
  dateFrom: Date = null;
  dateTo: Date = null;
  orderCode: string = null;
  customerName: string = null;
  customerCode: string = null;
  customerPhone: string = null;
  areaIds: number[] = [];
  blockName: string = null;
  problemIds: number[] = [];
  statusIds: number[] = [];
  rejection: number = 0;
  pageInfo: any = {};
}
