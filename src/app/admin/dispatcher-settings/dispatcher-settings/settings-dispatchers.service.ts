import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class SettingsDispatchersService {

  constructor() { }
  closeModal = new Subject();
}


// import { Injectable } from '@angular/core';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// @Injectable({
//   providedIn: 'root'
// })
// export class SettingsDispatchersService {

//   constructor() { }
//   closeModal = new BehaviorSubject(null);
// }

