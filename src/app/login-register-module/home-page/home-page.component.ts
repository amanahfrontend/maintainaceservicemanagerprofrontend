import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from "primeng/components/common/message";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  routerName: string;
  msg: Message[];

  constructor(private router: Router) { }

  ngOnInit() {
    this.assignHeaderName(this.router.url);
    this.router.events
      .subscribe(
        (e: any) => {
          this.assignHeaderName(e.url);
        }
      );
  }

  assignHeaderName(name) {
    switch (name) {
      case "/master-data":
        this.routerName = "Master Data";
        break;
      case "/user-management":
        this.routerName = "User Management";
        break;
      case "/team-management":
        this.routerName = "Team Management";
        break;
      case "/dispatcher-settings":
        this.routerName = "Dispatcher Settings";
        break;
      case "/timesheet":
        this.routerName = "Time Sheet";
        break;
      case "/map-settings":
        this.routerName = "Map Settings";
        break;
    }
  }

}