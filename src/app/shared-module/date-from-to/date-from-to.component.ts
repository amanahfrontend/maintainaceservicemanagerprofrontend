import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-date-from-to',
  templateUrl: './date-from-to.component.html',
  styleUrls: ['./date-from-to.component.css']
})

export class DateFromToComponent implements OnInit {
  @Input() submitted: boolean;
  @Input() required: boolean;
  @Input() layout: string;
  @Input() startDateLable: string;
  @Input() endDateLable: string;

  date: any = {};
  layoutClass: string;
  @Output() startDate = new EventEmitter();
  @Output() endDate = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.layoutClass = this.layout;
  }

  emitStartDate() {
    this.startDate.emit(this.date.startDate);
  }

  emitEndDate() {
    this.endDate.emit(this.date.endDate);
  }
}
