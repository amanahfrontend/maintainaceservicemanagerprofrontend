import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/primeng';
import { pagesItemSharedService } from '../../../api-module/pagesItemsShared/pagesItemsShared.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-master-data-container',
  templateUrl: './master-data-container.component.html',
  styleUrls: ['./master-data-container.component.css']
})

export class MasterDataContainerComponent implements OnInit {
  items = [];
  itemsRoles: string[] = [];
  getDataForType: string;
  getUserDataFlag: boolean;
  getDataForRole: string;

  constructor(private pagesItemSharedService: pagesItemSharedService,
    private messagingService: MessageService,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.pagesItemSharedService.getItemsValues();
    this.items = [
      'Area',
      'Attendance Status',
      'Contract Type',
      'Cost Center',
      'Division',
      'Shift',
      'Languages',
      'Problems',
      'Idle Types',
      'Break Types',
      'Order Status',
      'On-Hold Sub-Status Types',
      'Order Cancellation Reasons',
      'Complete Sub-Status Types',
      'Customer Types',
      'Machine Types',
      'Phone Types',

    ];
    this.getUserDataFlag = false;
    this.getData('Area');
  }

  getData(type) {
    this.getDataForType = type;
  }

  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open'
    }
  }

  applyActiveTab(role) {
    if (this.itemsRoles.indexOf(role) == 0) {
      return 'uk-active'
    }
  }


  /**
   * ? show pop up to delete orders
   */
  deleteAllOrders() {
    this.messagingService.add({
      key: 'blockItem',
      sticky: true,
      severity: 'error',
      summary: this.translateService.instant(`Are you sure you want to delete all orders?`),
      detail: this.translateService.instant('Confirm to proceed'),
      data: {
        action: "deleteAllOrders",
      }
    });
  }

  /**
   * ? show pop up to delete user settings
   */
  deleteUserManagement() {
    this.messagingService.add({
      key: 'blockItem',
      sticky: true,
      severity: 'error',
      summary: this.translateService.instant(`Are you sure you want to delete all users?`),
      detail: this.translateService.instant('Confirm to proceed'),
      data: {
        action: "deleteUserManagement",
      }
    });
  }

  /**
   * ? show pop up to delete dispatcher settings
   */
  deleteDispatcherSettings() {
    this.messagingService.add({
      key: 'blockItem',
      sticky: true,
      severity: 'error',
      summary: this.translateService.instant(`Are you sure you want to delete all dispatcher settings?`),
      detail: this.translateService.instant('Confirm to proceed'),
      data: {
        action: "deleteDispatcherSettings",
      }
    });
  }


}