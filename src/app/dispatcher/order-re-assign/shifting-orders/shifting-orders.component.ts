import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { forkJoin } from 'rxjs';
import { SelectItemGroup, MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-shifting-orders',
  templateUrl: './shifting-orders.component.html',
  styleUrls: ['./shifting-orders.component.css']
})

export class ShiftingOrdersComponent implements OnInit, AfterViewInit {
  orders: any = [
    {
      code: "",
      gettingOrderData: false,
      valid: ""
    }
  ];
  assignTo: SelectItemGroup[] = [];
  dispatchers = [];
  isSubmitting: boolean = false;
  constructor(
    private lookupService: LookupService,
    private messageService: MessageService,
    public cd: ChangeDetectorRef,
    private translateService: TranslateService) {
  }

  ngOnInit() {
    let dispatchers: any = [];
    let teams: any = [];
    forkJoin(
      this.lookupService.getAllDispatchers(),
      this.lookupService.getAllTeamsWithOutMembers()
    ).subscribe(
      ([res1, res2]) => {
        res1.data.map((disp) => {
          dispatchers.push({
            value: {
              isTeam: false,
              dispatcherId: disp.id,
              dispatcherName: disp.fullName,
              teamId: null,
              supervisorId: disp.supervisorId,
              supervisorName: disp.supervisorName
            },
            label: disp.fullName
          });
          this.dispatchers = dispatchers;
        });
        res2.data.map((team) => {
          teams.push({
            value: {
              status: team.statusId,
              isTeam: true,
              dispatcherId: team.dispatcherId,
              dispatcherName: team.dispatcherName ? '' : team.dispatcherName,
              teamId: team.id,
              supervisorId: team.supervisorId,
              supervisorName: team.supervisorName ? '' : team.supervisorName
            },
            label: team.name
          });
        });
        this.assignTo = [
          {
            label: 'Dispatchers',
            items: dispatchers
          },
          {
            label: 'Teams',
            items: teams
          },
        ];
      }, ([err1, err2]) => {
        console.error(err1);
        console.error(err2);
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed'),
          detail: this.translateService.instant(`Failed to load data due to server error.`)
        });
      }
    )
  }

  ngAfterViewInit() {
    // the line below is commented as it solve ng serve error that's not in production
    // this.cd.detectChanges();
  }

  getOrderByCode(code, index) {
    this.orders[index].status = "";
    this.orders[index].dispatcherName = "";
    this.orders[index].techName = "";
    this.orders[index].reAssignStatus = "";
    const length = ("" + code).length;
    if (length > 9) {
      console.log(code, index);
      this.orders[index].disabled = true;
      this.orders[index].gettingOrderData = true;
      this.orders.forEach((order, loopIndex) => {
        if (order.code && order.code == code && (index != loopIndex)) {
          this.messageService.add({
            severity: 'error',
            detail: `${this.translateService.instant('The order code')} ${order.code} ${this.translateService.instant('is dublicated , please change it !')}`
          });
          //disable removing row
          // this.orders.pop();
        }
      });
      this.lookupService.getOrderByCode(code).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.orders[index].status = res.data.statusName;
          this.orders[index].statusId = res.data.statusId;
          this.orders[index].dispatcherName = res.data.dispatcherName || 'none';
          this.orders[index].techName = res.data.techName || 'none';
          if ((res.data.statusId == 1 || res.data.statusId == 6)) {
            this.orders[index].valid = "valid";
            this.orders[index].id = res.data.id;
            if (!res.data.lat || !res.data.long) {
              this.orders[index].reAssignStatus = this.translateService.instant(`Order can be assigned to a dispatcher only (no co-ordinates)`);
            } else {
              this.orders[index].reAssignStatus = this.translateService.instant("Submit To Re-assign");
            }
          } else {
            this.orders[index].disabled = false;
            this.orders[index].valid = 'invalid';
            if (res.data.statusId == 7) {
              this.orders[index].reAssignStatus = this.translateService.instant("Order status is Completed");
            } else if (res.data.statusId == 8) {
              this.orders[index].reAssignStatus = this.translateService.instant("Order status is Cancelled");
            } else if (res.data.statusId == 5 || res.data.statusId == 4 || res.data.statusId == 3 || res.data.statusId == 2) {
              this.orders[index].reAssignStatus = ` ${this.translateService.instant('Order already assigned to')} ${res.data.techName || res.data.dispatcherName}`;
            }
          }
        } else {
          this.orders[index].disabled = false;
          this.orders[index].valid = 'invalid';
          this.orders[index].reAssignStatus = this.translateService.instant("Incorrect Order Code! Kindly re-type it");
        }
        console.log(res);
        this.orders[index].gettingOrderData = false;
      }, (err) => {
        console.error(err);
        this.orders[index].disabled = false;
        // this.orders[index].valid = 'invalid';
        this.orders[index].gettingOrderData = false;
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed'),
          detail: this.translateService.instant(`Failed due to server error.`)
        });
      });
    }
  }

  onAddOrder() {
    this.orders.push({
      code: "",
      gettingOrderData: false,
      valid: ""
    });
    return;

    // let lastOrderLength = 0;
    // if (this.orders.length > 0) {
    //   lastOrderLength = ("" + this.orders[this.orders.length - 1].code).length;
    // }
    // if (this.orders.length == 0 || this.orders[this.orders.length - 1].valid == 'valid' && this.orders[this.orders.length - 1].assignedTo) {
    //   this.orders.push({
    //     code: "",
    //     gettingOrderData: false,
    //     valid: ""
    //   });
    // }
    //  else if (lastOrderLength < 10) {
    //   this.messageService.add({
    //     severity: 'error',
    //     detail: this.translateService.instant(`Kindly enter the complete value of previous order number first!`)
    //   });
    // } else if (this.orders[this.orders.length - 1].valid !== 'valid') {
    //   this.messageService.add({
    //     severity: 'error',
    //     detail: this.translateService.instant(`Kindly remove invalid orders first!`)
    //   });
    // } else if (!this.orders[this.orders.length - 1].assignedTo) {
    //   this.messageService.add({
    //     severity: 'error',
    //     detail: this.translateService.instant(`Kindly assign previous order first!`)
    //   });
    // }
  }

  checkIfAllOrdersValid(): (boolean | any) {

    let allOrdersCodes = [];
    for (let singleOrder of this.orders) {
      console.log(singleOrder);
      if (singleOrder.valid == "invalid" ||
        singleOrder.valid != "valid" ||
        singleOrder.valid == "" ||
        allOrdersCodes.includes(singleOrder.code)
      ) {
        return singleOrder;
      }
      else {
        allOrdersCodes.push(singleOrder.code)
      }
    }
    return true;
  }

  removeOrder(i) {
    // if (this.orders.length > 1) {
    this.orders.splice(i, 1);
    // }
  }

  onSubmit() {
    //debugger
    let validateion = this.checkIfAllOrdersValid();
    if (validateion && validateion.code && validateion.statusId != 7 && validateion.statusId != 8) {
      this.messageService.add({
        severity: 'error',
        detail: this.translateService.instant(`Order ${validateion.code} is already assigned, please remove it  first`)
      });
      return;
    }
    if (validateion && validateion.code && validateion.statusId == 8) {
      this.messageService.add({
        severity: 'error',
        detail: this.translateService.instant(`Order Status Is Cancelled`)
      });
      return;
    }
    if (validateion && validateion.code && validateion.statusId == 7) {
      this.messageService.add({
        severity: 'error',
        detail: this.translateService.instant(`Order Status Is Completed`)
      });
      return;
    }
    // statusId=7


    let valid: number = 0;
    let ordersToPost = [];
    let ordersCodeList = [];
    this.orders.forEach((order, index) => {



      if (ordersCodeList.includes(order.code)) {
        valid++;
        this.messageService.add({
          severity: 'error',
          detail: `${this.translateService.instant('The order code')} ${order.code} ${this.translateService.instant('is dublicated, Kindly remove it to submit!')}`
        });
      } else {
        ordersCodeList.push(order.code)
      }
      let length = ("" + order.code).length;
      if (!order.code) {
        valid++;
        this.messageService.add({
          severity: 'error',
          detail: ` ${this.translateService.instant('Please enter the order number at row no ')} ${index + 1} `
        });
      } else if (length < 9) {
        valid++;
        this.messageService.add({
          severity: 'error',
          detail: `${this.translateService.instant('The order code')} ${order.code} ${this.translateService.instant('is invalid, Kindly re-type it correctly to submit')}!`
        });
      } else {
        if (order.valid == "invalid") {
          valid++;
          this.messageService.add({
            severity: 'error',
            detail: `${this.translateService.instant('order')} ${order.code} ${this.translateService.instant(`isn't a valid order id, Kindly remove it to submit`)}!`
          });
        } else {
          if (!(order.assignedTo)) {
            valid++;
            this.messageService.add({
              severity: 'error',
              detail: `${this.translateService.instant(`order`)} ${order.code} ${this.translateService.instant(`isn't assigned`)}!`
            });
          }
        }
      }
      if (valid == 0) {
        ordersToPost.push({
          orderId: order.id,
          isTeam: order.assignedTo.isTeam,
          teamId: order.assignedTo.teamId,
          dispatcherId: order.assignedTo.dispatcherId,
          dispatcherName: order.assignedTo.dispatcherName,
          supervisorId: order.assignedTo.supervisorId,
          supervisorName: order.assignedTo.supervisorName,
          statusId: order.statusId,
          statusName: order.status
        })
      }
    });
    console.log(ordersToPost);
    if (valid == 0 && !this.isSubmitting) {
      this.isSubmitting = true;
      this.lookupService.bulkAssignOrders(ordersToPost).subscribe((res: any) => {
        this.isSubmitting = false;
        console.log(res);
        if (res.isSucceeded) {
          this.messageService.add({
            severity: 'success',
            summary: this.translateService.instant('success'),
            detail: this.translateService.instant(`Orders Updated Successfully!`)
          });
          this.orders = [{
            code: "",
            gettingOrderData: false,
            valid: false
          }];
        } else {
          console.log(res.status.message)
          this.messageService.add({
            severity: 'error',
            summary: this.translateService.instant('Failed'),
            detail: this.translateService.instant(res.status.message)
          });
        }
      }, (err) => {
        this.isSubmitting = false;
        console.error(err);
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed'),
          detail: this.translateService.instant(`Failed due to server error.`)
        });
      })
    }
  }

  onPasteCode(e: any, index) {
    setTimeout(() => {
      this.getOrderByCode(e.target.value, index);
    }, 0);
  }

  onSelectAssignTo(e, index) {
    if (e.value.dispatcherName == this.orders[index].dispatcherName) {
      this.orders[index].assignedTo = "";
      this.messageService.add({
        severity: 'error',
        detail: this.translateService.instant(`Can't assign to same dispatcher!`)
      });
    } else if (e.value.status == 2) {
      this.orders[index].assignedTo = "";
      this.messageService.add({
        severity: 'error',
        detail: this.translateService.instant(`Can't assign to unavailable team!`)
      });
    }
  }
}
