import { AuthGuardGuard } from '../api-module/guards/auth-guard.guard';
import { NotificationListComponent } from '../shared-module/shared/notification-list/notification-list.component';
import { Routes } from '@angular/router';
import { PrintoutComponent } from './printout/printout.component';

export const dispatcherRoutes: Routes = [
  {
    path: '',
    canActivate: [AuthGuardGuard],
    data: { roles: ['Dispatcher'] },
    children: [
      {
        path: '',
        redirectTo: 'board',
        pathMatch: 'full',
      },
      {
        path: 'board',
        loadChildren: './dispatcher-board/dispatcher-board.module#DispatcherBoardModule'
      },
      {
        path: "notifications",
        component: NotificationListComponent,
        data: { title: 'Notifications' }
      },
      {
        path: 'map',
        loadChildren: './map/map.module#MapModule',
        data: { title: 'Orders Map' }
      },
      {
        path: 'printout',
        component: PrintoutComponent,
        canActivate: [AuthGuardGuard],
        data: {
          title: 'Print Out',
          roles: ['Dispatcher']
        }
      },
      {
        path: 'orders-reassignment',
        loadChildren: "./order-re-assign/order-re-assign.module#OrderReAssignModule",
        canActivate: [AuthGuardGuard],
        data: {
          title: 'Order Re-Assignment',
          roles: ['Dispatcher']
        }
      }
    ]
  }
];
