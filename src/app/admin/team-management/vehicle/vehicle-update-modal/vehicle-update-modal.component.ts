import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from '../../../../api-module/services/lookup-services/lookup.service';
import { MessageService } from 'primeng/primeng';
import { VehicleService } from '../vehicle.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-vehicle-update-modal',
  templateUrl: './vehicle-update-modal.component.html',
  styleUrls: ['./vehicle-update-modal.component.css']
})
export class VehicleUpdateModalComponent implements OnInit {

  @Input() data;
  plateNo: string = "";

  constructor(private activeModal: NgbActiveModal,
    private lookup: LookupService,
    private messageService: MessageService,
    private vehicleService: VehicleService,
    private translateService: TranslateService) { }

  ngOnInit() {
    if (this.data) {
      this.plateNo = this.data.plate_no;
    }
  }
  onSubmit() {
    this.lookup.addVehcile(this.plateNo)
      .subscribe(result => {
        if (result.isSucceeded) {
          this.vehicleService.getVehicle.next();
          this.messageService.add({
            severity: 'success',
            summary: this.translateService.instant('Successful!'),
            detail: this.translateService.instant("Saved Successfully!")
          });
          this.activeModal.close();
        } else if (!result.isSucceeded && result.status.code == 6) {
          this.showErrorMsg(result.status.message);
          this.activeModal.close();
        }
      }, error => {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('error!'),
          detail: error
        });
      }
      );
  }


  close() {
    this.activeModal.dismiss();
    this.messageService.add({
      severity: 'info',
      summary: this.translateService.instant('Nothing Added!'),
      detail: this.translateService.instant("You didn't saved new value.")
    });
  }

  /**
   * show error messages
   * 
   * 
   * @param msg 
   */
  showErrorMsg(msg) {
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Value existed before'),
      detail: this.translateService.instant(msg)
    });
  }

}
