import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NotificationsService } from './notifications.service';
import { supervisorRoutes } from './supervisorRoutes';
import { DispatchingModule } from './dispatching.module';

@NgModule({
    imports: [
        DispatchingModule,
        RouterModule.forChild(supervisorRoutes),
    ],
    providers: [
        NotificationsService
    ]
})

export class SupervisorModule {
}
