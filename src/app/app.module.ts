import { RoutingModule, RoutingComponents } from './router.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { MessageService } from 'primeng/components/common/messageservice';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// drag and drop using material
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCardModule } from '@angular/material';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { SharedModuleModule } from './shared-module/shared-module.module';
import { pagesItemSharedService } from './api-module/pagesItemsShared/pagesItemsShared.service';
import { UtilitiesService } from './api-module/services/utilities/utilities.service';
import { AuthenticationServicesService } from './api-module/services/authentication/authentication-services.service';
import { TokenInterceptor } from './shared-module/token-interceptor';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LanguageService } from './services/language.service';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from "@angular/common/http";
import {
  MatInputModule,
  MatDialogModule,
  MatProgressSpinnerModule,
  MatButtonModule,
  MatDialog,
  MatDialogRef
} from '@angular/material';
import { StoreModule } from '@ngrx/store';
import { refreshRateReducer } from './store/refresh-rate.reducer';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    FormsModule,
    HttpClientModule,
    DragDropModule,
    MatCardModule,
    ScrollingModule,
    MatInputModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatDialogModule,
    // MatDialog,
    // MatDialogRef,

    SharedModuleModule.forRoot(),
    StoreModule.forRoot({ refreshRateData: refreshRateReducer }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },

      useDefaultLang: true
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    UtilitiesService, MessageService, AuthenticationServicesService, pagesItemSharedService
  ],
  bootstrap: [AppComponent],

})
export class AppModule {
  constructor(
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {
    this.languageService.language.subscribe(lng => {
      console.log('lng', lng);

      this.translateService.setDefaultLang(lng);
      this.translateService.use(lng);

    });
  }

}