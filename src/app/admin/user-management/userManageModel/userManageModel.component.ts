import { Component, OnInit, Input, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { userService } from "../../../api-module/services/userService/userService.service";
import { NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { Dropdown } from 'primeng/primeng';


@Component({
  selector: 'app-userManageModel',
  templateUrl: './userManageModel.component.html',
  styleUrls: ['./userManageModel.component.css'],
  encapsulation: ViewEncapsulation.None
})





export class userManagementModelComponent implements OnInit {
  @Input() header;
  @Input() role;
  @Input() data: any;
  isSaving: boolean;
  defaultDate: any;
  startDate: Date;
  endDate: Date;
  divisions: any = [];
  isSuccess: boolean = false;
  costCenters: any = [];
  supervisors: any = [];
  dispatchers: any = [];
  engineers: any = [];
  managers: any = [];
  division: any;
  costCenter: any;
  supervisor: any;
  dispatcher: any;
  manager: any;
  engineer: any;
  isDriver: boolean = true;
  isLoading: boolean = true;
  numberOfApiInProgress = 0;
  mode = 'edit';
  userObj: any = { roleNames: [] };
  userData: any = {
    baseUser: { roleNames: [] }
  };
  pf: number;
  newPassword: string = null;
  userName: string = null;
  show_resetPasswordFields: boolean = false;
  states: any[];
  Days: any = ['Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  @ViewChild('divisionDropDown', { 'static': false }) divisionDropDown: Dropdown;
  @ViewChild('engineerDropDown', { 'static': false }) engineerDropDown: Dropdown;
  @ViewChild('supervisorDropDown', { 'static': false }) supervisorDropDown: Dropdown;
  @ViewChild('dispatcherDropDown', { 'static': false }) dispatcherDropDown: Dropdown;
  @ViewChild('editForm', { static: true }) editForm: ElementRef;

  originalData: any = null;

  constructor(public activeModal: NgbActiveModal,
    private lookup: LookupService,
    private messageService: MessageService,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.userName = this.data.name;
    if (!this.userName) {
      this.userName = this.data.userName
    }

    if (this.role == 'Admin') {
      this.isLoading = false;
    }

    this.mode = this.userName ? 'edit' : 'add';
    this.userData.baseUser = {};

    if (this.role != 'Admin') {
      this.getCostCenter()

      if (this.role != 'Manager') {
        this.getDivisions();
      }
    }

    if (this.mode == 'add') {
      if (this.role == 'Supervisor') {
        this.getManagers();
      }
    }
    else if (this.mode == 'edit') {
      this.getRolesById();
      if (this.role == 'Foreman') {
        this.getDispatchersBySupervisorId();
        this.getEngineersByDivisionId();
      }
      if (this.role == 'Foreman' || this.role == 'Dispatcher') {
        this.getSuperVisorbyDivisionId();
      }
      if (this.role == 'Supervisor') {
        this.getManagers();
      }
    };

  }



  /**
  * TODOs test comments Colors
  * ! asdasdasdasd
  * ? Takes two numbers and returns their sum
  * Returns the sum of a and b
  * @param a first input to sum
  * @param b second input to sum
  * @returns sum of a and b
  */
  checkIfServicesFinished() {
    this.numberOfApiInProgress--;
    if (this.numberOfApiInProgress == 0) {
      this.isLoading = false;
      if (this.userData.baseUser.divisionId) {
        let selectedDivision = this.divisions.filter((d) => d.id == this.userData.baseUser.divisionId);
        if (selectedDivision.length) {
          this.division = selectedDivision[0];
        } else if (this.divisions.length) {
          this.division = this.divisions[0];
        }
      }
      if (this.userData.baseUser.costCenterId) {
        let selectedCostCenter = this.costCenters.filter((c) => c.id == this.userData.baseUser.costCenterId);
        if (selectedCostCenter.length) {
          this.costCenter = selectedCostCenter[0];
        } else if (this.costCenters.length) {
          this.costCenter = this.costCenters[0];
        }
      }
      if (this.userData.baseUser.supervisorId) {
        let selectedSupervisor = this.supervisors.filter((s) => s.id == this.userData.baseUser.supervisorId);
        if (selectedSupervisor.length) {
          this.supervisor = selectedSupervisor[0];
        } else if (this.supervisors.length) {
          this.supervisor = this.supervisors[0];
        }
      }
      if (this.userData.baseUser.managerId) {
        let selectedManager = this.managers.filter((m) => m.id == this.userData.baseUser.managerId);
        if (selectedManager.length) {
          this.manager = selectedManager[0];
        } else if (this.managers.length) {
          this.manager = this.managers[0];
        }
      }
      if (this.userData.baseUser.dispatcherId) {
        let selectedDispatcher = this.dispatchers.filter((d) => d.id == this.userData.baseUser.dispatcherId);
        if (selectedDispatcher.length) {
          this.dispatcher = selectedDispatcher[0];
        } else if (this.dispatchers.length) {
          this.dispatcher = this.dispatchers[0];
        }
      }
      if (this.userData.baseUser.engineerId) {
        let selectedEngineer = this.engineers.filter((d) => d.id == this.userData.baseUser.engineerId);
        if (selectedEngineer.length) {
          this.engineer = selectedEngineer[0];
        } else if (this.engineers.length) {
          this.engineer = this.engineers[0];
        }
      }
    }
  }

  logging(e) {
    console.log(e);
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfApiInProgress++;
  }

  getManagers() {
    this.seriveCalled();
    this.managers = [];
    this.lookup.getAllMangaer()
      .subscribe(
        result => {
          this.managers = result.data || [];
          if (!this.managers.length) {
            this.noMembersFound('Managers')
          }
          this.checkIfServicesFinished();
        }, error => {
          this.onRequestFailed(error);
          this.checkIfServicesFinished();
        }
      );
  }

  getEngineersByDivisionId() {
    this.seriveCalled();
    this.engineers = [];
    this.lookup.getEngineersByDivisionId(this.userData.baseUser.divisionId)
      .subscribe(
        result => {
          this.engineers = result.data || [];
          if (!this.engineers.length) {
            this.noMembersFound('Engineers')
          }
          this.checkIfServicesFinished();
        }, error => {
          this.onRequestFailed(error);
          this.checkIfServicesFinished();
        }
      );
  }

  getDivisions() {
    this.seriveCalled();
    this.divisions = [];
    this.lookup.getDivisionEnUrl()
      .subscribe(
        result => {
          this.divisions = result.data || [];
          if (!this.divisions.length) {
            this.noMembersFound('Divisions')
          }
          this.checkIfServicesFinished();
        }, error => {
          this.onRequestFailed(error);
          this.checkIfServicesFinished();
        }
      );
  }

  getCostCenter() {
    this.seriveCalled();
    this.costCenters = [];
    this.lookup.getCostCenter()
      .subscribe(
        result => {
          this.costCenters = result.data || [];
          if (!this.costCenters.length) {
            this.noMembersFound('Cost Centers')
          }
          this.checkIfServicesFinished();
        }, error => {
          this.onRequestFailed(error);
          this.checkIfServicesFinished();
        }
      );
  }

  getSuperVisorbyDivisionId() {
    this.seriveCalled();
    this.supervisors = [];
    this.lookup.getSuperVisorByDivisionId(this.userData.baseUser.divisionId)
      .subscribe(
        result => {
          this.supervisors = result.data || [];
          if (!this.supervisors.length) {
            this.noMembersFound('Supervisors')
          }
          this.checkIfServicesFinished();
        }, error => {
          this.onRequestFailed(error);
          this.checkIfServicesFinished();
        }
      );
  }

  getDispatchersBySupervisorId() {
    this.seriveCalled();
    this.dispatchers = [];
    this.lookup.getDispatcherBySuperId(this.userData.baseUser.supervisorId)
      .subscribe(
        result => {
          this.dispatchers = result.data || [];
          if (!this.dispatchers.length) {
            this.noMembersFound('Dispatchers')
          }
          this.checkIfServicesFinished();
        }, error => {
          this.onRequestFailed(error);
          this.checkIfServicesFinished();
        }
      );
  }

  // compareByOptionId(idFist, idSecond) {
  //   return idFist && idSecond && idFist.id == idSecond.id;
  // }

  getRolesById() {
    this.seriveCalled();
    this.userData.baseUser.divisionId = this.data.divisionId;
    this.userData.baseUser.supervisorId = this.data.supervisorId;
    this.pf = this.userData.baseUser.pf;
    let request: Observable<any>;
    if (this.role == 'Admin') {
      request = this.lookup.getUserById(this.data.id);
    } else if (this.role == 'Dispatcher') {
      request = this.lookup.getDispatcherById(this.data.id);
    } else if (this.role == 'Supervisor') {
      request = this.lookup.getSuperVisorById(this.data.id);
    } else if (this.role == 'Manager') {
      request = this.lookup.getManagerById(this.data.id);
    } else if (this.role == 'Material Controller') {
      request = this.lookup.getMaterialControllerById(this.data.id);
    } else if (this.role == 'Engineer') {
      request = this.lookup.getEngineerById(this.data.id);
    } else if (this.role == 'Technician') {
      request = this.lookup.getTechnicianById(this.data.id);
    } else if (this.role == 'Foreman') {
      request = this.lookup.getForemanById(this.data.id);
    }
    request.subscribe(
      res => {
        if (res.isSucceeded) {
          this.userData.baseUser = res.data;
          this.isDriver = this.userData.baseUser.isDriver;
          this.originalData = Object.assign({}, res.data);
          this.checkIfServicesFinished();
        } else {
          this.onRequestFailed(res);
        }
      }, (error) => {
        this.onRequestFailed(error);
      }
    );
  }

  close(result) {
    result = {
      id: this.data.id,
      divisionId: result.divisionId,
    }
    this.activeModal.close(result);
  }

  saveUser() {
    //debugger
    let objToPost: any = Object.assign({}, this.userData);
    if (this.userData.baseUser.confirmPassword == this.userData.baseUser.password) {
      this.isSaving = true;
      objToPost.baseUser.roleNames = [];
      objToPost.baseUser.roleNames.push(this.role);
      objToPost.baseUser.pf = objToPost.baseUser.userName;
      if (this.role == 'Manager') {
        objToPost.managerViewModel = Object.assign({}, objToPost.baseUser);
        objToPost.managerViewModel.id = this.data.id;
        objToPost.managerViewModel.name = objToPost.baseUser.userName;
      } else if (this.role == 'Dispatcher') {
        if (this.originalData && (this.originalData.supervisorId !== this.userData.baseUser.supervisorId || this.originalData.divisionId !== this.userData.baseUser.divisionId))
          if (objToPost.baseUser.foreManCount && objToPost.baseUser.orderCount) {
            this.showErrorMsg("This dispatcher has foremen & current orders; you can’t change his structure. Please re-assign his foremen & current orders then try again");
            return;
          } else if (objToPost.baseUser.foreManCount) {
            this.showErrorMsg("This dispatcher has foremen; you can’t change his structure. Please re-assign his foremen to another dispatcher, then try again");
            return;
          } else if (objToPost.baseUser.orderCount) {
            this.showErrorMsg("This dispatcher has current orders; you can’t change his structure. Please finish or re-assign theses orders, then try again");
            return;
          }
        objToPost.dispatcherViewModel = Object.assign({}, objToPost.baseUser);
        objToPost.dispatcherViewModel.id = this.data.id;
        objToPost.dispatcherViewModel.name = objToPost.baseUser.userName;
      } else if (this.role == 'Engineer') {
        if (this.originalData && (this.originalData.divisionId !== this.userData.baseUser.divisionId))
          if (objToPost.baseUser.teamCount) {
            this.showErrorMsg("This engineer is assigned to a foreman; you can’t change his division. Please remove this engineer from his foreman(s) then try again");
            return;
          }
        objToPost.engineerViewModel = Object.assign({}, objToPost.baseUser);
        objToPost.engineerViewModel.id = this.data.id;
        objToPost.engineerViewModel.name = objToPost.baseUser.userName;
      } else if (this.role == 'Foreman') {
        if (this.originalData && (this.originalData.dispatcherId !== this.userData.baseUser.dispatcherId || this.originalData.divisionId !== this.userData.baseUser.divisionId || this.originalData.engineerId !== this.userData.baseUser.engineerId))
          if (objToPost.baseUser.ordersCount) {
            this.showErrorMsg("This foreman has orders; you can’t change his structure.");
            return;
          }
        objToPost.foremanViewModel = Object.assign({}, objToPost.baseUser);
        objToPost.foremanViewModel.id = this.data.id;
        objToPost.foremanViewModel.name = objToPost.baseUser.userName;
      } else if (this.role == 'Supervisor') {
        // if (this.originalData && this.originalData.divisionId !== this.userData.baseUser.divisionId) {
        //   // need to discuss with menna
        //   if (objToPost.baseUser.orderCount) {
        //     this.showErrorMsg("");
        //     return;
        //   } else if (objToPost.baseUser.orderCount) {
        //     this.showErrorMsg("");
        //     return;
        //   }
        // }
        objToPost.supervisorViewModel = Object.assign({}, objToPost.baseUser);
        objToPost.supervisorViewModel.id = this.data.id;
        objToPost.supervisorViewModel.name = objToPost.baseUser.userName;
      } else if (this.role == 'Technician') {
        if (objToPost.baseUser.teamId && this.originalData) {
          if (this.originalData.divisionId !== this.userData.baseUser.divisionId) {
            this.showErrorMsg("This technician is assigned to a team, you can’t change his division. Please remove this technician from his team then try again");
            return;
          } else if (this.originalData.isDriver !== this.isDriver) {
            this.showErrorMsg("You can't change this technician type, because this technician is assigned to a team");
            return;
          }
        }
        objToPost.technicianViewModel = Object.assign({}, objToPost.baseUser);
        objToPost.technicianViewModel.id = this.data.id;
        objToPost.technicianViewModel.name = objToPost.baseUser.userName;
        objToPost.technicianViewModel.isDriver = this.isDriver;
      }
      if (this.data.id) {
        objToPost.baseUser.id = this.role == 'Admin' ? this.data.id : this.data.userId;
        this.lookup.updateUser(objToPost)
          .subscribe((res: any) => {
            if (res.isSucceeded) {
              let currentUser: any = JSON.parse(localStorage.getItem("AlghanimCurrentUser"));
              if (this.role == 'Admin' && this.data.id == currentUser.data.userId) {
                currentUser.data.name = `${this.userData.baseUser.firstName} ${this.userData.baseUser.lastName} - ${this.userData.baseUser.userName}`
                localStorage.setItem("AlghanimCurrentUser", JSON.stringify(currentUser));
              }
              this.onSaveRequestCompleted(res);
            } else {
              this.checkIfServicesFinished();
              if (res.status.code == 7) {
                this.showErrorMsg('The Username already existed!');
              } else if (res.status.code == 9) {
                // this.showErrorMsg('HasChilds');
                if (this.role == 'Dispatcher') {
                  this.showErrorMsg("This dispatcher has foremen; you can’t change his structure. Please re-assign his foremen to another dispatcher, then try again");
                } else if (this.role == 'Engineer') {
                  this.showErrorMsg("This engineer is assigned to a foreman; you can’t change his structure. Please remove this engineer from his foreman(s) then try again");
                } else {
                  this.onRequestFailed(res);
                }
              } else if (res.status.code == 10) {
                // this.showErrorMsg('AssignedForWork!');
                if (this.role == 'Dispatcher') {
                  this.showErrorMsg("This dispatcher has current orders; you can’t change his structure. Please finish or re-assign theses orders, then try again");
                } else {
                  this.onRequestFailed(res);
                }
              } else if (res.status.code == 11) {
                // this.showErrorMsg('UserHasTeam!');
                if (this.role == 'Technician') {
                  this.showErrorMsg("This technician is assigned to a team, you can’t change his structure. Please remove this technician from his team then try again");
                } else if (this.role == 'Foreman') {
                  this.showErrorMsg("This foreman is assigned to a team; you can’t change his structure. Please remove this foreman from his team(s) then try again");
                } else {
                  this.onRequestFailed(res);
                }
              } else if (res.status.code == 12) {
                // this.showErrorMsg('AssignedForWorkAndHasUsers!');
                if (this.role == 'Dispatcher') {
                  this.showErrorMsg("This dispatcher has foremen & current orders; you can’t change his structure. Please re-assign his foremen & current orders then try again");
                } else {
                  this.onRequestFailed(res);
                }
              } else {
                this.onRequestFailed(res);
              }
            }
          }, (err: any) => {
            this.onRequestFailed(err);
          });
      } else {
        this.lookup.createUser(objToPost)
          .subscribe((res: any) => {
            if (res.isSucceeded) {
              this.onSaveRequestCompleted(res);
            } else {
              if (res.status.code == 7) {
                this.checkIfServicesFinished();
                this.showErrorMsg('The Username already existed!');
              }
            }
          }, (err: any) => {
            this.onRequestFailed(err);
          });
      }
    }

    // if (this.userData.baseUser.confirmPassword == this.userData.baseUser.password) {
    //   this.isSaving = true;
    //   this.userData.baseUser.roleNames = [];
    //   this.userData.baseUser.roleNames.push(this.role);
    //   this.pf = this.userData.baseUser.pf = this.userData.baseUser.userName;
    //   if (this.data.id) {
    //     this.userData.baseUser.id = this.data.userId;
    //     this.userData.phone = this.data.phone;
    //     this.userData.otherObj.pf = this.userData.baseUser.pf;
    //     if (this.role == 'Admin') {
    //       this.userData.baseUser.id = this.data.id;
    //       this.lookup.editUser(this.userData.baseUser)
    //         .subscribe(
    //           result => {
    //             if (result.isSucceeded == true) {
    //               this.messageService.add({
    //                 severity: 'success',
    //                 summary: this.translateService.instant('Successful!'),
    //                 detail: this.translateService.instant("New value saved Successfully!.")
    //               });
    //             }
    //             this.activeModal.close()
    //           },
    //           err => {
    //             console.log(err);
    //             err.status == 401 && this.utilities.unauthrizedAction();
    //             this.messageService.add({
    //               severity: 'error',
    //               summary: this.translateService.instant('Failed!'),
    //               detail: this.translateService.instant(this.translateService.instant("New value saved Successfully!."))
    //             });
    //           }
    //         );
    //     }

    //     if (this.role != 'Admin') {
    //       this.lookup.editUser(this.userData.baseUser)
    //         .subscribe(
    //           result => {
    //             if (result.isSucceeded == true) {
    //               this.isSuccess = true;
    //               this.userData.otherObj.isDriver = this.isDriver;
    //               this.userData.otherObj.pf = this.pf;
    //               this.userData.otherObj.id = this.data.id;
    //               this.userData.otherObj.userId = result.data.id;
    //               this.userData.otherObj.name = result.data.userName;
    //               if (this.division) {
    //                 this.userData.otherObj.divisionId = this.division.id;
    //                 this.userData.otherObj.divisionName = this.division.name;
    //               }
    //               if (this.costCenter) {
    //                 this.userData.otherObj.costCenterId = this.costCenter.id;
    //                 this.userData.otherObj.costCenterName = this.costCenter.name;
    //               }
    //               if (this.supervisor) {
    //                 this.userData.otherObj.supervisorId = this.supervisor.id;
    //                 this.userData.otherObj.supervisorName = this.supervisor.name;
    //               }
    //               if (this.manager) {
    //                 this.userData.otherObj.managerId = this.manager.id;
    //                 this.userData.otherObj.managerName = this.manager.name;
    //               }
    //               if (this.dispatcher) {
    //                 this.userData.otherObj.dispatcherId = this.dispatcher.id;
    //                 this.userData.otherObj.dispatcherName = this.dispatcher.name;
    //               }
    //               if (this.engineer) {
    //                 this.userData.otherObj.engineerId = this.engineer.id;
    //                 this.userData.otherObj.engineerName = this.engineer.name;
    //               }

    //               if (this.role == 'Dispatcher') {
    //                 this.lookup.editUserDispatcher(this.userData.otherObj)
    //                   .subscribe(
    //                     (data) => {
    //                       if (data.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       console.log(err);
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.messageService.add({
    //                         severity: 'error',
    //                         summary: this.translateService.instant('Failed!'),
    //                         detail: this.translateService.instant(this.translateService.instant("New value saved Successfully!."))
    //                       });
    //                     }
    //                   );
    //               }
    //               if (this.role == 'Supervisor') {
    //                 this.lookup.editUserSuperVisor(this.userData.otherObj)
    //                   .subscribe(
    //                     (data) => {
    //                       if (data.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       console.log(err);
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.messageService.add({
    //                         severity: 'error',
    //                         summary: this.translateService.instant('Failed!'),
    //                         detail: this.translateService.instant(this.translateService.instant("New value saved Successfully!."))
    //                       });
    //                     }
    //                   );
    //               }
    //               if (this.role == 'Manager') {
    //                 this.lookup.editUserManager(this.userData.otherObj)
    //                   .subscribe(
    //                     (data) => {
    //                       if (data.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       console.log(err);
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.messageService.add({
    //                         severity: 'error',
    //                         summary: this.translateService.instant('Failed!'),
    //                         detail: this.translateService.instant(this.translateService.instant("New value saved Successfully!."))
    //                       });
    //                     }
    //                   );
    //               }
    //               if (this.role == 'Material Controller') {
    //                 this.lookup.editUserMateialController(this.userData.otherObj)
    //                   .subscribe(
    //                     (data) => {
    //                       if (data.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       console.log(err);
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.messageService.add({
    //                         severity: 'error',
    //                         summary: this.translateService.instant('Failed!'),
    //                         detail: this.translateService.instant(this.translateService.instant("New value saved Successfully!."))
    //                       });
    //                     }
    //                   );
    //               }
    //               if (this.role == 'Engineer') {
    //                 this.lookup.editUserEngineer(this.userData.otherObj)
    //                   .subscribe(
    //                     (data) => {
    //                       if (data.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       console.log(err);
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.messageService.add({
    //                         severity: 'error',
    //                         summary: this.translateService.instant('Failed!'),
    //                         detail: this.translateService.instant(this.translateService.instant("New value saved Successfully!."))
    //                       });
    //                     }
    //                   );
    //               }
    //               if (this.role == 'Technician') {
    //                 this.lookup.editUserTechnician(this.userData.otherObj)
    //                   .subscribe(
    //                     (data) => {
    //                       if (data.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       console.log(err);
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.messageService.add({
    //                         severity: 'error',
    //                         summary: this.translateService.instant('Failed!'),
    //                         detail: this.translateService.instant(this.translateService.instant("New value saved Successfully!."))
    //                       });
    //                     }
    //                   );
    //               }
    //               if (this.role == 'Foreman') {
    //                 this.lookup.editUserForeman(this.userData.otherObj)
    //                   .subscribe(
    //                     (data) => {
    //                       if (data.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       console.log(err);
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.messageService.add({
    //                         severity: 'error',
    //                         summary: this.translateService.instant('Failed!'),
    //                         detail: this.translateService.instant("New value saved Successfully!.")
    //                       });
    //                     }
    //                   );
    //               }
    //             }
    //             else {
    //               this.messageService.add({
    //                 severity: 'error',
    //                 summary: this.translateService.instant('Failed!'),
    //                 detail: this.translateService.instant("New value saved Successfully!.")
    //               });
    //             }
    //           }, error => {
    //             console.log(error);
    //             this.messageService.add({
    //               severity: 'error',
    //               summary: this.translateService.instant('Failed!'),
    //               detail: error
    //             });
    //           }
    //         );
    //     }
    //   }
    //   else {
    //     {
    //       this.userService.checkUserNameExist(this.userData.baseUser.userName)
    //         .subscribe(
    //           result => {
    //             if (result.data == false) {
    //               if (this.role == 'Admin') {
    //                 this.lookup.postUser(this.userData.baseUser)
    //                   .subscribe(
    //                     result => {
    //                       if (result && result.isSucceeded == true) {
    //                         this.messageService.add({
    //                           severity: 'success',
    //                           summary: this.translateService.instant('Successful!'),
    //                           detail: this.translateService.instant("New value saved Successfully!.")
    //                         });
    //                       } else {
    //                         this.onRequestFailed(result);
    //                       }
    //                       this.activeModal.close()
    //                     },
    //                     err => {
    //                       err.status == 401 && this.utilities.unauthrizedAction();
    //                       this.onRequestFailed(err);
    //                     }
    //                   );
    //               }
    //               if (this.role != 'Admin') {
    //                 this.lookup.postUser(this.userData.baseUser).subscribe(result => {
    //                   if (result.isSucceeded == true) {
    //                     this.isSuccess = true;
    //                     this.userData.otherObj.isDriver = this.isDriver;
    //                     this.userData.otherObj.pf = this.pf;
    //                     this.userData.otherObj.userId = result.data.id;
    //                     this.userData.otherObj.name = result.data.userName;
    //                     this.userData.otherObj.costCenterId = this.costCenter.id;
    //                     this.userData.otherObj.costCenterName = this.costCenter.name;
    //                     if (this.division) {
    //                       this.userData.otherObj.divisionId = this.division.id;
    //                       this.userData.otherObj.divisionName = this.division.name;
    //                     }
    //                     if (this.supervisor) {
    //                       this.userData.otherObj.supervisorId = this.supervisor.id;
    //                       this.userData.otherObj.supervisorName = this.supervisor.name;
    //                     }
    //                     if (this.manager) {
    //                       this.userData.otherObj.managerId = this.manager.id;
    //                       this.userData.otherObj.managerName = this.manager.name;
    //                     }
    //                     if (this.dispatcher) {
    //                       this.userData.otherObj.dispatcherId = this.dispatcher.id;
    //                       this.userData.otherObj.dispatcherName = this.dispatcher.name;
    //                     }
    //                     if (this.engineer) {
    //                       this.userData.otherObj.engineerId = this.engineer.id;
    //                       this.userData.otherObj.engineerName = this.engineer.name;
    //                     }

    //                     if (this.role == 'Dispatcher') {
    //                       this.lookup.postUserDispatcher(this.userData.otherObj)
    //                         .subscribe(
    //                           (data) => {
    //                             if (data.isSucceeded == true) {
    //                               this.messageService.add({
    //                                 severity: 'success',
    //                                 summary: this.translateService.instant('Successful!'),
    //                                 detail: this.translateService.instant("New value saved Successfully!.")
    //                               });
    //                             }
    //                             this.activeModal.close()
    //                           },
    //                           err => {
    //                             this.onRequestFailed(err);
    //                           }
    //                         );
    //                     }
    //                     if (this.role == 'Supervisor') {
    //                       this.lookup.postSuperVisor(this.userData.otherObj)
    //                         .subscribe(
    //                           (data) => {
    //                             if (data.isSucceeded == true) {
    //                               this.messageService.add({
    //                                 severity: 'success',
    //                                 summary: this.translateService.instant('Successful!'),
    //                                 detail: this.translateService.instant("New value saved Successfully!.")
    //                               });
    //                             }
    //                             this.activeModal.close()
    //                           },
    //                           err => {
    //                             this.onRequestFailed(err);
    //                           }
    //                         );
    //                     }
    //                     if (this.role == 'Engineer') {
    //                       this.lookup.postEngineer(this.userData.otherObj)
    //                         .subscribe(
    //                           (data) => {
    //                             if (data.isSucceeded == true) {
    //                               this.messageService.add({
    //                                 severity: 'success',
    //                                 summary: this.translateService.instant('Successful!'),
    //                                 detail: this.translateService.instant("New value saved Successfully!.")
    //                               });
    //                             }
    //                             this.activeModal.close()
    //                           },
    //                           err => {
    //                             this.onRequestFailed(err);
    //                           }
    //                         );
    //                     }
    //                     if (this.role == 'Technician') {
    //                       this.lookup.postTechnician(this.userData.otherObj)
    //                         .subscribe(
    //                           (data) => {
    //                             if (data.isSucceeded == true) {
    //                               this.messageService.add({
    //                                 severity: 'success',
    //                                 summary: this.translateService.instant('Successful!'),
    //                                 detail: this.translateService.instant("New value saved Successfully!.")
    //                               });
    //                             }
    //                             this.activeModal.close()
    //                           },
    //                           err => {
    //                             this.onRequestFailed(err);
    //                           }
    //                         );
    //                     }
    //                     if (this.role == 'Foreman') {
    //                       this.lookup.postForeman(this.userData.otherObj)
    //                         .subscribe(
    //                           (data) => {
    //                             if (data.isSucceeded == true) {
    //                               this.messageService.add({
    //                                 severity: 'success',
    //                                 summary: this.translateService.instant('Successful!'),
    //                                 detail: this.translateService.instant("New value saved Successfully!.")
    //                               });
    //                             }
    //                             this.activeModal.close()
    //                           },
    //                           err => {
    //                             this.onRequestFailed(err);
    //                           }
    //                         );
    //                     }
    //                     if (this.role == 'Manager') {
    //                       this.lookup.postManager(this.userData.otherObj)
    //                         .subscribe(
    //                           (data) => {
    //                             if (data.isSucceeded == true) {
    //                               this.messageService.add({
    //                                 severity: 'success',
    //                                 summary: this.translateService.instant('Successful!'),
    //                                 detail: this.translateService.instant("New value saved Successfully!.")
    //                               });
    //                             }
    //                             this.activeModal.close()
    //                           },
    //                           err => {
    //                             this.onRequestFailed(err);
    //                           }
    //                         );
    //                     }

    //                     if (this.role == 'Material Controller') {
    //                       this.lookup.postMaterialController(this.userData.otherObj)
    //                         .subscribe(
    //                           (data) => {
    //                             if (data.isSucceeded == true) {
    //                               this.messageService.add({
    //                                 severity: 'success',
    //                                 summary: this.translateService.instant('Successful!'),
    //                                 detail: this.translateService.instant("New value saved Successfully!.")
    //                               });
    //                             }
    //                             this.activeModal.close()
    //                           },
    //                           err => {
    //                             this.onRequestFailed(err);
    //                           }
    //                         );
    //                     }
    //                   } else {
    //                     this.onRequestFailed(result);
    //                   }
    //                 });
    //               }
    //             }
    //             else {
    //               this.messageService.add({
    //                 severity: 'error',
    //                 summary: this.translateService.instant('Failed!'),
    //                 detail: this.translateService.instant("this user name already exist")
    //               });
    //               this.isSaving = false;
    //             }
    //           }
    //         );
    //     }
    //   }
    // }
  }

  onSaveRequestCompleted(res) {
    if (res && res.isSucceeded == true) {
      this.messageService.add({
        severity: 'success',
        summary: this.translateService.instant('Successful!'),
        detail: this.translateService.instant("New value saved Successfully!.")
      });
      this.activeModal.close();
    } else {
      this.onRequestFailed(res);
    }
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  onResetPassword() {
    this.show_resetPasswordFields = true;
  }

  editOtherFields() {
    this.show_resetPasswordFields = false;
  }

  saveNewPassword(form: NgForm) {


    this.isLoading = true;
    const data = {
      Username: this.userData.baseUser.userName,
      NewPassword: form.value.newPassword
    };

    this.lookup.resetUserPassword(data)
      .subscribe(
        res => {
          this.isLoading = false;
          this.messageService.add({
            severity: 'success',
            summary: this.translateService.instant('Successful!'),
            detail: this.translateService.instant("Password Updated Successfully!.")
          });
          this.activeModal.close();
        }, err => {
          console.log(err);
          this.showErrorMsg(err);
        }
      );
  }

  onRequestFailed(err) {
    this.isLoading = false;
    console.error(err);
    this.showErrorMsg("Failed due to network error");
  }

  noMembersFound(members) {
    this.messageService.add({
      severity: 'info',
      summary: this.translateService.instant('Warning!'),
      detail: this.translateService.instant(`No ${members} found`)
    });
  }


  onSelectDivision(event: any) {
    if (this.role == 'Supervisor' && event.value.currentUserId && this.userData.baseUser.userName != event.value.currentUserId) {
      this.resetDivisionDropDownValue();
      this.showErrorMsg("This Division Is Already Assigned To A Supervisor");
    } else {
      this.userData.baseUser.divisionId = event.value.id;
      this.userData.baseUser.divisionName = event.value.name;
      if (this.role == 'Foreman') {
        this.resetEngineer();
        this.getEngineersByDivisionId();
        this.resetDispatcher();
      }
      if (this.role == 'Foreman' || this.role == 'Dispatcher') {
        this.resetSupervisor();
        this.getSuperVisorbyDivisionId()
      }
    }
  }

  onSelectSupervisor(event: any) {
    this.userData.baseUser.supervisorId = event.value.id;
    this.userData.baseUser.supervisorName = event.value.fullName;
    if (this.role == 'Foreman') {
      this.resetDispatcher();
      this.getDispatchersBySupervisorId()
    }
  }


  onSelectCostCenters(event: any) {
    this.userData.baseUser.costCenterId = event.value.id;
    this.userData.baseUser.costCenterName = event.value.name;
  }

  onSelectManager(event: any) {
    this.userData.baseUser.managerId = event.value.id;
    this.userData.baseUser.managerName = event.value.fullName;
  }

  onSelectEngineer(event: any) {
    this.userData.baseUser.engineerId = event.value.id;
    this.userData.baseUser.engineerName = event.value.fullName;
  }

  onSelectDispatcher(event: any) {
    this.userData.baseUser.dispatcherId = event.value.id;
    this.userData.baseUser.dispatcherName = event.value.fullName;
  }

  resetDivisionDropDownValue() {
    this.division = null;
    this.userData.baseUser.divisionId = null;
    this.userData.baseUser.divisionName = null;
    this.divisionDropDown && this.divisionDropDown.writeValue(null);
  }

  resetSupervisor() {
    this.supervisor = null;
    this.userData.baseUser.supervisorId = null;
    this.userData.baseUser.supervisorName = null;
    this.supervisorDropDown && this.supervisorDropDown.writeValue(null);
    this.supervisors = [];
    this.resetDispatcher();
  }

  resetDispatcher() {
    this.dispatcher = null;
    this.userData.baseUser.dispatcherId = null;
    this.userData.baseUser.dispatcherName = null;
    this.dispatcherDropDown && this.dispatcherDropDown.writeValue(null);
    this.dispatchers = [];
  }

  resetEngineer() {
    this.engineer = null;
    this.userData.baseUser.engineerId = null;
    this.userData.baseUser.engineerName = null;
    this.engineerDropDown && this.engineerDropDown.writeValue(null);
    this.engineers = [];
  }

  showErrorMsg(msg) {
    this.isSaving = false;
    // this.messageService.clear();

    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed!'),
      detail: this.translateService.instant(msg)
    });
  }
}
