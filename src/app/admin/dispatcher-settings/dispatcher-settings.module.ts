import { NgModule } from '@angular/core';
import { DispatcherSettingsComponent } from './dispatcher-settings/dispatcher-settings.component';
import { EditDisAndProbModalComponent } from './dispatcher-settings/edit-dis-and-prob-modal/edit-dis-and-prob-modal.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  { path: "", component: DispatcherSettingsComponent }
];

@NgModule({
  declarations: [
    DispatcherSettingsComponent,
    EditDisAndProbModalComponent,
  ],
  imports: [
    SharedModuleModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ], entryComponents: [
    EditDisAndProbModalComponent,
  ]
})
export class DispatcherSettingsModule { }
