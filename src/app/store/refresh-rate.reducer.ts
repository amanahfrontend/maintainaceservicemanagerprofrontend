import * as refreshRateActions from "./refresh-rate.actions";

export interface state {
    refreshRate: number;
}

export interface AppState {
    refreshRateData: state;
}

const initState: state = {
    refreshRate: +localStorage.getItem("refreshRate") || 5
}

export function refreshRateReducer(
    state = initState,
    action: refreshRateActions.RefreshRateActions
) {
    switch (action.type) {
        case refreshRateActions.UPDATE_REFRESH_RATE:
            const updatedRefreshRate = action.payload || +localStorage.getItem("refreshRate") || 5;
            localStorage.setItem("refreshRate", ("" + updatedRefreshRate));
            return {
                ...state,
                refreshRate: updatedRefreshRate
            };
        default:
            return state;
    }
}
