import { Injectable } from "@angular/core";
import * as myGlobals from "../globalPath";
import { UtilitiesService } from "../utilities/utilities.service";
import { HttpClient, HttpEventType } from "@angular/common/http";
import { Observable } from "rxjs";
import { PaginatedTeamOrdersViewModel } from "src/app/models/paginatedTeamOrdersViewModel";

@Injectable()
export class LookupService {
  constructor(private http: HttpClient) { }

  logout(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlIdentity +
      myGlobals.logoutUrl +
      "?userId=" +
      obj.userId +
      "&deviceId=" +
      obj.deviceId,
      {}
    );
  }

  createUser(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlIdentity + myGlobals.createUserUrl,
      obj
    );
  }

  updateUser(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlIdentity + myGlobals.updateUserUrl,
      obj
    );
  }

  getAllAvailability(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.availabilitysUrl
    );
  }

  getAvailabilities(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.availabilitysWithoutLangUrl
    );
  }

  getAllAvailabilityWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.availabilitysUrlWithBlock
    );
  }

  blockAvailabilty(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockAvailability + id
    );
  }

  getBuildingTypes(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.buildingTypesUrl
    );
  }

  getBuildingTypesUrlWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.buildingTypesUrlWithBlock
    );
  }

  blockBuildingType(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockBuildingType + id
    );
  }

  getDispatcherFormensWithTeams(dispatcherId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement +
      myGlobals.getDispatcherFormensWithTeamsUrl +
      "?dispatcherId=" +
      dispatcherId
    );
  }
  getTeamOrdersByTeamId(model: any): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.getTeamOrdersByTeamIdUrl,
      model
    );
  }

  getAllAreasDataManagement(): Observable<any> {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.areasUrl);
  }

  getAllAreasWithBlocked(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.areasUrlWithBlocked
    );
  }

  blockArea(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockArea + id
    );
  }

  getAllAreasDataManagementPaginated(pageNoSize): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.areasUrl,
      pageNoSize
    );
  }

  getAllAttendanceStates(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.attendanceStatusUrl
    );
  }

  getAllAttendanceStatesWithBlocked(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.attendanceStatusUrlWithBlock
    );
  }

  blockAttendanceStatus(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockAttendanceStatus + id
    );
  }
  ///OrderStatus
  blockOrderStatus(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.blockOrderStatus + id
    );
  }

  getCompanyCodes(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.companyCodeUrl
    );
  }

  getCompanyCodesWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.companyCodeUrl
    );
  }

  getCompanyCodesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.companyCodeUrlWithBlock
    );
  }

  blockCompanyCode(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockCompanyCode + id
    );
  }

  getContractTypes(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.contractTypesUrl
    );
  }

  getContractTypesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.contractTypesUrlWithBlock
    );
  }

  blockContractType(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockContractType + id
    );
  }

  getCostCenter(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.costCenterUrl
    );
  }

  getCostCenterWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.costCenterUrlWithBlock
    );
  }

  blockCostCenter(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockCostCenter + id
    );
  }

  getDivision(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.divisionUrl
    );
  }

  getDivisionWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.divisionUrlWithBlock
    );
  }

  blockDivision(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockDivision + id
    );
  }

  getDivisionEnUrl(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.divisionEnUrl
    );
  }

  getGovernorates(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.governoratesUrl
    );
  }

  getGovernoratesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.governoratesUrlWithBlock
    );
  }

  blockGovernorate(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockGovernorate + id
    );
  }

  getShift(): Observable<any> {
    return this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.shiftUrl);
  }

  getShiftWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.shiftUrlWithBlock
    );
  }

  blockShift(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockShift + id
    );
  }

  getlanguages(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.languagesUrl
    );
  }

  getlanguagesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.languagesUrlWithBlock
    );
  }

  blockLanguage(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.blockLanguage + id
    );
  }

  getOrderStatus(): Observable<any> {
    return this.http.get(myGlobals.orderStatus);
  }

  getOrderByCode(code) {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getOrderByCode + code
    );
  }

  bulkAssignOrders(obj) {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.orderBulkAssign,
      obj
    );
  }

  getOrderProgress(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.orderProgressUrl + id
    );
  }

  addUserDevice(key): Observable<any> {
    const deviceData = {
      deveiceId: key,
      fk_AppUser_Id: JSON.parse(localStorage.getItem("AlghanimCurrentUser"))
        .data.userId,
    };
    return this.http.post(
      myGlobals.BaseUrlIdentity + myGlobals.addUserDeviceUrl,
      deviceData
    );
  }

  getAllProblems(): Observable<any> {
    return this.http.get(myGlobals.allOrderProblemsUrl);
  }

  getAllProblemsLocalized(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.allProbsLocalized
    );
  }

  getAllProblemsLocalizedWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.allProbsLocalizedWithBlock
    );
  }

  blockProblem(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.blockProblem + id
    );
  }

  getAllDivisions(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.allDivisionsUrl
    );
  }

  getAllDivisionsWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.allDivisionsWithBlockedUrl
    );
  }

  getAllOrderTypes(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.allOrderTypesUrl
    );
  }

  getAllOrderTypesWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.allOrderTypesWithBlockedUrl
    );
  }

  getAllAreas(): Observable<any> {
    return this.http.get(myGlobals.allAreasDispatcher);
  }

  getDisAllOrders(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getOrdersByDis + id
    );
  }

  getAllOrderProblems(): Observable<any> {
    return this.http.get(myGlobals.allOrderProblemsUrl);
  }

  postOrderType(orderType): Observable<any> {
    return this.http.post(myGlobals.postOrderTypeUrl, orderType);
  }

  postOrderStatus(orderState): Observable<any> {
    return this.http.post(myGlobals.postOrderStateUrl, orderState);
  }

  filterOrders(filterObj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.filterMapOrdersURL,
      filterObj
    );
  }

  postMapOrderFilter(params): Observable<any> {
    //api/Order/OrderFilter
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.postMapOrderFilterURL,
      params
    );
  }

  postAllOrdersBoardOrderFilter(params): Observable<any> {
    // dispatcher board filter function
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.postAllOrdersBoardOrderFilterURL,
      params
    );
  }

  postBoardOrderFilter(params): Observable<any> {
    // dispatcher board filter function
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.postBoardOrderFilterURL,
      params
    );
  }

  postBoardUnAssignedOrderFilter(params): Observable<any> {
    // dispatcher board filter function
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.postUnassignedOrdersBoardFilterURL,
      params
    );
  }
  postOrderProblems(objectProblems): Observable<any> {
    return this.http.post(myGlobals.postOrderProblems, objectProblems);
  }

  ////////////////////////////////////////// New //////////////////////////////////////////////////
  getAllRoles(): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.rolesUrl);
  }
  postArea(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postAreasUrl,
      obj
    );
  }
  editArea(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putAreasUrl,
      obj
    );
  }
  getAreaLangById(areaId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.areaLanguageByIdUrl + areaId
    );
  }
  deleteArea(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteAreaUrl + id
    );
  }

  postProblem(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.addProbURL,
      obj
    );
  }
  postAttendanceStates(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postAttendanceStates,
      obj
    );
  }
  editAttendanceStates(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putAttendanceStatesUrl,
      obj
    );
  }
  deleteAttendanceStates(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteAttendanceStatesUrl + id
    );
  }
  getAttendanceStatesById(attendId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement +
      myGlobals.attendanceStatesByIdUrl +
      attendId
    );
  }
  //OrderStatus
  getOrderStatusById(OrderStatusId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.attendanceStatesByIdUrl +
      OrderStatusId
    );
  }
  postAvailability(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postAvailability,
      obj
    );
  }
  editAvailability(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putAvailabilityUrl,
      obj
    );
  }
  deleteAvailability(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteAvailabilityUrl + id
    );
  }
  getAvailabilityById(avaliblId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement +
      myGlobals.availabilityByIdUrl +
      avaliblId
    );
  }
  postBuildingTypes(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postBuildingTypes,
      obj
    );
  }
  editBuildingTypes(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putBuildingTypesUrl,
      obj
    );
  }
  deleteBuildingTypes(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteBuildingTypesUrl + id
    );
  }
  getBuildingTypesById(buildId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.buildingTypesByIdUrl + buildId
    );
  }
  postCompanyCode(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postCompanyCode,
      obj
    );
  }
  editCompanyCode(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putCompanyCodeUrl,
      obj
    );
  }
  deleteCompanyCode(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteCompanyCodeUrl + id
    );
  }
  getCompanyCodeById(companyId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.companyCodeByIdUrl + companyId
    );
  }
  postContractType(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postContractTypes,
      obj
    );
  }
  editContractTypes(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putContractTypesUrl,
      obj
    );
  }
  deleteContractTypes(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteContractTypesUrl + id
    );
  }
  getContractTypeById(contId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.contractTypesByIdUrl + contId
    );
  }
  postCostCenter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postCostCenter,
      obj
    );
  }
  editCostCenter(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putCostCenterUrl,
      obj
    );
  }
  deleteCostCenter(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteCostCenterUrl + id
    );
  }
  getCostCenterById(costId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.CostCentersByIdUrl + costId
    );
  }
  postDivision(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postDivision,
      obj
    );
  }
  editDivision(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putDivisionUrl,
      obj
    );
  }
  deleteDivision(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteDivisionUrl + id
    );
  }
  getDivisionById(DivisionId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.divisionByIdUrl + DivisionId
    );
  }
  postGovernorates(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postGovernorates,
      obj
    );
  }
  editGovernorates(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putGovernoratesUrl,
      obj
    );
  }
  deleteGovernorates(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteGovernoratesUrl + id
    );
  }
  getGovernoratesById(govId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.governoratesByIdUrl + govId
    );
  }
  postShift(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postShift,
      obj
    );
  }
  editShift(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putShiftUrl,
      obj
    );
  }
  deleteShift(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteShiftUrl + id
    );
  }
  getShiftById(govId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.ShiftByIdUrl + govId
    );
  }
  postLanguages(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postLanguages,
      obj
    );
  }
  editLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putLanguagesUrl,
      obj
    );
  }
  deleteLanguages(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteLanguagesUrl + id
    );
  }

  postAreaLanguages(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postAreaLanguage,
      obj
    );
  }
  editAreaLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putAreaLanguageUrl,
      obj
    );
  }
  deleteAreaLanguages(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement + myGlobals.deleteAreaLanguagesUrl + id
    );
  }
  postAttendanceStatesLanguages(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postAttendanceStatesLanguage,
      obj
    );
  }
  editAttendanceStatesLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement +
      myGlobals.putAttendanceStatesLanguageUrl,
      obj
    );
  }
  deleteAttendanceStatesLanguages(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlDataManagement +
      myGlobals.deleteAttendanceStatesLanguagesUrl +
      id
    );
  }
  //////OrderStatus/////
  postOrderStatusLanguages(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.postOrderStatusLanguage,
      obj
    );
  }
  editOrderStatusLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.putOrderStatusLanguageUrl,
      obj
    );
  }
  deleteOrderStatusLanguages(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.deleteAttendanceStatesLanguagesUrl +
      id
    );
  }
  /////Working Type
  editWorkingTypeLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.putWorkingTypeLanguageUrl,
      obj
    );
  }
  //
  editOrderSubStatusLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.putOrderSubStatusLanguageUrl,
      obj
    );
  }

  postAvailabilityLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postAvailabilityLanguage,
      obj
    );
  }
  editAvailabilityLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putAvailabilityLanguageUrl,
      obj
    );
  }
  postBuildingTypesLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postBuildingTypesLanguage,
      obj
    );
  }
  editBuildingTypesLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putBuildingTypesLanguageUrl,
      obj
    );
  }
  postCompanyCodeLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postCompanyCodeLanguage,
      obj
    );
  }
  editCompanyCodeLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putCompanyCodeLanguageUrl,
      obj
    );
  }
  postContractTypesLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postContractTypesLanguage,
      obj
    );
  }
  editContractTypesLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putContractTypesLanguageUrl,
      obj
    );
  }
  postCostCenterLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postCostCenterLanguage,
      obj
    );
  }
  editCostCenterLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putLang_CostCenterLanguageUrl,
      obj
    );
  }
  postDivisionLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postDivisionLanguage,
      obj
    );
  }
  editDivisionLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putDivisionLanguageUrl,
      obj
    );
  }
  postGovernoratesLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postGovernoratesLanguage,
      obj
    );
  }
  editGovernoratesLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putGovernoratesLanguageUrl,
      obj
    );
  }
  postShiftLanguage(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlDataManagement + myGlobals.postShiftLanguage,
      obj
    );
  }
  editShiftLanguages(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putShiftUrl,
      obj
    );
  }
  editProblem(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.putProblemLanguageUrl,
      obj
    );
  }

  ///////////User Management//////////
  getAllUserAdmin(): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.adminUserUrl);
  }
  getUserById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlIdentity + myGlobals.getrUserById + id
    );
  }
  postUser(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlIdentity + myGlobals.postUserUrl,
      obj
    );
  }
  editUser(obj): Observable<any> {
    return this.http.put(myGlobals.BaseUrlIdentity + myGlobals.putUserUrl, obj);
  }
  getAllDispatchers(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatcherUrl
    );
  }
  getAllDispatchersWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatcherWithBlockedUrl
    );
  }
  getDispatcherBySuperId(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getDispatcherBySuperIdUrl +
      id
    );
  }
  getAllDispatchersWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatcherUrlWithBlock
    );
  }
  blockDispatcher(id): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.blockDispatcher + id,
      {}
    );
  }
  getDispatcherById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getDispatcherUserById + id
    );
  }
  getDispatcherBySuper(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getDispatchersUserBySuper +
      id
    );
  }
  postUserDispatcher(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postUserDispatcherUrl,
      obj
    );
  }
  editUserDispatcher(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.putUserDispatcherUrl,
      obj
    );
  }
  getAllDrivers(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getDriverUrl
    );
  }
  getAllIcAgents(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getICAgentUrl
    );
  }
  getAllSuperVisor(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getSuperVisorUrl
    );
  }
  getSuperVisorByDivisionId(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getSuperVisorByDivisionIdUrl +
      id
    );
  }
  getAllSuperVisorWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getSupervisorUrlWithBlock
    );
  }
  blockSupervisor(id): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.blockSupervisor + id,
      {}
    );
  }
  postSuperVisor(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postSupervisorUrl,
      obj
    );
  }
  editUserSuperVisor(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.putUserSupervisorUrl,
      obj
    );
  }
  getSuperVisorById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getSupervisorUserById + id
    );
  }
  getSuperVisor(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getSupervisorUser + id
    );
  }
  getSupervisorByUserId(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getSupervisorUserByUserId +
      id
    );
  }
  getSuperVisorByDivision(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getSupervisorUserBydivison +
      id
    );
  }
  postDriver(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postDriverUrl,
      obj
    );
  }
  editUserDiver(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.putUserDriverUrl,
      obj
    );
  }
  editUserManager(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.putUserMangerUrl,
      obj
    );
  }
  editUserMateialController(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.putUserMaterialControllerUrl,
      obj
    );
  }
  getDriverById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getDriverUserById + id
    );
  }
  getManagerById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getMangerUserById + id
    );
  }
  getMaterialControllerById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getMateialControllerUserById +
      id
    );
  }
  getDriverByDivision(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getDriverUserBydivison + id
    );
  }
  getAllEngineers(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getEngineerUrl
    );
  }
  getEngineersByDivisionId(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getEngineersByDivisionIdUrl +
      id
    );
  }
  getAllEngineersWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getEngineerUrlWithBlock
    );
  }
  blockEngineer(id): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.blockEngineer + id,
      {}
    );
  }
  postEngineer(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postEngineerUrl,
      obj
    );
  }
  editUserEngineer(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.putUserEngineerUrl,
      obj
    );
  }
  getEngineerById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getEngineerUserById + id
    );
  }
  getEngineerByDivision(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getEngineerUserBydivison +
      id
    );
  }
  getAllTechnicians(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getTechnicianUrl
    );
  }
  //By Shaimaa Sayed
  getAllAssignedTechnicians(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getAssignedTechnicianUrl
    );
  }
  getAllTechniciansWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getTechnicianUrlWithBlock
    );
  }
  blockTechnician(id): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.blockTechnician + id,
      {}
    );
  }
  postTechnician(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postTechnicianUrl,
      obj
    );
  }
  editUserTechnician(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.putUserTechnicianUrl,
      obj
    );
  }
  getTechnicianById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getTechnicianUserById + id
    );
  }
  getAllForemans(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getForemanUrl
    );
  }
  getAllForemansWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getForemanUrlWithBlock
    );
  }
  blockForeman(id): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.blockForeman + id,
      {}
    );
  }
  postForeman(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postForemanUrl,
      obj
    );
  }
  editUserForeman(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.putUserForemanUrl,
      obj
    );
  }
  getForemanById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getForemanUserById + id
    );
  }
  getForemanByDispatcher(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getFormanUserByDispatcher +
      id
    );
  }
  getAllMangaer(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getManagerUrl
    );
  }
  getAllMangaerWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getManagerUrlWithBlock
    );
  }
  blockManager(id): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.blockManager + id,
      {}
    );
  }
  postManager(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postManagerUrl,
      obj
    );
  }
  postMaterialController(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postMaterialControllerUrl,
      obj
    );
  }
  getAllMaterial(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getMaterialUrl
    );
  }
  getAllMaterialWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getMaterialUrlWithBlock
    );
  }
  blockMaterialController(id): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.blockMaterialController +
      id,
      {}
    );
  }
  resetUserPassword(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlIdentity + myGlobals.resetPassword,
      obj
    );
  }
  deactivateUserInIdentity(name): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlIdentity + myGlobals.deactivateUserByUsername + name
    );
  }

  ///////////Team Management//////////
  getAllUnassignedMempers(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getUnassignedMempersUrl
    );
  }

  getAllUnassignedMempersByDivision(divisionId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getUnassignedMempersByDivisionIdUrl +
      divisionId
    );
  }

  getAllUnassignedVehicles(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getUnassignedVehiclesUrl
    );
  }

  getTechnicianByPF(pf, divisionId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getTechnicianByPFUrl +
      pf +
      "&division=" +
      divisionId
    );
  }

  getTeamByName(name): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getTeamByNameUrl + name
    );
  }

  getVehicleByPlateNo(PlateNo): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getVehicleByPlateNoUrl +
      PlateNo
    );
  }

  addVehcile(PlateNo): Observable<any> {
    let obj = {
      plate_no: PlateNo,
    };
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.addVehicleURL,
      obj
    );
  }

  updateVehcile(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.updateVehicleURL,
      obj
    );
  }

  getAllVehicles(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getVehicleAllUrl
    );
  }

  getMembersByTeamId(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getMembersByTeamIdUrl + id
    );
  }

  getTeamByDispatcherById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getByDispatcherByIdUrl + id
    );
  }

  getTeams(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getTeams
    );
  }

  getAllTeams(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getAllTeams
    );
  }
  getAllTeamsWithOutMembers(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getAllTeamsWithOutMembers
    );
  }

  postTeam(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postTeam,
      obj
    );
  }

  updateTeam(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.updateTeam,
      obj
    );
  }

  assginMemberToTeam(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.assignTeamMember +
      "?teamId=" +
      obj.teamId +
      "&memberId=" +
      obj.memberId,
      null
    );
  }

  assginMembersToTeam(teamId:number,members:any[],defaultTech:number): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.assignTeamMembers + teamId+`/${defaultTech}`,
      members
    );
  }
  reassignTeamToDispatcher(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.reassignTeamToDispatcher +
      "?teamId=" +
      obj.teamId +
      "&dispatcherId=" +
      obj.dispatcherId,
      null
    );
  }
  unAssginMember(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.unassignedMember +
      "?memberId=" +
      obj.memberId,
      null
    );
  }

  unAssginMembers(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlUserManagementNew + myGlobals.unassignedMembersURL,
      obj
    );
  }

  deleteTeam(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.deleteTeamURL.replace("{id}", id)
    );
  }

  ///////////dispatcherSetting////////////
  getALLareasEnUrl(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.areasEnUrl
    );
  }

  getAllAreasWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.areasWithblocked
    );
  }

  getAllOrderProblem(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.OrderProblem
    );
  }

  getAllOrderProblemWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.OrderProblemWithBlocked
    );
  }

  getALLDispatcherSettingsList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getALLDispatcherSettingsListUrl
    );
  }

  getAllDispatcherSettings(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.disSettingUrl
    );
  }

  getAllDispatcherSettingspaginated(pageobj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.disSettingUrls,
      pageobj
    );
  }

  getDispatcherSettingById(disId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getDispatcherSettingById.replace("{0}", disId)
    );
  }

  GetMultiAreasAndProblemsbyDispatcherId(disId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.GetMultiAreasAndProblemsbyDispatcherId.replace(
        "{dispatcherId}",
        disId
      )
    );
  }

  deleteDisDetting(disId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.deleteDispatcherSetting.replace("{0}", disId)
    );
  }

  postDispatcherAllProblemsandArea(Disobj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.postDispatcherAllProblemsandArea,
      Disobj
    );
  }

  UpdateMulitWithAreasAndOrderProblems(Disobj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.UpdateMulitWithAreasAndOrderProblemsUrl,
      Disobj
    );
  }

  AddMulitWithAreasAndOrderProblems(Disobj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.AddMulitWithAreasAndOrderProblemsUrl,
      Disobj
    );
  }

  getALLDispatcherSettingsbyGroup(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew +
      myGlobals.getALLDispatcherSettingsbyGroupIdUrl.replace("{GroupId}", id)
    );
  }

  postProblemArea(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlUserManagementNew + myGlobals.postProblemArea,
      obj
    );
  }

  ////////////////////// time sheet ///////////////////

  GetTimeSheet(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.TimeSheetUrlgetdate,
      // "https://dispatcher-order-dev-api.chevrolet-autline-dev.p.azurewebsites.net/api/OrderAction/GetTimeSheet"
      obj
    );
  }

  exportTimeSheetResults(obj): Observable<any> {
    return this.http.post(
      // "https://dispatcher-order-dev-api.chevrolet-autline-dev.p.azurewebsites.net/api/OrderAction/GetTimeSheetExcelExport"
      myGlobals.BaseUrlOrderManagementNew + myGlobals.exportTimeSheetResultsUrl,
      obj
    );
  }

  //////////////////order management////////////////////////////
  getAllUnAssignedOrdersForDispatcher(dispatcherId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.unAssignedOrdersForDispatcherUrl +
      dispatcherId
    );
  }

  getAllAssignedOrdersForDispatcher(dispatcherId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.assignedOrdersForDispatcherUrl +
      dispatcherId
    );
  }

  postAllUnAssignedOrdersForDispatcher(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.unAssignedOrdersForDispatcherPaginatedUrl,
      obj
    );
  }

  postAllUnAssignedOrdersForDispatcherFilter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.unAssignedOrdersForDispatcherPaginatedFilterUrl,
      obj
    );
  }

  //////////////////Supervisor////////////////////////////
  getAllUnAssignedOrdersForSupervisor(Id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.unAssignedOrdersForSupervisorUrl +
      Id
    );
  }

  postAllUnAssignedOrdersForSupervisor(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.unAssignedOrdersForSupervisorPaginatedUrl,
      obj
    );
  }

  postAllUnAssignedOrdersForSupervisorFilter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.unAssignedOrdersForSupervisorFilteredPaginatedUrl,
      obj
    );
  }

  postAllAssignedOrdersForSupervisorFilter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.assignedOrdersForSupervisorFilteredPaginatedUrl,
      obj
    );
  }

  getAllAssignedOrdersForSupervisor(Id, size): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.assignedOrdersForSupervisorUrl +
      Id +
      "&pageSize=" +
      size
    );
  }

  unAssginOrderFromDispatcher(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.unAssigndOrdersFromDispatcherUrl,
      obj
    );
  }

  assginOrderToDispatcher(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.assigndOrdersToDispatcherUrl,
      obj
    );
  }

  assginOrder(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.assigndOrdersUrl,
      obj
    );
  }

  unAssginOrder(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.unAssigndOrdersUrl,
      obj
    );
  }

  rankTeamOrders(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.rankTeamOrdersUrl,
      obj
    );
  }

  rankTeamOrderskByDrag(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.rankTeamOrderskByDragUrl,
      obj
    );
  }

  rankTeamOrdersWithoutNotification(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.rankTeamOrdersWithoutNitificationUrl,
      obj
    );
  }

  getOrderDetails(orderId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.orderDetailsUrl.replace("{ID}", orderId)
    );
  }

  getAllSubOrderStatus(statusId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.orderSubStatus + statusId
    );
  }

  getAllSubOrderStatusWithBlock(statusId): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.orderSubStatusWithBlock +
      statusId
    );
  }
  // getAllSubOrderStatusWithBlock():Observable<any> {
  //   return this.http.get(myGlobals.BaseUrlOrderManagementNew + myGlobals.orderSubStatusWithBlock);
  // }

  blockOrderSubstatus(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.blockSubstatus + id
    );
  }

  updateOrderSubStatus(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.updateOrderSubStatus,
      obj
    );
  }

  addOrderSubStatus(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.addOrderSubStatus,
      obj
    );
  }

  changeOrderStatus(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.orderChangeStatus,
      obj
    );
  }

  updateOrder(order): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.updateOrderUrl,
      order
    );
  }

  getLocationByPaci(paciCode): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlLocation + myGlobals.getPaciLocationUrl + paciCode
    );
  }

  getAllPriority(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getAllPriorityUrl
    );
  }

  getAllStatus(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getAllStatusUrl
    );
  }

  getAllStatusWithBlockedList(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getAllStatusWithBlockedUrl
    );
  }

  getCompanyCode(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.getCompanyCodeUrl
    );
  }

  getTeamsForForemen(data): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlUserManagementNew + myGlobals.getTeamsForForemen,
      data
    );
  }
  getTeamsWithBlockedForForemen(data): Observable<any> {
    return this.http.get(myGlobals.BaseUrlUserManagementNew + myGlobals.getTeamsWithBlockedForForemen, data);
  }

  getFilteredOrdersForPrint(data): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getFilteredOrdersForPrint,
      data
    );
  }

  deleteOrderType(id): Observable<any> {
    return this.http.delete(myGlobals.deleteOrderTypeUrl + id);
  }

  deleteOrderStatus(id): Observable<any> {
    return this.http.delete(myGlobals.deleteOrderStatusUrl + id);
  }

  removeProblem(id): Observable<any> {
    return this.http.delete(myGlobals.removeProblemUrl + id);
  }

  updateOrderStatus(status): Observable<any> {
    return this.http.put(myGlobals.updateOrderStatusUrl, status);
  }

  updateOrderType(type): Observable<any> {
    return this.http.put(myGlobals.updateOrderTypeUrl, type);
  }

  updateOrderProgressLookUp(progress): Observable<any> {
    return this.http.put(myGlobals.updateOrderProgressUrl, progress);
  }

  updateOrderProblemsLookUp(problem): Observable<any> {
    return this.http.put(myGlobals.updateOrderProblemsUrl, problem);
  }

  /////////////////////////// notification center/////////////////////////

  postNotificationCenter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.PostNotificationCenterURL,
      obj
    );
  }

  getNotificationCenterById(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.GetNotificationCenterByIdURL.replace("{id}", id)
    );
  }

  getNotificationCenterCount(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.GetNotificationCenterByIdURL +
      id
    );
  }

  getNotificationCenterByIds(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.PostNotificationCenterGetByIdsURL,
      obj
    );
  }

  GetAllNotificationCenter(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.GetAllNotificationCenterURL
    );
  }

  updateNotificationCenterItem(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.PutNotificationCenterItemURL,
      obj
    );
  }

  ChangeReadFlagNotificationCenterItem(id: number): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.ChangeReadFlagNotificationCenterItemURL +
      id
    );
  }

  ChangeActionFlagNotificationCenterItem(id: number): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.ChangeActionFlagNotificationCenterItemURL +
      id
    );
  }

  AddNotificationCenter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.PostAddNotificationCenterURL,
      obj
    );
  }

  AddMultiNotificationCenter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.PostAddMultiNotificationCenterURL,
      obj
    );
  }

  getAllPaginatedNotificationCenter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.GetAllPaginatedNotificationCenterURL,
      obj
    );
  }

  getAllPaginatedByPredicateNotificationCenter(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.GetAllPaginatedByPredicateNotificationCenterURL,
      obj
    );
  }
  //Added By Shaimaa Sayed
  getAllNotificationCenterByTechAndOrderCode(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.getAllNotificationCenterByTechAndOrderCode,
      obj
    );
  }
  //getAllNotificationCenterByTechAndOrderCode(obj):Observable<any> {
  //return this.http.post(myGlobals.BaseUrlOrderManagementNew + myGlobals.getAllNotificationCenterByTechAndOrderCode, obj);
  //}

  deleteNotificationCenter(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.DeleteNotificationCenterURL.replace("{id}", id)
    );
  }

  GetNotificationCenterCountByRecieverId(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.GetNotificationCenterCountByRecieverIdURL.replace(
        "{recieverId}",
        id
      )
    );
  }

  MarkAllAsReadByRecieverIdURL(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.MarkAllAsReadByRecieverIdURL +
      id
    );
  }

  putNotificationCenterChangeTeamOrderRank(acceptChange, obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.PutNotificationCenterChangeTeamOrderRankURL.replace(
        "{acceptChange}",
        acceptChange
      ),
      obj
    );
  }

  postNotificationCenterFilteredByDate(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.PostNotificationCenterFilteredByDate,
      obj
    );
  }

  //////////////////////////map settings

  getMapSettings(id): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlDataManagement + myGlobals.getMapSettingsURL + id
    );
  }

  putMapSettings(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlDataManagement + myGlobals.putMapSettingsURL,
      obj
    );
  }

  getAllWorkingTypes(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getWorkingTypes
    );
  }

  getAllWorkingTypesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getWorkingTypesWithBlock
    );
  }
  //getAllOrderStatusWithBlock
  getAllOrderStatusWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.getOrderStatusWithBlock
    );
  }
  blockWorkingType(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.blockWorkingType + id
    );
  }

  addWorkingType(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.addWorkingType,
      obj
    );
  }

  updateWorkingType(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.updateWorkingType,
      obj
    );
  }

  deleteAllOrders(): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.deleteAllOrders
    );
  }

  deleteDispatcherSettings(): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlUserManagementNew + myGlobals.deleteAllDispatcherSettings
    );
  }

  deleteAllUsersFromUserManagement(): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlUserManagementNew + myGlobals.deleteAllManagementUsers
    );
  }

  deleteAllUsersFromIdentity(): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlIdentity + myGlobals.deleteAllUsers
    );
  }

  getOrdersManagementReport(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.postOrdersManagementReport,
      obj
    );
  }

  exportOrdersManagementReport(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew +
      myGlobals.exportOrdersManagementReport,
      obj
    );
  }
  //////////////////////customer Types
  //getAllCustomerTypesWithBlock
  getAllCustomerTypesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.allCustomerTypesLang
    );
  }

  addCustomerType(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.addCustomerTypeURL,
      obj
    );
  }
  blockCustomerType(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.blockCustomerType + id
    );
  }

  editCustomerType(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.putCustomerTypeLanguageUrl,
      obj
    );
  }


  //////////////////////machine Types
  getAllMachineTypesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.allMachineTypesLang
    );
  }

  addMachineType(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.addMachineTypeURL,
      obj
    );
  }
  blockMachineType(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.blockMachineType + id
    );
  }

  editMachineType(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.putMachineTypeLanguageUrl,
      obj
    );
  }


  //////////////////////Phone Types
  getAllPhoneTypesWithBlock(): Observable<any> {
    return this.http.get(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.allPhoneTypesLang
    );
  }

  addPhoneType(obj): Observable<any> {
    return this.http.post(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.addPhoneTypeURL,
      obj
    );
  }
  blockPhoneType(id): Observable<any> {
    return this.http.delete(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.blockPhoneType + id
    );
  }

  editPhoneType(obj): Observable<any> {
    return this.http.put(
      myGlobals.BaseUrlOrderManagementNew + myGlobals.putPhoneTypeLanguageUrl,
      obj
    );
  }


  
  // AreaIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistAreaUrl, obj);
  // }
  // AttendanceStatesIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistAttendanceStatesUrl, obj);
  // }
  // AvailabilityIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistAvailabilityUrl, obj);
  // }
  // BuildingTypesIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistBuildingTypesUrl, obj);
  // }
  // CompanyCodeIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistCompanyCodeUrl, obj);
  // }
  // ContractTypesIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistContractTypesUrl, obj);
  // }
  // CostCenterIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistCostCenterUrl, obj);
  // }
  // DivisionIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistDivisionUrl, obj);
  // }
  // GovernoratesIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistGovernoratesUrl, obj);
  // }
  // ShiftIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistShiftUrl, obj);
  // }
  // LanguagesIsExiste(obj): Observable<any> {
  //   return this.http.put(myGlobals.BaseUrlDataManagement + myGlobals.IsExistLanguagesUrl, obj);
  // }

  // private helper methods
  // private jwt():Observable<any> {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
  //   if (currentUser && currentUser.data.token):Observable<any> {
  //     let headers = new Headers({ 'Authorization': 'bearer ' + currentUser.data.token });
  //     headers.append('Content-Type', 'application/json');
  //     return new RequestOptions({ headers: headers });
  //   }
  // }

  // // private helper methods
  // private jwtwithParams(params):Observable<any> {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
  //   if (currentUser && currentUser.data.token):Observable<any> {
  //     let headers = new Headers({ 'Authorization': 'bearer ' + currentUser.data.token });
  //     headers.append('Content-Type', 'application/json');
  //     return new RequestOptions({ headers: headers, params: params });
  //   }
  // }

  // private jwtDoc():Observable<any> {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //   if (currentUser && currentUser.token.accessToken):Observable<any> {
  //     let headers = new Headers({'Authorization': 'bearer ' + currentUser.token.accessToken});
  //     headers.append('Content-Type', 'application/json');
  //     return new RequestOptions({headers: headers});
  //   }
  // }

  // private header():Observable<any> {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
  //   if (currentUser && currentUser.data.token):Observable<any> {
  //     var headers = new Headers({ 'Authorization': 'bearer ' + currentUser.data.token });
  //     headers.append('Content-Type', 'application/json');
  //     return headers;
  //   }
  // }
}
