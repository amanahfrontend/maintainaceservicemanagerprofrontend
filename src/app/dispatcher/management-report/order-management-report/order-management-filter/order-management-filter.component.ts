import { Component, OnInit, Output, EventEmitter, ViewChild, Input } from "@angular/core";
import { LookupService } from "../../../../api-module/services/lookup-services/lookup.service";
import { NgForm } from "@angular/forms";
import { MessageService } from "primeng/primeng";
import { OrderManagementReportService } from "../order-management-report.service";
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { BaseUrlOrderManagementNew } from '../../../../api-module/services/globalPath';

@Component({
  selector: 'app-order-management-filter',
  templateUrl: './order-management-filter.component.html',
  styleUrls: ['./order-management-filter.component.css']
})


export class OrderManagementFilterComponent implements OnInit {
  @Output() filterEvent = new EventEmitter<object>();
  @Output() exportEvent = new EventEmitter<object>();
  @Output() resetEvent = new EventEmitter();
  @Input() isLoading: boolean = false;
  @Input() isExporting: boolean = false;
  @Input() $filterView: BehaviorSubject<boolean>

  selectedtechs: any;
  filterObj = {
    dateFrom: null,
    dateTo: null,
    customerCode: null,
    orderId: null,
    statusId: null,
    problemId: null,
    orderTypeId: null,
    areaId: null,
    teamId: null,
    techs: null,
    dispatcherId: null,
    divisionId: null,
    companyCode: null,
    includeProgressData: false,
    closed: false,
    allProgress: false,
    lastProgress: false,
    onHoldOnly: false,
    onHoldLastProgress: false
  }



  progressType: any = "allProgress";

  orderStatuses: any[] = [];
  problemTypes: any[] = [];
  orderTypes: any[] = [];
  areas: any[] = [];
  technicians: any[] = [];
  dispatchers: any[] = [];
  divisions: any[] = [];
  companyCodes: any[] = [];
  currentFilteViewFlag: boolean;


  @ViewChild('filterForm', { static: false }) filterForm: NgForm;

  constructor(
    private lookup: LookupService,
    private MessageService: MessageService,
    private orderManagementReportService: OrderManagementReportService,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.currentFilteViewFlag = true;
    this.$filterView.subscribe(flagForFilterView => {
      this.currentFilteViewFlag = flagForFilterView;
    });



    this.lookup.getAllStatusWithBlockedList().subscribe((res1) => {
      this.orderStatuses = res1.data;
      this.orderStatuses.map(element => {
        element.value = element.id;
        element.label = element.name
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
    this.lookup.getAllOrderProblemWithBlockedList().subscribe((res2) => {
      this.problemTypes = res2.data;
      this.problemTypes.map(element => {
        element.value = element.id;
        element.label = element.name
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
    this.lookup.getAllOrderTypesWithBlockedList().subscribe((res3) => {
      this.orderTypes = res3.data;
      this.orderTypes.map(element => {
        element.value = element.id;
        element.label = element.name
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
    this.lookup.getAllAreasWithBlockedList().subscribe((res4) => {
      this.areas = res4.data;
      this.areas.map(element => {
        element.value = element.id;
        element.label = element.name
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
    this.lookup.getAllTechniciansWithBlockedList().subscribe((res5) => {
      this.technicians = res5.data;
      this.technicians.map(element => {
        element.value = {
          id: element.id,
          teamId: element.teamId
        };
        element.label = element.fullName
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
    this.lookup.getAllDispatchersWithBlockedList().subscribe((res6) => {
      this.dispatchers = res6.data;
      this.dispatchers.map(element => {
        element.value = element.id;
        element.label = element.fullName
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
    this.lookup.getAllDivisionsWithBlockedList().subscribe((res7) => {
      this.divisions = res7.data;
      this.divisions.map(element => {
        element.value = element.id;
        element.label = element.name
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
    this.lookup.getCompanyCodesWithBlockedList().subscribe((res8) => {
      this.companyCodes = res8.data;
      this.companyCodes.map(element => {
        element.value = element.id;
        element.label = element.code;
      });
    }, error => {
      console.error(error);
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Failed to load data due to server error.`)
      });
    });
  }

  onClickShowOnlyClosedOrders() {
    if (this.filterObj.closed == true) {
      this.filterObj.includeProgressData = false;
    }
  }

  onClickIncludeProgressData() {
    if (this.filterObj.includeProgressData == true) {
      this.filterObj.closed = false;
      if (this.progressType !== "allProgress") {
        delete this.filterObj.teamId;
        delete this.filterObj.techs;
        this.selectedtechs = [];
      }
    } else {
      delete this.filterObj.teamId;
      delete this.filterObj.techs;
      this.selectedtechs = [];
    }
  }

  onChangeTechs(techsArray) {
    console.log(techsArray.value);
    this.filterObj.teamId = [];
    this.filterObj.techs = [];
    techsArray.value.map((techObj: any) => {
      this.filterObj.techs.push(techObj.id);
      if (techObj.teamId && this.filterObj.teamId.indexOf(techObj.teamId) < 0) {
        this.filterObj.teamId.push(techObj.teamId);
      }
    });
    console.log(this.filterObj);
  }

  // this function to send all 4 values of radio button
  private fixFilterObjectBeforRequest(): any {
    if (this.filterObj.includeProgressData) {
      if (this.progressType == 'allProgress') {
        this.filterObj.allProgress = true;
        this.filterObj.lastProgress = false;
        this.filterObj.onHoldOnly = false;
        this.filterObj.onHoldLastProgress = false;
      }
      else if (this.progressType == 'lastProgress') {
        this.filterObj.allProgress = false;
        this.filterObj.lastProgress = true;
        this.filterObj.onHoldOnly = false;
        this.filterObj.onHoldLastProgress = false;
      }
      else if (this.progressType == 'onHoldOnly') {
        this.filterObj.allProgress = false;
        this.filterObj.lastProgress = false;
        this.filterObj.onHoldOnly = true;
        this.filterObj.onHoldLastProgress = false;
      }
      else if (this.progressType == 'onHoldLastProgress') {
        this.filterObj.allProgress = false;
        this.filterObj.lastProgress = false;
        this.filterObj.onHoldOnly = false;
        this.filterObj.onHoldLastProgress = true;
      }
    } else {
      this.filterObj.allProgress = false;
      this.filterObj.lastProgress = false;
      this.filterObj.onHoldOnly = false;
      this.filterObj.onHoldLastProgress = false;
    }

    let Obj: any = Object.assign({}, this.filterObj);
    var timeZoneShift = new Date().getTimezoneOffset() / (-60);
    var dt = new Date(Obj.dateTo);
    dt.setHours(dt.getHours() + timeZoneShift);
    Obj.dateTo = dt;

    var df = new Date(Obj.dateFrom);
    df.setHours(df.getHours() + timeZoneShift);
    Obj.dateFrom = df;

    // if (Obj.orderId == null || Obj.orderId == undefined) Obj.orderId = 0;
    if (Obj.customerCode == null || Obj.customerCode == undefined) Obj.customerCode = null;
    if (Obj.companyCode == null || Obj.companyCode == undefined) Obj.companyCode = [];
    if (Obj.statusId == null || Obj.statusId == undefined) Obj.statusId = [];
    if (Obj.problemId == null || Obj.problemId == undefined) Obj.problemId = [];
    if (Obj.orderTypeId == null || Obj.orderTypeId == undefined) Obj.orderTypeId = [];
    if (Obj.areaId == null || Obj.areaId == undefined) Obj.areaId = [];
    if (Obj.teamId == null || Obj.teamId == undefined) Obj.teamId = [];
    if (Obj.dispatcherId == null || Obj.dispatcherId == undefined) Obj.dispatcherId = [];
    if (Obj.divisionId == null || Obj.divisionId == undefined) Obj.divisionId = [];
    if (Obj.techs == null || Obj.techs == undefined) Obj.techs = [];




    return Obj;
  }


  exportFile() {
    let start = (new Date(this.filterObj.dateFrom)).getTime();
    let end = (new Date(this.filterObj.dateTo)).getTime();
    if (start < end) {

      let Obj = this.fixFilterObjectBeforRequest();
      this.exportEvent.emit(Obj);
    } else {
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Kindly set the start date before end date.`),
      });
    }
  }


  applyFilter(form) {
    let start = (new Date(this.filterObj.dateFrom)).getTime();
    let end = (new Date(this.filterObj.dateTo)).getTime();
    if (start < end) {

      let Obj = this.fixFilterObjectBeforRequest();
      this.filterEvent.emit(Obj);

    } else {
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Kindly set the start date before end date.`),
      });
    }
    this.orderManagementReportService.resetPagination.next();
  }



  resetFilter() {
    this.resetEvent.emit();
    this.filterForm.resetForm();
    this.selectedtechs = [];
    this.filterObj = {
      dateFrom: null,
      dateTo: null,
      customerCode: null,
      orderId: null,
      statusId: null,
      problemId: null,
      orderTypeId: null,
      areaId: null,
      teamId: null,
      techs: null,
      dispatcherId: null,
      divisionId: null,
      companyCode: null,
      includeProgressData: false,
      closed: false,
      allProgress: true,
      lastProgress: false,
      onHoldOnly: false,
      onHoldLastProgress: false
    }
    this.progressType = "allProgress";
    this.$filterView.next(true);
  }

  chnageFilerFlag(newValue: boolean) {
    this.$filterView.next(newValue);
  }
  //Validation
  patternPreventSpecialCharacter(event: any) {
    const pattern = /^[0-9a-zA-Z \b]+$/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (event.which != 8 && event.which != 0 && event.which < 48 || event.which > 57) {
      event.preventDefault();
    }
  }

}