import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[prevent-click-by-enter]'
})
export class PreventClickByEnterDirective {
  inputElement: ElementRef;

  constructor(el: ElementRef) {
    this.inputElement = el;
  }

  @HostListener('keypress', ['$event']) onKeyPress(event) {
    if (event.which === 32 || event.which === 13) {
      event.preventDefault();
    }
  }

}
