import { NgModule } from '@angular/core';
import { MapSettingsComponent } from './map-settings.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  { path: "", component: MapSettingsComponent }
];


@NgModule({
  declarations: [
    MapSettingsComponent
  ],
  imports: [
    SharedModuleModule,
    RouterModule.forChild(routes),
    TranslateModule,
  ]
})
export class MapSettingsModule { }
