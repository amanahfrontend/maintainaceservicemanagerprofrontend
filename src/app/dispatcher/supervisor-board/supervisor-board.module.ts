import { NgModule } from '@angular/core';
import { SupervisorBoardComponent } from './supervisor-board.component';
import { SharedBoardModule } from '../shared-board/shared-board.module';
import { RouterModule, Routes } from '@angular/router';
import { EditOrderComponent } from '../shared-board/edit-order/edit-order.component';
import { TranslateModule } from '@ngx-translate/core';

const supervisorboardRoutes: Routes = [
  {
    path: "",
    component: SupervisorBoardComponent
  },
  {
    path: "edit-order/:orderId",
    component: EditOrderComponent
  }
];

@NgModule({
  declarations: [
    SupervisorBoardComponent
  ],
  imports: [
    SharedBoardModule,
    RouterModule.forChild(supervisorboardRoutes),
    TranslateModule
  ]
})
export class SupervisorBoardModule { }
