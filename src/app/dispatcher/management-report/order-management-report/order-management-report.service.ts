import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class OrderManagementReportService {

  constructor() { }
  resetPagination = new Subject();
  pageSize: number = 10;
}
