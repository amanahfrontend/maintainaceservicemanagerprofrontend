import { Injectable } from "@angular/core";


@Injectable()
export class GoogleMapsConfig {
  apiKey: string;
  libraries: any;
  constructor() {
    this.apiKey = localStorage.getItem('webApiKey');
    this.libraries = ["places"];
  }
}