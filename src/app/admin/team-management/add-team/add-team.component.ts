import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css']
})
export class AddTeamComponent implements OnInit {
  addForm: NgForm;
  userObj: any = {
    name: "",
  };
  statuses: any[];
  divisions: any = [];
  isSuccess: boolean = false;
  drivers: any = [];
  formans: any = [];
  shifts: any = [];
  availabilities: any = [];
  superVisors: any = [];
  dispatchers: any = [];
  engineers: any = [];
  divisionObj: any = {};
  NewDivisions: any = {};
  NewForemans: any = {};
  NewshiftObj: any = {};
  NewAvailabilityObj: any = {};
  shiftObj: any = {};
  availabilityObj: any = {};
  engObj: any = {};
  supervisorObj: any = {};
  dispatcherObj: any = {};
  statusObj: any = {};
  driverObj: any = {};
  formanObj: any = {};
  objToPost: any = {};
  unassignedMembers: any = [];
  memberToPost: any;
  memberTeamToPost: any;
  teamToPost: Array<any> = [];
  teams: any = [];
  unassignedVehicles: Array<any> = [];
  currentTeam: any = { members: [] };
  numberOfServicesFinished: number = 0;
  isLoading: boolean = false;
  PF: string = "";
  plateNo: string = "";
  disableChangeDivision: boolean = false;
  numberOfServicesCalled: number = 0;
  allForemen: any = [];
  // ForemanList: any = [];
  ForemanList: any = {};
  cashedUnassignedVehicles: any = [];
  cashedUnassignedMembers: any = [];

  constructor(private lookUp: LookupService, private messageService: MessageService, private translateService: TranslateService) { }


  ngOnInit() {
    this.getDivisions();
    this.getUnassignedMembers(1);
    this.getUnassignedVehicles();
    this.getAllFormen();
    this.getShifts();
    this.getavailabilities();
    // this.currentTeam.members.length;
    // console.log(this.currentTeam.members.length)
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  getavailabilities() {
    this.seriveCalled();
    this.lookUp.getAvailabilities()
      .subscribe(
        result => {
          this.availabilities = result.data;
          this.availabilityObj = this.availabilities[0];
          this.NewAvailabilityObj = this.availabilities[0];
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  setMemberAsDefault(member) {
   
    if (this.currentTeam && this.currentTeam.driver) {
      this.currentTeam.driver.isDefault = (this.currentTeam.driver.id == member.id);
    }

    
    for (let i = 0; i < this.currentTeam.members.length; i++) {
      // let m = this.currentTeam.members[i];
      this.currentTeam.members[i].isDefault = (this.currentTeam.members[i].id == member.id);
    }
    
    this.userObj.name = member.memberPhone ? (member.memberName + " - " + member.memberPhone) : (member.memberName);
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }

  onServiceFailure(err) {
    console.error(err);
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed!'),
      detail: this.translateService.instant('Failed to get data due to network error.')
    });
    this.checkIfServicesFinished();
  }

  getShifts() {
    this.seriveCalled();
    this.lookUp.getShift()
      .subscribe(
        result => {
          this.shifts = result.data;
          this.shiftObj = this.shifts[0];
          this.NewshiftObj = this.shifts[0];
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  getDivisions() {
    this.seriveCalled();
    this.lookUp.getDivisionEnUrl()
      .subscribe(
        result => {
          this.divisions = result.data;
          this.divisionObj = this.divisions[0];
          this.NewDivisions = this.divisions[0];
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  getUnassignedVehicles() {
    this.seriveCalled();
    this.lookUp.getAllUnassignedVehicles()
      .subscribe(
        result => {
          this.unassignedVehicles = result.data;
          this.unassignedVehicles.map(
            (v, i) => {
              if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName == v.memberParentName) {
                this.unassignedVehicles.splice(i, 1);
              }
            }
          );
          this.cashedUnassignedVehicles = this.unassignedVehicles;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  removeMember(unAssignedMember, i) {
    if (unAssignedMember.isDriver &&
      this.currentTeam.driver &&
      this.currentTeam.driver.memberParentId == unAssignedMember.memberParentId) {
      this.unassignedMembers.splice(i, 1);
    }
    if (!unAssignedMember.isDriver && this.currentTeam.members.length > 0) {
      this.currentTeam.members.map(
        (member: any) => {
          if (member.memberParentId == unAssignedMember.memberParentId) {
            this.unassignedMembers.splice(i, 1);
          }
        }
      );
    }
  }


  resetPFAndGetUnassignedMembers() {
    this.PF = '';
    this.unassignedMembers = this.cashedUnassignedMembers;
  }

  getUnassignedMembers(divisionId) {
    this.seriveCalled();
    this.isLoading = true;
    this.lookUp.getAllUnassignedMempersByDivision(divisionId)
      .subscribe(
        result => {
          this.unassignedMembers = result.data;
          for (let i = (this.unassignedMembers.length - 1); i >= 0; i--) {
            if (this.unassignedMembers[i].isDriver &&
              this.currentTeam.driver &&
              this.currentTeam.driver.memberParentId == this.unassignedMembers[i].memberParentId) {
              this.unassignedMembers.splice(i, 1);
            }
            if (!this.unassignedMembers[i].isDriver && this.currentTeam.members.length > 0) {
              this.currentTeam.members.map(
                (member: any) => {
                  if (member.memberParentId == this.unassignedMembers[i].memberParentId) {
                    this.unassignedMembers.splice(i, 1);
                  }
                }
              );
            }
          }
          this.cashedUnassignedMembers = this.unassignedMembers;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  getAllFormen() {
    //debugger
    this.seriveCalled();
    this.lookUp.getAllForemans()
      .subscribe(
        result => {
          this.allForemen = result.data;
          this.formans = result.data.filter((f) => f.divisionId == 1);
          this.formanObj = this.formans[0];
          this.NewForemans = this.formans[0];
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  onChangeDivision(divisionId) {
    //debugger
    this.getUnassignedMembers(divisionId);
    this.formans = this.allForemen.filter((f) => f.divisionId == divisionId);
    this.formanObj = this.formans[0];
    this.NewForemans = this.formans[0];
  }

  assignVehicle(vehicle: any, index: number) {
    if (this.currentTeam.vehicle && this.currentTeam.vehicle.memberParentName) {
      this.unassignedVehicles.push(this.currentTeam.vehicle);
    }
    this.currentTeam.vehicle = vehicle;
    this.unassignedVehicles.splice(index, 1);
  }

  addTech(tech: any, i: number) {
    this.disableChangeDivision = true;
    if (tech.isDriver) {
      if (this.currentTeam.driver) {
        this.unassignedMembers.push(this.currentTeam.driver);
      }
      this.currentTeam.driver = tech;
    } else {
      this.currentTeam.members.push(tech);
    }
    this.unassignedMembers.splice(i, 1);
  }

  removeDriver(driver) {
    this.unassignedMembers.unshift(driver);
    if (this.userObj.name == (this.currentTeam.driver.memberName + " - " + this.currentTeam.driver.memberPhone)) {
      this.userObj.name = "";
    }
    if (this.currentTeam.members.length == 0) {
      this.disableChangeDivision = false;
    }
    this.currentTeam.driver = null;
  }

  removeTeamMember(member: any, index: number) {
    this.unassignedMembers.unshift(member);
    this.currentTeam.members.splice(index, 1);
    if (this.userObj.name == (member.memberName + " - " + member.memberPhone)) {
      this.userObj.name = "";
    }
    if (this.currentTeam.members.length == 0 && !this.currentTeam.driver) {
      this.disableChangeDivision = false;
    }
  }

  removeVehicle(vehicle) {
    this.unassignedVehicles.unshift(vehicle);
    this.currentTeam.vehicle = null;
  }

  onTechSearch() {
    this.unassignedMembers = [];
    this.cashedUnassignedMembers.map((cashedMember) => {
      if (cashedMember.pf.indexOf(this.PF) > -1) {
        this.unassignedMembers.push(cashedMember);
      }
    });
  }

  onResetVehicleSearch() {
    this.plateNo = '';
    this.unassignedVehicles = this.cashedUnassignedVehicles;
  }

  onVehicleSearch() {
    this.unassignedVehicles = [];
    this.cashedUnassignedVehicles.map((cashedVehicle) => {
      if (cashedVehicle.memberParentName.indexOf(this.plateNo) > -1) {
        this.unassignedVehicles.push(cashedVehicle);
      }
    });
  }


  onSubmit() {
    if (!this.userObj.name) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select name to submit')
      });
    } else if (!this.divisionObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Division to submit')
      });
    } else if (!this.formanObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Forman to submit')
      });
    } else if (!this.shiftObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Shift to submit')
      });
    } else if (!this.availabilityObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Availability to submit')
      });
    } else {
      this.teamToPost = [];
      this.objToPost = {
        divisionId: this.divisionObj.id,
        divisionName: this.divisionObj.name,
        foremanId: this.formanObj.id,
        foremanName: this.formanObj.name,
        shiftId: this.shiftObj.id,
        name: this.userObj.name,
        statusId: this.availabilityObj.id,
        statusName: this.availabilityObj.name,
      }
      let defaultTech: number = 0;

      if (this.currentTeam.vehicle) {
        this.teamToPost.push(this.currentTeam.vehicle.id);
      }

      if (this.currentTeam.driver) {
        if (this.currentTeam.driver.isDefault) {
          defaultTech = this.currentTeam.driver.id;
        }

        this.teamToPost.push(this.currentTeam.driver.id);
      }
      this.currentTeam.members.forEach(member => {
        if (member.isDefault) {
          defaultTech = member.id;
        }
        this.teamToPost.push(member.id);
      });

      if (defaultTech == 0) {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed!'),
          detail: this.translateService.instant('Select Default Tech')
        });
        return false;
      }



      this.isLoading = true;
      this.seriveCalled();
      this.lookUp.postTeam(this.objToPost)
        .subscribe(
          (teamCreationResponse) => {
            if (teamCreationResponse.isSucceeded) {
              let teamIdWithMembers = {
                teamId: teamCreationResponse.data.id,
                members: this.teamToPost
              }
              let teamId: number = teamCreationResponse.data.id;
              let members = this.teamToPost;


              this.lookUp.assginMembersToTeam(teamId,members,defaultTech)
                .subscribe(
                  (res: any) => {
                    this.checkIfServicesFinished();
                    if (res.isSucceeded) {
                      this.currentTeam.driver = null;
                      this.currentTeam.vehicle = null;
                      this.currentTeam.members = [];
                      this.userObj.name = "";
                      this.disableChangeDivision = false;
                      this.divisionObj = this.NewDivisions,
                        this.shiftObj = this.NewshiftObj;
                      this.availabilityObj = this.NewAvailabilityObj;
                      //this.divisionObj.name = "",
                      //console.log("hhh", this.allForemen)
                      this.formans = this.allForemen.filter((f) => f.divisionId == 1);
                      //this.ForemanList = this.allForemen.filter((f) => f.divisionId == 1);
                      this.formanObj = this.formans[0],
                        //  console.log("", this.formanObj)
                        //this.formanObj.name = "",
                        this.messageService.add({
                          severity: 'success',
                          summary: this.translateService.instant('Success!'),
                          detail: this.translateService.instant('Team Added Successfully!')
                        });
                    } else {
                      this.onServiceFailure(res);
                    }
                  }, err => {
                    this.onServiceFailure(err);
                  });
            } else if (teamCreationResponse.status.code == 7) {
              this.checkIfServicesFinished();
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant('The Team Name already existed!')
              });
            } else {
              this.onServiceFailure(teamCreationResponse);
            }
          }, err => {
            this.onServiceFailure(err);
          }
        );
    }
  }
}