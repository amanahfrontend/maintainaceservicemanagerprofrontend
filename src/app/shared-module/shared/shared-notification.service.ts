import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedNotificationService {

  acceptChangeRankSubject = new Subject<any>();
  notificationCountSubject = new Subject<number>();
  notificationData = new Subject<any>();
  notificationCount: number = 0;
  constructor() { }
}
